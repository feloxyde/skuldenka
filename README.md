# skuldenka
A tiny programming tool oriented toward game development trying to offer a different game (and software) development philosophy. This engine sees a program as a concept, where every elements are meant to be adapted by the user itself to give him the ability to enhance its own experience.

**Please note this project is in a really early state of development and current version is only a proof of concept. The base of the engine will see heavy changes in next months. More on that below.** 

Documentation of this project is still in progress, if you want more infos, please contact me either by email at felix.bertoni987@gmail.com or on matrix.org : [#skuldenka:matrix.org](https://riot.im/app/#/room/#skuldenka:matrix.org).

Please refer to [roadmap](ROADMAP.md) for more infos about development. First functional version (model, no scripting) is expected on September 2020 while fully functional (with scripting) on January 2021. 

## in this document

[[_TOC_]]

## concept

First, let's focus on *what* Skuldenka wants to achieve before speaking about *how*. This tool is meant to provide a scripting and execution environment allowing an "absolute" modularity of the program : everything can be changed by the user and components of a program can be used to replace components in another program. That's -sort of- a generalization of a plugin architecture : every component in the program can be plugged in and out. 

![documentation/readme_resources/skdn_infography.jpg](documentation/readme_resources/skdn_infography.jpg)


## motivations 

Such a high level of modularity isn't forcibly useful for most applications and traditional plugin architectures are enough in most of cases. However video games are softwares depending, from a point of view of user experience, much from art than pure science. What is "good" in a game is mostly subjective and thus only depends on user, and games are, unlike other "specialized" softwares very... well, unspecialized. A picture editing tool will always have core features anyone wants -load a picture, save, edit on screen... etc-, while games are much various, even in their core : a turn-based game isn't built on the same "main loop" structure a fast paced real time shooter would be built on. But both can be fairly similar in eveything else -characters, graphics, ...-. 

Big games can afford to build themselves on a plugin architecture, but indies can't always afford that, especially to a large extent. An indie developer creating a twin stick shooter game won't forcibly bother to allow players to add a spell system in the game -and he/she is right-. Skuldenka is built with indies and hobbyists in mind to allow them to develop highly moddables games without worrying about architecture. 

## then, how ?

The main idea is to always give access to full architecture of the program to the user, and to also expose component's specifications, while greatly easing component change. This is inspired by five programming concepts. 

*Interfaces and templates* A program is made of mostly two main components : interfaces and implems. Interfaces (as in Object Oriented Design) defines atomic specifications of a component, while implems are concrete components satisfying specifications of one or more interfaces. The templating comes then here : implems aren't allowed to *use* another implem directly, they request it through interface. As for example, we could imagine that an implem "Wooden Table" would need feets and a plateau, so it will request two implems, implementing respectively "Plateau" and "Foot" interfaces. When building a program, the programmer chooses, recusively, which implem to use for which request.


*Adapter pattern and decoration pattern* In order to make things easier when swapping components from a program to another, the adapter pattern shall be built-in the interface-implem system. An adapter is an attachment that can be put on an implem in order to allow it to satisfy other interfaces, given it first satisfies a set of interfaces. To get back on the example, we could imagine an adapter adapting a "GlassPane" implem to satisfy the "Plateau" interface, because log implements "PutThingsOn" interface. Making it built-in the system allows us to have it working like a decoration and not a wrapper : adapter data is attached to the object. It also allows some optimizations at runtime.


*Contracts* Because of template-like program creation and adapters, traditional interfaces as you can see in OOP are insufficient because they can't describe with enough flexibility requirements to assign implems. For example, with interfaces, and adapter would not be able to add requirements on the inner implems (actually requests) of an implem. This is why a contract-like paradigm will be implemented soon. At first it was meant to be a complement to interfaces, but turns out it is much easier to only implement contracts. 

You can learn more about it in [conception/model.md](conception/model.md). 

## limits 

A solution solving every IT problems perfectly does not exist, and skuldenka is no exception and has limits.

First, we can't efficiently fight against the eventual will of a developper to prevent his program -game- to not be modular, especially since we allow some components to be written with C++, that are obviously possible to make completely opaque. But in the first place, why would someone bother to use skuldenka to make a non modular software ? It would be like using a knife to eat soup : at some extent it works but there are much more fitting tools for that.

Second, we can only *ease* components swapping, not making them trivial for every case. Easing could be great in most of points, as the developper is heavily guided and there are a lot of safeguards in the model to preven the creation of an invalid program. With the same idea, an overall poorly designed architecture will be harder to tweak than one created with modularity in mind.

Third, performances are lower than a direct, compiled software or an highly optimized scripting language : a layer has to be added between the code and the execution to allow modularity. As it is in an early stage, the tool's effective speed has not been tested yet, but performances to expect are somewhere between 4 and 10 time slower than C++ code. As it is possible to use C++ compiled components, critical parts of the code can still be written with high speed technologies.

Fourth, using skuldenka is a bit different than using a traditional programming language, and there can be a bit of learning time before reaching maximum productivity. 

Fifth, using C++ code in skuldenka requires to manually link the code with the model. Even with tools to partially automate the process, this may need a significant amount of efforts. If you require a library to be linked with skuldenka for a project and you can't manage to link it, please contact us so we can help you.

Last but not least : skuldenka is a tool meant mostly to *swap* components between programs, which is really cool for a video game as you could imagine mixing games together. However it requires both programs to be coded using skuldenka, and this can a bit of a paradox : the more people develop the same kind of program with skuldenka, the more such programs will be customizable. That's also why I'm focusing video game first : they involve extremely various fields and thus encourage development of components usable in a lot of other softwares types.

# features 

For more informations about features and when they will be implemented, please refer to the [roadmap](ROADMAP.md). Wait ! did someone just made an readme section to only put a link in it ? EXACTLY !

# repository structure

This repos is divided in several sections.

- skuldenka : the source code of skuldenka, divided into modules and submodules.
- examples : examples on how to use skuldenka
- conception : yet-to-come modifications and reflexions
- documentation : wide scale documentation. Please note that C++ related items -classes etc- are likely to be put directly in source code.
- tutorials : tutorials on how to use skuldenka

# how to use

## building

Currently to build skuldenka, you should not need more that CMake, Make or another build system like Ninja, and a C++17 ready compiler with standard library enabled. 

### Windows 

*To be added*

### MacOS

Honestly, no idea, but is should be close to Linux instructions.

### Linux

First, you need to install dependencies, which are CMake, Make or Ninja and a C++17 ready compiler, like GCC. I'm on Manjaro Linux so following commands are those used to install deps and compile on Manjaro, except for your package manager, they should be the same.

First, clone this repos.

```
git clone https://gitlab.com/feloxyde/skuldenka.git
```

Then install dependencies if not already done. For now there is no differenciation between dev and release, so dev dependencies are needed as well (gcovr / unittest++)

```
sudo pacman -S cmake make gcc gcovr unittest-cpp
```

Change into ```skuldenka/skuldenka``` directory (yep, that not a mistake, skuldenka twice !) and create a ```build``` directory and change into it.

```
cd skuldenka/skuldenka
mkdir build
cd build
```

Call cmake to generate build files and call make

```
cmake ..
make
```

There should be a ```bin``` directory now, with a lot of exectutables.

you can also run tests by calling, please note most tests are not passing, that's normal because most of them have some parts not implemented yet.

```
make testo 
```

To build examples, do the same last manipulations but in ```skuldenka/examples``` directory.

## generate doc

*To be added*

## building for devlopment

This is yet to be done, just look at cmake target to figure it out. Use gcovr for generating coverage report and clang-format to format code.

## building a project 

This is yet to be done, for now please use the main CMake architecture to build your projects if you want to try Skuldenka.

# how to contribute

For now, contributions are not fully open, as the model of the core will change and therefore it makes it hard to accept contributions
during this changes. If you want to actively participate to the development of the core, or be contacted when the core's API will be stable, 
please send me an email at felix.bertoni987@gmail.com. Also, feel free to send me an email or put a star on this project if you are interested in it,
as it is a nice source of motivation for me, or join the matrix/discord to discuss your expectations and ideas about this project.

Later, contributions will be highly welcome on a lot of fields :
- Scripting syntax 
- Testing
- Documenting / Tutorials
- Code review
- Build system
- Runtime optimizations
- Creating libs for Skuldenka and/or binding existing C/C++ libs
- a lot more

# licencing

This software is published under CeCILL-C licence. This licence is fairly similar to GNU-LGPL licence. Learn more at [https://cecill.info/](https://cecill.info/).
