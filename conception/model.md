
# Introduction

## in this document

[[_TOC_]]

## Prerequisites

It is recommended to have some Object Oriented paradigm knowledge.
 
# A Modularity problem

With traditional programming approach, a program will usually look like an assembly of components. 
Each component is a black -or dark- box providing functionnalities. A component has roughly two main parts. 
First part is its interface, providing a way to interact with the component. Interface is composed of entry points 
to link components with others and specifications stating how to use the component and what it does. Specs are usually
expressed through documentation. Second part is the applicative mechanism, insides of the black-or-dark box, or more simply,
*how* the component works. In our case mechanism is code. Applicative code is often using and interacting with other components, 
and these interactions are the architecture of our software. 

In our traditional approach, architecture is usually a developper-side matter and user is rarely exposed 
to the architecture of the software. Therefore, architecture is a mean of a technical documentation, which, of course,
only the lead architect of the team understands. The architecture of a software is a keypoint in understanding how
the software works and what each component does. If the software is insufficiently documented on this side, it can 
slow down new developpers in their exploration of the program. This can become a critical problem in free and open 
source softwares, where outsiders with not much time to spare are common. 

This way of seeing a program also has another drawback : it makes component change fairly hard and hides 
architecture in the binary, preventing user from changing compiled (and so fast) code after the program
is built. This is not supposed to be a problem, except in programs built as art products, as for example 
video games, where user satisfaction is highly depending on what user wants emotionally and not related
to an applicative field.

We will now show a possible solution, chosen by Skulenka, to overcome these difficulties.

# Exposing program structure

## Specification, implementation, program

Let's model one or more programs as three distincts entities. Example are made creating a modular table (not an array, a kitchen table).

**Specifications** (later refered to as specs) define a set of characteristics a component has or should have. For now we can consider it as an interface 
from a point of view of object oriented conception. 

![model_table_example_specs.png](model_table_example_specs.png)


**Implementation** (later refered to as implem) is a piece of code implementing a set of specifications. From a OOC perspective, it can be seen as a class.

![model_table_example_implem.png](model_table_example_implem.png)

Now, let's allow (and force) the programmer to have the entire structure of the program written a only one place. In order to 
do this, we forbid implems to use other implems directly. Instead they shall request the implem using a set of specs.

![model_table_example_request.png](model_table_example_request.png)

A program is created by first selecting an implem for "main" specs and then recursively associate each request with an implem.

![model_table_example_program.png](model_table_example_program.png)

This method is interesting if provided alongside with a tool to easily view specs, programs and available implems and to easily replace an implem by another.

## Adapters

In order to ease implem swapping, for example using industrial style feets instead of four feets for our table, we can provide adapters. Adapters are 
a generalization of the adpter pattern. An adapter makes a implem matching a set of specs *A* to match a specs of specs *B*. Note that *A* can be included or not in *B*.
Ideally, we want that several adapters can be added to a single implem. Building in adapter pattern instead of letting the programmer simulate it allows to create syntaxic
shortcuts as well as performance optimizations, and solves some potential problems of data consistency throught different adaptings.

# Program correctness 

It is important to ensure that a program is valid before it's execution, especially with skuldenka which tries to encourage and ease program modification by the user. 
We also need to allow libraries providing precompiled components -for speed or bindings- to use the skuldenka modeling method and allow component swapping. We also
need to keep in mind that interpreted program shall have decent performances, and thus use an pointer-based and/or integer-based indexing, and not a map-based one as 
some script languages can do. 

The first approach was to implement a simple interface-class system exactly as described in previous section, but we noticed it would be needed to add a wider and more
generic constraint system, as it makes really difficult to change a type properties and requirement on an interface. Therefore we decided that this generic system would 
fully replace the interfaces, as it makes implementation of skuldenka more concise. This system will be called "contracts".

## Contract

A contract is a set of clauses. A clauses expresses a property of the concerned element. As for example, we can have the following operation :

```  javascript
function enlist ( name, age ) 
```

We have here 4 target elements : "enlist" function, "name" and "age" parameters, and finally the return value of the function, "RETVAL".
Each element can be target by clauses. Here are some example of possible clauses :

- name shall implement method "mymethod (method) "

- age shall be convertible to int

- RETVAL is implements 

Depending the point of view, clauses can be seen either as constraints or as guarantees. On abstract elements, as 
function parameters and implem requests, they are constraints. On concrete elements, as variables, implems, function arguments,
function return values, they guarantee that properties stated in clauses on the object are satisfied.

## Contract propagation

In a traditional static approach, we would use the concept of types on each targets to ensure the program is valid : when a concrete element is 
matched with an abstract element, type of the concrete element has to match required type of abstract one. It allows to have really concise
entry points for compiled code, as well as compile-time checks for program correctness. On the other hand, dynamic typed languages does not
enforce this. It allows a better code genericity, as a function can basically be used with any data type, but at the same time it increases
the risk of errors being only detected at runtime if wrong type is provided. 
In order to allow that genericity, some static-typed languages as C++ implement template mechanism, where a data type is allowed to depend on 
another data type. Somehow, in skuldenka that is exactly what we are doing, except that *mostly everything* is a template. Yet we want
to allow compiled code to be templated as well. 

In order to achieve this, the model shall allow to declare from which other element an element properties are inherited, for every element 
that are exposed out of the black box.

Let's explore another function and an implem requests it uses (please note syntax is pseudocode): 

```javascript
function getBulletFromTwo()
{/* function body*/}


/* requesting some implems, they have "bullet" in commin in their
contract but have also "fire" and "ice" respectively */
request(bullet1, {BulletContract))
request(bullet2, {BulletContract})

/* we declare that retval is either bullet1 or bullet2 */
getBulletFromTwo()::RETVAL is bullet1 or bullet2

getBulletFromTwo() implements BulletManagerContract.
```

Because of this declaration, we know which elements can be bound to RETVAl. Because of this
we can see our program as a graph stating possible relation between elements. 
let's consider that the function we defined right before is used in a request, in a program. 
First, let's define our main.


```javascript 
function main(){
    var a = getBullet()
    
    /* */
    renderBullet(a) 

    
}

request(getBullet, {BulletManagerContract})
request(renderBullet, {BulletRendererContract})

main::getBullet::RETVAL is passed as parameter in renderBullet::bullet
```
For purpose in this example, the rendering function implementation using bullet adds one more requirement

```javascript 
function fastRenderBullet( bullet ) /* does rendering without need of other implems */

//we declare that bullet parameter shall implement RenderableContract */
constraint_satisfies(fastRenderBullet::bullet, RenderableContract)
```
We also consider that FireBullet and IceBullet in following program assignment both implements BulletContract, but only FireBullet implements RenderableContract

```yaml  
implem : main 
requests :
    #1
    - rname : getBullet
        implem : getBulletFromTwo()
        requests: 
        #1.1
        - rname : bullet1
          implem : FireBullet()
        #1.2
        - rname : bullet2
          implem : IceBullet()
    #2
    - rname : renderBullet
      implem : fastRenderBullet()
```
At this point, each element of the model is instanciated for it's assign and then we propagate constraints on predecessors of elements. 
For example, here we have the following precedence chain:

```
main::renderBullet::bullet <- main::getBullet::RETVAL <- (main::getBullet::bullet1 || main::getBullet::bullet2)
```

It means that implems selected for main::getBullet::bullet1 and main::getBullet::bullet2 also need to satisfy RenderableContract that is required 
on main::renderBullet::bullet, and as IceBullet does not satisfies it, the program is invalid.

