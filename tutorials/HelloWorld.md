# HelloWorld skuldenka tutorial

<div style="padding:10px; border:4px solid #7f0000; border-radius:5px;">
This tutorial is incomplete and serves as an example to understand the main concept under skuldenka. It does not cover how to setup a skuldenka project. 
As the model representation will change, the way of interacting with skuldenka model representation will be different in the future. However, core concept, 
the separation between the architecture and applicative code, will stay the same. 
The syntax and method to interact with skuldenka from C++ presented here is really complex and effort demanding, it will wrapped into a simpler C++ interface in the future.
</div>

## Introduction

For this tutorial, let's assume we want to make an EXTREMELY complicated program : the traditional "Hello world"
which serves as first example in most programs. 

As skuldenka does not supports scripting yet, we will have to simulate the situation in which the "architecture" part
of the program is accessible when program is compiled. In order to do this, in our main C++ file, we will create a 
```registerProgram``` function in order to setup the architecture of our program, as if it was done in an external file.

But let's start from... well... the start.

Please note that full code described in this tutorial can be found in the examples/hello_world folder.

## Skuldenka first notions

We first want to create our ```main``` procedure. However, Skuldenka does not allow us to define a procedure out of nowhere, 
procedure needs to be attached to an ```implem```, and an implem needs to be related to a ```trait```. 

So we will define a ```main``` trait in our main requirements. Let's have a look at ```trait_main.hpp```. 
As we need to interact with skuldenka inner model representation, each file will have an inner function looking like
```registerXXX(std::shared_ptr<Model> model)```. In our case, the function is called ```registerMainTrait```.

Our trait, main, will be later built-in skuldenka, but for now we need to define it by hand. It simply defines the entry point of a program. We simply tell the model we want to declare a new trait called ```main``` and check there is no error triggered. 

```cpp
    ErrorLog errs = model->declareTrait("main");
    assert(!errs);
```
Then we recover trait created in the model by its name.

```cpp
    auto trait = model->find<Trait>("main");
    assert(trait);
```
Finally, we add a method with no parameters and not returning a value to the trait, called ```main```.

```cpp 
  trait->sm("main");
```
```sm``` stands for static method. Methods are declared as taking no argument and returning nothing by default.

## Main procedure

At this point, we can create our main procedure and our main implem. This is done implem_HelloWorld.hpp. The beginning of our registering code looks a lot like the one for registering the trait : 

```cpp 
ErrorLog errs = model->declareImplem("HelloWorld", 0);
assert(!errs);

auto implem = model->find<Implem>("HelloWorld");
assert(implem);
```

You can notice the ```0``` passed as argument after the name of the implem. It is
a way to tell skuldenka that instances of this implem will need no space for variable. That's pretty logic since our main method is static, and thus will not be called from an instance but directly from the implem itself. In this tutorial we will not mess with instances, but you can look at ```core/translation/tests/test_translation.cpp``` and ```core/translation/tests/fixture_Model.cpp``` to see how it is done. However, it is recommended to finish reading this tutorial before doing so.

Getting back to our main implem, we tell skuldenka that this implem implements the ```main``` trait : 

```cpp
implem->t("main");
```
and we define a vtable for the trait : 

```cpp
skuldenka::core::model::Vtable& vmain = implem->defineVtable("main");
```

Vtables are collection of implementations of methods of a trait in an implem. In the future, traits will probably disappear and vtables as well to be replaced by a much simpler and flexible system.

```cpp
skuldenka::core::model::Vtable& vmain = implem->defineVtable("main");
```

As it returns a vtable, we can add main method definition to the vtable. 

```cpp
vmain.defineMethod("main", 
            std::make_shared<StaticBodyWrapper0a>(hello_world_main));
```

```defineMethod``` takes two arguments : the name of the method and a method body, which is an abstraction of a function pointer. ```StaticBodyWrapper0a``` is
defined in ```MethodBodyWrapper.hpp``` in case you want to learn more about it. 

We passed a function pointer as parameter, but... what actually this function does ? It is the body of our ```main``` method. 

```cpp
Variant hello_world_main(std::shared_ptr<ImplemNode> node)
```

This function takes an ```ImplemNode``` pointer as the first parameter. An ```ImplemNode``` is a decorated representation of our implem that will be created when building runtime or our program. It returns a Variant, which is used in Skuldenka to carry variables around.

Before discussing how to use it, we want to look at what our method will do. This is an HelloWorld program, so we simply want it to output some text. In order to make this interesting, we want to delegate some tasks to other components of the program : one will create a text for the helloworld, and
one will output the string in the console. If it was pure C++, our main body would look somehow like this : 

```cpp 
string text = getText()
ostream << text;
```

However, skuldenka disallows the use of concrete components -aka implems- directly into the method of another implem. We have to declare our needs into the registering method.

```cpp
implem->requestTrait("Ostream");
implem->requestTrait("HelloText");

implem->requestImplem("output").t("Ostream");
implem->requestImplem("text").t("HelloText");
```

```requestTrait``` tells the model we will need to call method related to that trait. ```requestImplem``` creates an implem request. An implem request can be seen as a template parameter for our implem. We restrict our implem request to a certain set of traits. Here we want an implem implementing the trait ```Ostream``` that we will refer to as ```output``` and a implem implementing ```HelloText``` trait that we will refer to as ```text```. We will see their implementation later in this tutorial.

Those requests are ordered in their order of declaration. As we want to use their indices to refer to them in method body, we can declare constants.

```cpp 
size_t constexpr t_Ostream = 0;
size_t constexpr t_HelloText = 1;

size_t constexpr i_output = 0;
size_t constexpr i_text = 1;

size_t constexpr m_send = 0;
size_t constexpr m_text = 0;
```

```t_*``` refers to trait requests indices, ```i_*``` to implem requests indices, and finally ```m_*``` to method indices in their respective implem. It corresponds to the order in which methods are declared in the corresponding trait. Just trust me they are the right values.

```send``` method from trait ```Ostream``` has a prototype like 

```cpp
    void send(string);
```

and ```text``` method from trait ```HelloText``` has a prototype like 

```cpp
    string text();
```

Now we know this, we can implement the first line of our main body : getting the hello text. 

```cpp 
Variant text = node->sub(i_text)->operator()
                            (node->traitId(t_HelloText),
                            m_text);                                            
```

This code, if it was pure C++, would look like this : 

```cpp
    template<Implem text>
    /* ... */
    string text = text.text();
```

The difference is that in skuldenka, we have no guarantee of what will be the implem used into an ImplemNode to fullfill an implem request, but we still allow polymorphism between two ImplemNode related to the same implem but not having the same implem fullfilling requests. As if this C++ code would be valid : 

```cpp
    std::vector<ATemplate> v = {ATemplate<OneType>, ATemplate<AnotherType>};
```

Because of this, an implem node carries with it pointers to nodes fullfilling its implem requests. They can be accessed using ```sub``` method and a request index.

```cpp
node->sub(i_text) //returns the ImplemNode related to "text" implem request
```

Then, we call the ```operator()``` of ImplemNode to call a method, taking two or three parameters. The first one is the global trait id of the method we want to call, the second is the method index and the third -optional- is and array of the arguments we want to pass to the method. When the method is called, the node will pass itself as the first argument, and pass the arg array after. 

```cpp
operator()(node->traitId(t_HelloText),m_text);
```

calls method m_text of trait HelloText. Please note that we need to pass by trait request to know the global id of the trait once program building is done. 

Then we do quite the same with the send method of output implem : 

```cpp 
  node->sub(i_output)->operator()(node->traitId(t_Ostream), m_send,
                                  std::array<Variant, 1>({text}));
```

Here is a tiny summary of what the method does : 


```cpp 
  /* retrieve hello world text by calling text = node.text.text() */
  Variant text = node->sub(i_text)->operator()(node->traitId(t_HelloText),
                                               m_text);
  /* display hello text by calling node.output.send(text) */
  node->sub(i_output)->operator()(node->traitId(t_Ostream), m_send,
                                  std::array<Variant, 1>({text}));

  return Variant(); //return nothing
```

Now our ```main``` implem, ```HelloWorld``` is functionnal, and the hardest part of the tutorial is behind us. Other traits and implem definitions are quite the same, so I'll don't go further into details about these ones. Just adding some tips about syntax to declare a method returning value or taking a parameter : 

```cpp 
    /* (found in implem_HelloTextFr.hpp */
    /* defining a method returning a string */
 vhellotext
      .defineMethod("text", std::make_shared<StaticBodyWrapper0a>(hello_text_fr))
      .r(Type::STRING);


    /* found in implem_DirectOstream.hpp) */
 vostream
      .defineMethod("send",
                    std::make_shared<StaticBodyWrapper1a>(direct_ostream_print))
      .p(Type::STRING);
    /* please note that for declaring multiple arguments or a return type, you
    can chaincall "p" and "r" method : .p(...).p(...).r(...) */
```

## Finally creating our program

We implemented -in an ellipse not described in this tutorial-, two implem for ```Ostream``` trait, ```DirectOstream```, printing the text on screen, and ```ReverseOstream``` printing the text **BACKWARD** on screen. We also implemented two implems for ```HelloText``` trait, ```HelloTextEn```, producing a simple and efficient "hello, world !", and ```HelloTextFr```  producing a baguette-flavored "bonjour, le monde !".

At this point you should as yourself several questions, as :
- Why writing a simple Hello World program takes like 100+ lines and 10 files ?
- Is a multilangual, reversible Hello World program really something relevant ?
- Aren't croissants giving more perfume than baguette ? 

Let's tackle simple question first. Baguette and croissants aren't used for the same thing, baguettes are much more versatile. And the smell of hot bread in general is purely amazing. 

Then, of course, it is not really relevant to make a multilingual, reversible hello world, even more since it takes like 100+ lines to write. But this is a funny example about how skuldenka can be useful. Let's create our program in order to do this. Please have a look at ```registerProgram``` into ```HelloWorld.cpp``` file. 

We first have our usual declaration. 

```cpp
ErrorLog errs = model->declareProgram("program", "HelloWorld");
assert(!errs);

auto program = model->find<Program>("program");
```

```HelloWorld``` after program name refers to the ```main``` implem we will use.
And now we can start our Implem assignment.

```cpp
auto& mainAssing = program->implem();
```

This returns a reference to an ImplemAssign object. ImplemAssign have sub assigns to form a tree structure. Exactly like a template instanciation. Let's focus only on the passing block into the if constexpr.

```cpp
   /* assigning DirectOstream implem to "output" request of HelloWorld */
    auto& output = mainAssing.addSub("output", "DirectOstream");
    /* assigning HelloTextEn implem to "text" request of HelloWorld */
    auto& text = mainAssing.addSub("text", "HelloTextEn");
```

Comments are pretty explanatory. Note that ```addSub``` returns returns a reference to the sub ImplemAssign created. This could recursively go really long if our program was bigger. 

With this method, programs can easily be modified form a single location, instead of having to dig into the code to change it. It surely isn't worth to write 100 more lines to make an Hello World program teakable, but it can be extremely interesting for larger and complex programs, as video games can be. Later will be implemented C++ tools to 


## Actually running the program

We finally want to build and run the program. Note that in final version, skuldenka will be an autonomous tool, like an interpreter or a virtual machine, so there will be no need to rebuild it with each programs. 

Lets write the main. 

```cpp
void initModel(std::shared_ptr<Model> model)
{
  /* here we are building up model, you don't need too much to worry about it */
  /* models parts are meant to be mostly provided by libraries. */
  registerOstreamTrait(model);
  registerMainTrait(model);
  registerHelloTextTrait(model);
  registerReverseOstreamImplem(model);
  registerHelloWorldImplem(model);
  registerHelloTextEnImplem(model);
  registerHelloTextFrImplem(model);
  registerDirectOstreamImplem(model);
}

int main()
{
  auto model = std::make_shared<Model>();

  // registering model components
  initModel(model);

  // creating a program
  registerProgram(model);

  // checking model to ensure no errors were made.
  ErrorLog errs = model->check();
  if (errs) {
    std::cout << errs;
    std::cout << "errors found in model, exiting" << std::endl;
    exit(EXIT_FAILURE);
  }

  auto program = model->find<Program>("program");
  assert(program);

  // compile program
  auto builtProgram = skuldenka::core::translation::buildProgram(model, program);

  // run program
  builtProgram->run();

  return 0;
}
```

AAAAND, that's done. Take a moment to understand what is the sequence done, but to be honnest it is no different than any program : code, check, build.

To compile everything, go into examples/hello_world directory. Then make a new ```build``` directory and access it. Please note those instructions are only valid on Linux.

```bash
cd examples/hello_world
mkdir build
cd build
```

run CMake and build, this implies that CMake is installed, as well as a recent C++ compiler, as gcc or clang, and a build tool as Make or Ninja. 

```bash
cmake ..

make
#or
ninja
```

then, you can run the program 

```bash
./bin/HelloWorld
```

ouput should be something like : 
```
hello, world ! 
```
but it all depends which implems you chose to assign to requests.
