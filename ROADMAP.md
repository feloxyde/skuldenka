# Skuldenka roadmap

This document shows features planned or discussed for skuldenka, their order of implementation and an estimation on when they each will be available, or if they already are their support rate. Please note that the roadmap can be subject to changes. If you need a feature, contact us and we'll discuss about it !

# Current features 

- **Model** : Full, except signals, still uses old type-based modeling
- **C++ binds** : partial, fully manual
- **Runtime** : partial, runs program from model but no scripting implemented. 

# incoming features

* **Model's Contract architecture** : (June 2020)
  * **Implems** 
  * **Traits** 
  * **Implemless operations**  
  * **Signals** 
  * **Simple Program** 
* **Basic translation** : (Jul 2020)
    * **Build program from model** 
    * **Light optimizations options** 
* **Core library** : (Aug 2020) 
    * **Primary types contract definitions (int, ...)** 
    * **Other primary contracts definitions**
* **C++ linking** : (Sep 2020) 
    * **Compiler-dependant plugins**
    * **Lightweight tools for linking C++ with skuldenka's model**


**A this point (Sep 2020), skuldenka starts to be usable**


* **Scripting** : (Jan 2021)
    * **Syntax definition and parsing** : (Oct 2020)
    * **Model directives** : (Nov 2020)
    * **Basic sequences (var, for, while, if ...) and AST** : (Dec 2020)
    * **Scripts dependencies, minor corrections** : (Jan 2021)    
* **GameDev light libs** : (mar 2021)
    * **Graphics**
    * **Physics**
    * **MainLoop**
    * **Inputs**
* **Example game** : (Apr 2021)
* **Corrections and enhancements** : (Jun 2021 and later)

Planned for later : 

* Scripting and project management enhancement
* Heavy runtime optimizations
* Model visualization and manipulation tools (GUI for example)
* Scripting improvements : (foreach etc)
* Components versionning (allowing multiple versions of a component to be in the same model)
* Networking (RPC ...)
