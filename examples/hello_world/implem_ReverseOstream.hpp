/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef REVERSEOSTREAM_HPP
#define REVERSEOSTREAM_HPP

#include <iostream>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <string>

#include "MethodBodyWrapper.hpp"

Variant reverse_ostream_print(std::shared_ptr<ImplemNode> node,
                              std::array<Variant, 1> args)
{
  std::string str = args[0].as<std::string>();
  std::string rev(str.size(), ' ');
  for (size_t i = 0; i < str.size(); ++i) {
    rev[str.size() - i - 1] = str[i];
  }
  std::cout << rev << std::endl;

  return Variant();
}

void registerReverseOstreamImplem(std::shared_ptr<skuldenka::core::model::Model> model)
{
  ErrorLog errs = model->declareImplem("ReverseOstream", 0);
  assert(!errs);

  auto implem = model->find<Implem>("ReverseOstream");
  assert(implem);

  implem->t("Ostream");

  skuldenka::core::model::Vtable& vostream = implem->defineVtable("Ostream");

  vostream
      .defineMethod("send",
                    std::make_shared<StaticBodyWrapper1a>(reverse_ostream_print))
      .p(Type::STRING);
}

#endif
