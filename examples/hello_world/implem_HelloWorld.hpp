/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef HELLOWORLD_HPP
#define HELLOTEXTFR_HPP

#include <iostream>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <string>

#include "MethodBodyWrapper.hpp"

size_t constexpr t_Ostream = 0;
size_t constexpr t_HelloText = 1;

size_t constexpr i_output = 0;
size_t constexpr i_text = 1;

size_t constexpr m_send = 0;
size_t constexpr m_text = 0;

Variant hello_world_main(std::shared_ptr<ImplemNode> node)
{
  assert(node->sub(i_text));
  assert(node->sub(i_output));

  Variant text = node->sub(i_text)->operator()(node->traitId(t_HelloText),
                                               m_text);
  node->sub(i_output)->operator()(node->traitId(t_Ostream), m_send,
                                  std::array<Variant, 1>({text}));
  return Variant();
}

void registerHelloWorldImplem(std::shared_ptr<skuldenka::core::model::Model> model)
{
  ErrorLog errs = model->declareImplem("HelloWorld", 0);
  assert(!errs);

  auto implem = model->find<Implem>("HelloWorld");
  assert(implem);

  implem->t("main");

  skuldenka::core::model::Vtable& vmain = implem->defineVtable("main");

  vmain.defineMethod("main",
                     std::make_shared<StaticBodyWrapper0a>(hello_world_main));

  implem->requestTrait("Ostream");
  implem->requestTrait("HelloText");

  implem->requestImplem("output").t("Ostream");
  implem->requestImplem("text").t("HelloText");
}

#endif
