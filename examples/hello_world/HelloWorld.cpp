/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "implem_DirectOstream.hpp"
#include "implem_HelloTextEn.hpp"
#include "implem_HelloTextFr.hpp"
#include "implem_HelloWorld.hpp"
#include "implem_ReverseOstream.hpp"
#include "trait_HelloText.hpp"
#include "trait_Ostream.hpp"
#include "trait_main.hpp"

#include <skuldenka/core/runtime/Program.hpp>

#include <skuldenka/core/translation/translation.hpp>

/* This function is what you mostly need to care about */
void registerProgram(std::shared_ptr<Model> model)
{
  /* Let's create a program using HelloWorld as a main trait implem */
  ErrorLog errs = model->declareProgram("program", "HelloWorld");
  assert(!errs);

  auto program = model->find<Program>("program");
  auto& mainAssing = program->implem();

  /* This constexpr if is meant to be toggled*/
  if constexpr (false) {
    /* assigning DirectOstream implem to "output" request of HelloWorld */
    auto& output = mainAssing.addSub("output", "DirectOstream");
    /* assigning HelloTextEn implem to "text" request of HelloWorld */
    auto& text = mainAssing.addSub("text", "HelloTextEn");
  } else {
    /* Same with other implems */
    auto& output = mainAssing.addSub("output", "ReverseOstream");
    auto& text = mainAssing.addSub("text", "HelloTextFr");
  }
}

void initModel(std::shared_ptr<Model> model)
{
  /* here we are building up model, you don't need too much to worry about it */
  /* models parts are meant to be mostly provided by libraries. */
  registerOstreamTrait(model);
  registerMainTrait(model);
  registerHelloTextTrait(model);
  registerReverseOstreamImplem(model);
  registerHelloWorldImplem(model);
  registerHelloTextEnImplem(model);
  registerHelloTextFrImplem(model);
  registerDirectOstreamImplem(model);
}

int main()
{
  auto model = std::make_shared<Model>();

  // registering model components
  initModel(model);

  // creating a program
  registerProgram(model);

  // checking model to ensure no errors are done.
  ErrorLog errs = model->check();
  if (errs) {
    std::cout << errs;
    std::cout << "errors found in model, exiting" << std::endl;
    exit(EXIT_FAILURE);
  }

  auto program = model->find<Program>("program");
  assert(program);

  // compile program
  auto builtProgram = skuldenka::core::translation::buildProgram(model, program);

  // run program
  builtProgram->run();

  return 0;
}