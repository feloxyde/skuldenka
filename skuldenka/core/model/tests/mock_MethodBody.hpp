/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_TEST_MOCK_METHODBODY
#define SKULDENKA_CORE_MODEL_TEST_MOCK_METHODBODY

#include <memory>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Variant.hpp>

using namespace skuldenka::core;
using namespace skuldenka::core::model;

class MockStaticMethodBody : public skuldenka::core::model::StaticMethod::Body
{
public:
  MockStaticMethodBody(){};
  virtual ~MockStaticMethodBody(){};

  virtual std::shared_ptr<Body> copy()
  {
    return std::make_shared<MockStaticMethodBody>();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 1> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 2> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 3> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 4> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 5> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 6> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 7> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 8> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 9> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::ImplemNode> thisptr,
                             std::array<Variant, 10> const& args)
  {
    return Variant();
  }
};

class MockMemberMethodBody : public skuldenka::core::model::MemberMethod::Body
{
public:
  MockMemberMethodBody(){};
  virtual ~MockMemberMethodBody(){};

  virtual std::shared_ptr<Body> copy()
  {
    return std::make_shared<MockMemberMethodBody>();
  }

  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 1> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 2> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 3> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 4> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 5> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 6> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 7> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 8> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 9> const& args)
  {
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<runtime::Instance> thisptr,
                             std::array<Variant, 10> const& args)
  {
    return Variant();
  }
};

std::shared_ptr<MockStaticMethodBody> mockStaticBody()
{
  return std::make_shared<MockStaticMethodBody>();
}

std::shared_ptr<MockMemberMethodBody> mockMemberBody()
{
  return std::make_shared<MockMemberMethodBody>();
}

#endif