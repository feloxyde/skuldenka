/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Prototype)
{
  TEST(check)
  {
    Model model;

    model.declareTrait("trait1");
    model.declareTrait("trait2");
    model.declareTrait("trait3");
    model.declareTrait("trait4");
    model.declareTrait("trait5");

    auto trait = model.find<model::Trait>("trait1");
    assert(trait);
    assert(!model.check());

    Prototype protoOK = Prototype(*trait, "protoOK");
    protoOK.r({"trait1", "trait2"})
        .p({"trait1"})
        .p({"trait2"})
        .p({"trait3", "trait4"})
        .p({"trait4", "trait5"});

    Prototype protoNOKret = Prototype(model, "protoOK");
    protoNOKret.r({"trait1", "trait6"})
        .p({"trait1"})
        .p({"trait2"})
        .p({"trait3", "trait4"})
        .p({"trait4", "trait5"});

    Prototype protoNOKparam = Prototype(model, "protoOK");
    protoNOKparam.r({"trait1", "trait3"})
        .p({"trait1"})
        .p({"trait2"})
        .p({"trait7", "trait4"})
        .p({"trait4", "trait6"});

    Prototype protoNOK = Prototype(model, "protoOK");
    protoNOK.r({"trait1", "trait8"})
        .p({"trait1"})
        .p({"trait2"})
        .p({"trait7", "trait4"})
        .p({"trait4", "trait6"});

    CHECK(!protoOK.check());

    ErrorLog errsret;
    errsret += protoNOKret.check();
    CHECK(errsret);
    CHECK_EQUAL(1, errsret.size());
    CHECK((errsret.containsStructure<Prototype<MemberMethod>::Error,
                                     Type::Error, AccessName<Trait>::Error,
                                     component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errsret.filter<Prototype<MemberMethod>::Error>().size()));
    auto errsret1 = (*(errsret.begin()))->subLog();
    CHECK_EQUAL(1, (errsret1.filter<Type::Error>().size()));
    auto errsret2 = (*(errsret1.begin()))->subLog();
    CHECK_EQUAL(1, (errsret2.filter<AccessName<Trait>::Error>().size()));
    auto errsret3 = (*(errsret2.begin()))->subLog();
    CHECK_EQUAL(1, (errsret3.filter<component_not_found<Trait>>().size()));

    ErrorLog errsparam;
    errsparam += protoNOKparam.check();
    CHECK(errsparam);
    CHECK_EQUAL(1, errsparam.size());
    CHECK((errsparam.containsStructure<Prototype<MemberMethod>::Error,
                                       Type::Error, AccessName<Trait>::Error,
                                       component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errsparam.filter<Prototype<MemberMethod>::Error>().size()));
    auto errsparam1 = (*(errsparam.begin()))->subLog();
    CHECK_EQUAL(2, (errsparam1.filter<Type::Error>().size()));
    auto errsparam2 = (*(errsparam1.begin()))->subLog();
    CHECK_EQUAL(1, (errsparam2.filter<AccessName<Trait>::Error>().size()));
    auto errsparam3 = (*(errsparam2.begin()))->subLog();
    CHECK_EQUAL(1, (errsparam3.filter<component_not_found<Trait>>().size()));

    auto errsparam2_2 = (*(++(errsparam1.begin())))->subLog();
    CHECK_EQUAL(1, (errsparam2_2.filter<AccessName<Trait>::Error>().size()));
    auto errsparam3_2 = (*(errsparam2_2.begin()))->subLog();
    CHECK_EQUAL(1, (errsparam3_2.filter<component_not_found<Trait>>().size()));

    ErrorLog errs;
    errs += protoNOK.check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Prototype<MemberMethod>::Error, Type::Error,
                                  AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errs.filter<Prototype<MemberMethod>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(3, (errs1.filter<Type::Error>().size()));
    auto errs2 = (*(errs1.begin()))->subLog();
    CHECK_EQUAL(1, (errs2.filter<AccessName<Trait>::Error>().size()));
    auto errs3 = (*(errs2.begin()))->subLog();
    CHECK_EQUAL(1, (errs3.filter<component_not_found<Trait>>().size()));

    auto errs2_2 = (*(++(errs1.begin())))->subLog();
    CHECK_EQUAL(1, (errs2_2.filter<AccessName<Trait>::Error>().size()));
    auto errs3_2 = (*(errs2_2.begin()))->subLog();
    CHECK_EQUAL(1, (errs3_2.filter<component_not_found<Trait>>().size()));

    auto errs2_3 = (*(++(++(errs1.begin()))))->subLog();
    CHECK_EQUAL(1, (errs2_3.filter<AccessName<Trait>::Error>().size()));
    auto errs3_3 = (*(errs2_3.begin()))->subLog();
    CHECK_EQUAL(1, (errs3_3.filter<component_not_found<Trait>>().size()));
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}