/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <algorithm>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(Trait)
{
  TEST(declaration)
  {

    Model model;
    // declare one

    ErrorLog errs;
    errs = model.declareTrait("trait1");
    auto trait1 = model.find<Trait>("trait1");

    CHECK(!errs);

    // declare another
    errs = model.declareTrait("trait2");
    auto trait2 = model.find<Trait>("trait2");
    // composition

    CHECK(!errs);

    errs = model.declareTrait("trait3");
    auto trait3 = model.find<Trait>("trait3");
    trait3->t("trait1");

    CHECK(!errs);

    // composition, reverse order
    errs = model.declareTrait("trait5");
    auto trait5 = model.find<Trait>("trait5");
    trait5->t("trait4").t("trait2").t("trait3");

    CHECK(!errs);

    errs = model.declareTrait("trait4");
    auto trait4 = model.find<Trait>("trait4");

    CHECK(!errs);

    CHECK(trait1 != nullptr);
    CHECK(trait2 != nullptr);
    CHECK(trait3 != nullptr);
    CHECK(trait4 != nullptr);
    CHECK(trait5 != nullptr);
    CHECK_EQUAL("trait1", trait1->name());
    CHECK_EQUAL("trait2", trait2->name());
    CHECK_EQUAL("trait3", trait3->name());
    CHECK_EQUAL("trait4", trait4->name());
    CHECK_EQUAL("trait5", trait5->name());

    CHECK_EQUAL(0, trait1->requirements().size());
    CHECK_EQUAL(0, trait2->requirements().size());
    CHECK_EQUAL(1, trait3->requirements().size());
    CHECK_EQUAL(0, trait4->requirements().size());
    CHECK_EQUAL(3, trait5->requirements().size());

    auto req = trait5->requirements().begin();
    CHECK(&((req++)->owner()) == &(*(dynamic_pointer_cast<Component>(trait5))));
    CHECK(&((req++)->owner()) == &(*(dynamic_pointer_cast<Component>(trait5))));
    CHECK(&((req++)->owner()) == &(*(dynamic_pointer_cast<Component>(trait5))));
    CHECK(req == trait5->requirements().end());

    // checking requirements are added properly
    bool trait1FoundIn3 = std::find_if(trait3->requirements().begin(),
                                       trait3->requirements().end(),
                                       [](AccessName<Trait> const& val) {
                                         return val.name() == "trait1";
                                       })
                          != trait3->requirements().end();

    CHECK(trait1FoundIn3);

    bool trait4FoundIn5 = std::find_if(trait5->requirements().begin(),
                                       trait5->requirements().end(),
                                       [](AccessName<Trait> const& val) {
                                         return val.name() == "trait4";
                                       })
                          != trait5->requirements().end();

    CHECK(trait4FoundIn5);

    bool trait2FoundIn5 = std::find_if(trait5->requirements().begin(),
                                       trait5->requirements().end(),
                                       [](AccessName<Trait> const& val) {
                                         return val.name() == "trait2";
                                       })
                          != trait5->requirements().end();

    CHECK(trait2FoundIn5);

    bool trait3FoundIn5 = std::find_if(trait5->requirements().begin(),
                                       trait5->requirements().end(),
                                       [](AccessName<Trait> const& val) {
                                         return val.name() == "trait3";
                                       })
                          != trait5->requirements().end();

    CHECK(trait3FoundIn5);

    CHECK(!errs);
  }

  TEST(construction)
  {
    CHECK(false);
  }

  TEST(requireTrait)
  {
    CHECK(false);
  }

  TEST(declarePrototype)
  {
    Model model;

    ErrorLog errs;
    model.declareTrait("trait");
    auto trait = model.find<Trait>("trait");

    trait->mm("proto1").r(Type::VOID).p(Type::INT).p(Type::FLOAT).p(Type::FLOAT);

    trait->mm("proto2").r(Type::INT).p(Type::INT).p(Type::FLOAT).p(Type::FLOAT);

    trait->mm("proto3").r(Type::FLOAT).p(Type::INT).p(Type::FLOAT);

    trait->mm("proto4").r(Type::IMPLEM).p(Type::INT).p(Type::DOUBLE).p(Type::FLOAT);

    trait->mm("proto1").r(Type::IMPLEM).p(Type::INT).p(Type::DOUBLE).p(Type::FLOAT);

    REQUIRE(trait->methods().size() == 5);

    CHECK(&(trait->methods()[0]->owner())
          == &(*(dynamic_pointer_cast<Component>(trait))));
    CHECK(&(trait->methods()[1]->owner())
          == &(*(dynamic_pointer_cast<Component>(trait))));
    CHECK(&(trait->methods()[2]->owner())
          == &(*(dynamic_pointer_cast<Component>(trait))));
    CHECK(&(trait->methods()[3]->owner())
          == &(*(dynamic_pointer_cast<Component>(trait))));
    CHECK(&(trait->methods()[4]->owner())
          == &(*(dynamic_pointer_cast<Component>(trait))));

    Prototype proto1(*trait, "proto1");
    proto1.r(Type::VOID).p(Type::INT).p(Type::FLOAT).p(Type::FLOAT);

    Prototype proto2(*trait, "proto2");
    proto2.r(Type::INT).p(Type::INT).p(Type::FLOAT).p(Type::FLOAT);

    Prototype proto3(*trait, "proto3");
    proto3.r(Type::FLOAT).p(Type::INT).p(Type::FLOAT);

    Prototype proto4(*trait, "proto4");
    proto4.r(Type::IMPLEM).p(Type::INT).p(Type::DOUBLE).p(Type::FLOAT);

    Prototype proto1_overload(*trait, "proto1");
    proto1_overload.r(Type::IMPLEM).p(Type::INT).p(Type::DOUBLE).p(Type::FLOAT);

    CHECK(*(trait->methods()[0]) == proto1);
    CHECK(*(trait->methods()[1]) == proto2);
    CHECK(*(trait->methods()[2]) == proto3);
    CHECK(*(trait->methods()[3]) == proto4);
    CHECK(*(trait->methods()[4]) == proto1_overload);
  }

  TEST(unpack)
  {
    CHECK(false);
  }
}

SUITE(methodRedeclaration)
{
  TEST(str)
  {
    CHECK(false);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}