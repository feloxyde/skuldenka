/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_TEST_MOCK_MODEL
#define SKULDENKA_CORE_MODEL_TEST_MOCK_MODEL

#include <cassert>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Trait.hpp>

using namespace skuldenka::core;
using namespace skuldenka::core::model;

void initQuickModel(Model& model)
{
  model.declareTrait("trait1");
  model.declareTrait("trait2");
  model.declareTrait("trait3");

  model.declareSubModule("sub1");
  {
    auto sub1 = model.find<SubModule>("sub1");
    assert(sub1);
    sub1->declareTrait("trait1");
    sub1->declareTrait("trait2");
    sub1->declareTrait("trait3");
    sub1->declareImplem("implem1", 0);
    sub1->declareImplem("implem2", 0);
    sub1->declareImplem("implem3", 0);

    sub1->declareSubModule("sub1");
    {
      auto sub1_1 = sub1->find<SubModule>("sub1");
      assert(sub1_1);
      sub1_1->declareTrait("trait1");
      sub1_1->declareTrait("trait2");
      sub1_1->declareTrait("trait3");
    }

    sub1->declareSubModule("sub2");
    {
      auto sub1_2 = sub1->find<SubModule>("sub2");
      assert(sub1_2);
      sub1_2->declareTrait("trait1");
      sub1_2->declareTrait("trait2");
      sub1_2->declareTrait("trait3");
      sub1_2->declareImplem("implem1", 0);
    }
  }

  model.declareSubModule("sub2");
  {
    auto sub2 = model.find<SubModule>("sub2");
    assert(sub2);
    sub2->declareTrait("trait1");
    sub2->declareTrait("trait2");
    sub2->declareTrait("trait3");
    sub2->declareImplem("implem1", 0);
    sub2->declareImplem("implem2", 0);
    sub2->declareImplem("implem3", 0);

    sub2->declareSubModule("sub1");
    {
      auto sub2_1 = sub2->find<SubModule>("sub1");
      assert(sub2_1);
      sub2_1->declareTrait("trait1");
      sub2_1->declareTrait("trait2");
      sub2_1->declareTrait("trait3");
      sub2_1->declareTrait("trait4");
      sub2_1->declareTrait("trait5");
      sub2_1->declareTrait("trait6");
    }

    sub2->declareSubModule("sub2");
    {
      auto sub2_2 = sub2->find<SubModule>("sub2");
      assert(sub2_2);
      sub2_2->declareImplem("implem1", 0);
    }
  }

  model.declareSubModule("sub3");
  {
    auto sub3 = model.find<SubModule>("sub3");
    assert(sub3);
    sub3->declareTrait("trait1");
    sub3->declareTrait("trait2");
    sub3->declareTrait("trait3");
    sub3->declareImplem("implem1", 0);
    sub3->declareImplem("implem2", 0);
    sub3->declareImplem("implem3", 0);

    sub3->declareSubModule("sub1");
    {
      auto sub3_1 = sub3->find<SubModule>("sub1");
      assert(sub3_1);
      sub3_1->declareTrait("trait1");
      sub3_1->declareTrait("trait2");
      sub3_1->declareTrait("trait3");

      sub3_1->declareSubModule("sub1");
      {
        auto sub3_1_1 = sub3_1->find<SubModule>("sub1");
        assert(sub3_1_1);
        sub3_1_1->declareTrait("trait1");
        sub3_1_1->declareTrait("trait2");
        sub3_1_1->declareTrait("trait3");
      }
    }
  }

  assert(!model.check());
}

#endif // SKULDENKA_TEST_MOCK_MODEL