/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Method)
{

  TEST(check_wrong_return)
  {
    Model model;
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem);

    implem->t("trait0");

    auto& vtable = implem->defineVtable("trait0");

    vtable.defineMethod("method", mockStaticBody()).r({"trait1"}).p(Type::INT);

    vtable.defineMethod("method", mockMemberBody()).r({"trait2"}).p(Type::INT);

    CHECK(vtable.methods<StaticMethod>()[0] != nullptr);
    ErrorLog errss = vtable.methods<StaticMethod>()[0]->check();
    CHECK(errss);
    CHECK_EQUAL(1, errss.size());
    CHECK((errss.containsStructure<
           StaticMethod::Error, Prototype<StaticMethod>::Error, Type::Error,
           AccessName<Trait>::Error, component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errss.filter<StaticMethod::Error>()).size());
    auto errss1 = (*(errss.begin()))->subLog();
    CHECK_EQUAL(1, (errss1.filter<Prototype<StaticMethod>::Error>()).size());
    auto errss2 = (*(errss1.begin()))->subLog();
    CHECK_EQUAL(1, (errss2.filter<Type::Error>()).size());
    auto errss3 = (*(errss2.begin()))->subLog();
    CHECK_EQUAL(1, (errss3.filter<AccessName<Trait>::Error>()).size());
    auto errss4 = (*(errss3.begin()))->subLog();
    CHECK_EQUAL(1, (errss4.filter<component_not_found<Trait>>()).size());

    CHECK(vtable.methods<MemberMethod>()[0] != nullptr);
    ErrorLog errsm = vtable.methods<MemberMethod>()[0]->check();
    CHECK(errsm);
    CHECK_EQUAL(1, errsm.size());
    CHECK((errsm.containsStructure<
           MemberMethod::Error, Prototype<MemberMethod>::Error, Type::Error,
           AccessName<Trait>::Error, component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errsm.filter<MemberMethod::Error>()).size());
    auto errsm1 = (*(errsm.begin()))->subLog();
    CHECK_EQUAL(1, (errsm1.filter<Prototype<MemberMethod>::Error>()).size());
    auto errsm2 = (*(errsm1.begin()))->subLog();
    CHECK_EQUAL(1, (errsm2.filter<Type::Error>()).size());
    auto errsm3 = (*(errsm2.begin()))->subLog();
    CHECK_EQUAL(1, (errsm3.filter<AccessName<Trait>::Error>()).size());
    auto errsm4 = (*(errsm3.begin()))->subLog();
    CHECK_EQUAL(1, (errsm4.filter<component_not_found<Trait>>()).size());
  }

  TEST(check_wrong_param)
  {
    Model model;
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem);

    implem->t("trait0");

    auto& vtable = implem->defineVtable("trait0");

    vtable.defineMethod("method", mockStaticBody()).p({"trait2"});

    vtable.defineMethod("method", mockMemberBody()).p({"trait3"});

    CHECK(vtable.methods<StaticMethod>()[0] != nullptr);
    ErrorLog errss = vtable.methods<StaticMethod>()[0]->check();
    CHECK(errss);
    CHECK_EQUAL(1, errss.size());
    CHECK((errss.containsStructure<
           StaticMethod::Error, Prototype<StaticMethod>::Error, Type::Error,
           AccessName<Trait>::Error, component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errss.filter<StaticMethod::Error>()).size());
    auto errss1 = (*(errss.begin()))->subLog();
    CHECK_EQUAL(1, (errss1.filter<Prototype<StaticMethod>::Error>()).size());
    auto errss2 = (*(errss1.begin()))->subLog();
    CHECK_EQUAL(1, (errss2.filter<Type::Error>()).size());
    auto errss3 = (*(errss2.begin()))->subLog();
    CHECK_EQUAL(1, (errss3.filter<AccessName<Trait>::Error>()).size());
    auto errss4 = (*(errss3.begin()))->subLog();
    CHECK_EQUAL(1, (errss4.filter<component_not_found<Trait>>()).size());

    CHECK(vtable.methods<MemberMethod>()[0] != nullptr);
    ErrorLog errsm = vtable.methods<MemberMethod>()[0]->check();
    CHECK(errsm);
    CHECK_EQUAL(1, errsm.size());
    CHECK((errsm.containsStructure<
           MemberMethod::Error, Prototype<MemberMethod>::Error, Type::Error,
           AccessName<Trait>::Error, component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errsm.filter<MemberMethod::Error>()).size());
    auto errsm1 = (*(errsm.begin()))->subLog();
    CHECK_EQUAL(1, (errsm1.filter<Prototype<MemberMethod>::Error>()).size());
    auto errsm2 = (*(errsm1.begin()))->subLog();
    CHECK_EQUAL(1, (errsm2.filter<Type::Error>()).size());
    auto errsm3 = (*(errsm2.begin()))->subLog();
    CHECK_EQUAL(1, (errsm3.filter<AccessName<Trait>::Error>()).size());
    auto errsm4 = (*(errsm3.begin()))->subLog();
    CHECK_EQUAL(1, (errsm4.filter<component_not_found<Trait>>()).size());
  }

  TEST(check_invalid_body)
  {
    CHECK(false);
  }

  TEST(check_ok)
  {

    Model model;

    model.declareTrait("trait1");
    model.declareTrait("trait2");
    model.declareTrait("trait3");

    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem);
    implem->t("trait0");

    auto& vtable = implem->defineVtable("trait0");

    vtable.defineMethod("method", mockStaticBody()).r({"trait1"}).p({"trait2"});

    vtable.defineMethod("method", mockMemberBody()).r({"trait1"}).p({"trait3"});

    CHECK(vtable.methods<StaticMethod>()[0] != nullptr);
    ErrorLog errss = vtable.methods<StaticMethod>()[0]->check();
    CHECK(!errss);

    CHECK(vtable.methods<MemberMethod>()[0] != nullptr);
    ErrorLog errsm = vtable.methods<MemberMethod>()[0]->check();
    CHECK(!errsm);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}