/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;
using namespace skuldenka::core::runtime;

SUITE(Variant)
{
  TEST(constructionTrivial)
  {
    Variant voidV;
    Variant charV((char)1);
    Variant shortV((short)2);
    Variant intV((int)3);
    Variant unsignedV((unsigned)4);
    Variant longV((long)5);
    Variant ulongV((unsigned long)6);
    Variant floatV((float)7);
    Variant doubleV((double)8);
    Variant stringV(std::string("hello world"));

    CHECK(voidV.isVoid());
    CHECK(charV.is<char>());
    CHECK(shortV.is<short>());
    CHECK(intV.is<int>());
    CHECK(unsignedV.is<unsigned>());
    CHECK(longV.is<long>());
    CHECK(ulongV.is<unsigned long>());
    CHECK(floatV.is<float>());
    CHECK(doubleV.is<double>());
    CHECK(stringV.is<std::string>());

    CHECK_EQUAL(1, charV.as<char>());
    CHECK_EQUAL(2, shortV.as<short>());
    CHECK_EQUAL(3, intV.as<int>());
    CHECK_EQUAL(4, unsignedV.as<unsigned>());
    CHECK_EQUAL(5, longV.as<long>());
    CHECK_EQUAL(6, ulongV.as<unsigned long>());
    CHECK_EQUAL(7, floatV.as<float>());
    CHECK_EQUAL(8, doubleV.as<double>());
    CHECK_EQUAL(std::string("hello world"), stringV.as<std::string>());
  }

  TEST(constructionNonTrivial)
  {
    Variant nodePtr = std::shared_ptr<ImplemNode>();
    Variant instancePtr = std::shared_ptr<Instance>();
    Variant nodeWeak = std::weak_ptr<ImplemNode>();
    Variant instanceWeak = std::weak_ptr<Instance>();
    Variant anyShared = std::shared_ptr<void>();
    Variant anyWeak = std::weak_ptr<void>();

    CHECK(nodePtr.is<std::shared_ptr<ImplemNode>>());
    CHECK(instancePtr.is<std::shared_ptr<Instance>>());
    CHECK(nodeWeak.is<std::weak_ptr<ImplemNode>>());
    CHECK(instanceWeak.is<std::weak_ptr<Instance>>());
    CHECK(anyShared.is<std::shared_ptr<void>>());
    CHECK(anyWeak.is<std::weak_ptr<void>>());

    CHECK(nodePtr.as<std::shared_ptr<ImplemNode>>() == nullptr);
    CHECK(instancePtr.as<std::shared_ptr<Instance>>() == nullptr);
    CHECK(nodeWeak.as<std::weak_ptr<ImplemNode>>().expired());
    CHECK(instanceWeak.as<std::weak_ptr<Instance>>().expired());
    CHECK(anyShared.as<std::shared_ptr<void>>() == nullptr);
    CHECK(anyWeak.as<std::weak_ptr<void>>().expired());
  }

  TEST(destructionNonTrivial)
  {
    //#CHECK for destruction of any types
    CHECK(false);
  }

  TEST(operatorEqual)
  {
    CHECK(false);
  }

  TEST(copy)
  {
    Variant voidV;
    Variant charV((char)1);
    Variant shortV((short)2);
    Variant intV((int)3);

    Variant voidC = voidV;
    Variant charC = charV;
    Variant shortC = shortV;
    Variant intC = intV;

    CHECK(voidC.isVoid());
    CHECK(charC.is<char>());
    CHECK(shortC.is<short>());
    CHECK(intC.is<int>());

    CHECK_EQUAL(1, charC.as<char>());
    CHECK_EQUAL(2, shortC.as<short>());
    CHECK_EQUAL(3, intC.as<int>());
  }

  Variant returnEmptyPtr()
  {
    return Variant(std::shared_ptr<Instance>());
  }

  TEST(returnptr)
  {
    Variant ret = returnEmptyPtr();

    CHECK(ret.is<std::shared_ptr<Instance>>());
    CHECK(ret.as<std::shared_ptr<Instance>>() == nullptr);
  }

  TEST(comparison)
  {
    CHECK(false);
  }

  TEST(serialize)
  {
    CHECK(false);
  }

  TEST(deserialize)
  {
    CHECK(false);
  }

  TEST(any_type)
  {
    CHECK(false);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}