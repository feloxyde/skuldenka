/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/model/typedefs.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Program)
{

  class FixtureModel
  {
  public:
    FixtureModel()
    {
      model.declareTrait("main");
      auto trait1 = model.find<Trait>("main");
      assert(trait1);
      trait1->sm("main").r(Type::VOID);

      model.declareTrait("trait2");
      model.declareTrait("trait3");
      model.declareTrait("trait4");

      model.declareImplem("implem1", 0);
      auto implem1 = model.find<Implem>("implem1");
      assert(implem1);
      implem1->t("main").t("trait2");

      auto& vtablei1t1 = implem1->defineVtable("main");
      vtablei1t1.defineMethod("main", mockStaticBody()).r(Type::VOID);
      implem1->defineVtable("trait2");

      implem1->requestImplem("request2").t("trait2");
      implem1->requestImplem("request3").t("trait3");
      implem1->requestImplem("request4").t("trait4");

      model.declareImplem("implem2", 0);
      auto implem2 = model.find<Implem>("implem2");
      assert(implem2);
      implem2->t("trait2");

      implem2->defineVtable("trait2");

      implem2->requestImplem("request3").t("trait3");
      implem2->requestImplem("request4").t("trait4");

      model.declareImplem("implem3", 0);
      auto implem3 = model.find<Implem>("implem3");
      assert(implem3);
      implem3->t("trait3");

      implem3->defineVtable("trait3");

      model.declareImplem("implem4", 0);
      auto implem4 = model.find<Implem>("implem4");
      assert(implem4);
      implem4->t("trait4");

      implem4->defineVtable("trait4");

      model.declareImplem("implem4bis", 0);
      auto implem4bis = model.find<Implem>("implem4bis");
      assert(implem4bis);
      implem4bis->t("trait4");
      implem4bis->defineVtable("trait4");

      ErrorLog errs;
      errs += model.check();
      assert(!errs);
    }

    ~FixtureModel()
    {}

    void setupAssign(std::shared_ptr<Program>& program)
    {
      auto& assign0 = program->implem();

      auto& assign0_2 = assign0.addSub("request2", "implem2");
      auto& assign0_3 = assign0.addSub("request3", "implem3");
      auto& assign0_4 = assign0.addSub("request4", "implem4");

      auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
      auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

      ErrorLog errs;
      errs += assign0.check();
      assert(!errs);
    }

  public:
    Model model;
  };

  TEST_FIXTURE(FixtureModel, check_assign_error)
  {
    model.declareProgram("program", "implem5");
    auto program = model.find<Program>("program");
    assert(program);

    ErrorLog errs;
    errs += program->check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Program::Error, ImplemAssign::Error,
                                  AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<Program::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<ImplemAssign::Error>().size()));
    auto errs2 = errs1.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<AccessName<Implem>::Error>().size()));
    auto errs3 = errs2.front()->subLog();
    CHECK_EQUAL(1, (errs3.filter<component_not_found<Implem>>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_trait_not_implemented)
  {
    model.declareProgram("program", "implem3");
    auto program = model.find<Program>("program");
    assert(program);

    ErrorLog errs;
    errs += program->check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((
        errs.containsStructure<Program::Error, main_method_trait_not_implemented>()));
    CHECK_EQUAL(1, (errs.filter<Program::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<main_method_trait_not_implemented>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_ok)
  {
    model.declareProgram("program", "implem1");
    auto program = model.find<Program>("program");
    assert(program);

    setupAssign(program);

    ErrorLog errs;
    errs += program->check();

    CHECK(!errs);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}