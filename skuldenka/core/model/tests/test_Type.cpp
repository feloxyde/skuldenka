/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <string>
#include <unordered_set>
#include <vector>
//#FIXME needs to ensure that there are all types are tested !
using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(Type)
{
  TEST(fixme)
  {
    CHECK(false);
    //#FIXME reorganize tests by primary/secondary types !
  }

  TEST(constructionPrimary)
  {
    Model mod;

    Type tchar(mod, Type::CHAR);
    Type tshort(mod, Type::SHORT);
    Type tint(mod, Type::INT);
    Type tunsigned(mod, Type::UNSIGNED);
    Type tlong(mod, Type::LONG);
    Type tunsignedlong(mod, Type::UNSIGNED_LONG);
    Type tfloat(mod, Type::FLOAT);
    Type tdouble(mod, Type::DOUBLE);
    Type tstring(mod, Type::STRING);
    Type timplem(mod, Type::IMPLEM);
    Type tinstance(mod, Type::INSTANCE);

    CHECK(tchar.primary() == Type::CHAR);
    CHECK(tshort.primary() == Type::SHORT);
    CHECK(tint.primary() == Type::INT);
    CHECK(tunsigned.primary() == Type::UNSIGNED);
    CHECK(tlong.primary() == Type::LONG);
    CHECK(tunsignedlong.primary() == Type::UNSIGNED_LONG);
    CHECK(tfloat.primary() == Type::FLOAT);
    CHECK(tdouble.primary() == Type::DOUBLE);
    CHECK(tstring.primary() == Type::STRING);
    CHECK(timplem.primary() == Type::IMPLEM);
    CHECK(tinstance.primary() == Type::INSTANCE);
  }

  TEST(constructionAbstracts)
  {
    Model mod;
    Type traitsNameInit(mod, {AccessName<Trait>(mod, "one"),
                              AccessName<Trait>(mod, "two"),
                              AccessName<Trait>(mod, "three")});

    REQUIRE(traitsNameInit.primary() == Type::TRAITS);
    CHECK_EQUAL(3, traitsNameInit.traits().size());
    CHECK(traitsNameInit.traits().find(AccessName<Trait>(traitsNameInit, "one"))
          != traitsNameInit.traits().end());
    CHECK(traitsNameInit.traits().find(AccessName<Trait>(traitsNameInit, "two"))
          != traitsNameInit.traits().end());
    CHECK(
        traitsNameInit.traits().find(AccessName<Trait>(traitsNameInit, "three"))
        != traitsNameInit.traits().end());
    auto nit = traitsNameInit.traits().begin();
    // checking ownership taking
    CHECK((&((nit++)->owner())) && (&traitsNameInit));
    CHECK((&((nit++)->owner())) && (&traitsNameInit));
    CHECK((&((nit++)->owner())) && (&traitsNameInit));
    CHECK(nit == traitsNameInit.traits().end());

    // CHECK creating from a string set.
    Type traitsNameString(mod, {"one", "::two::three::six", "four::five::seven"});
    REQUIRE(traitsNameString.primary() == Type::TRAITS);
    CHECK_EQUAL(3, traitsNameString.traits().size());
    CHECK(traitsNameString.traits().find(
              AccessName<Trait>(traitsNameString, "one"))
          != traitsNameString.traits().end());
    CHECK(traitsNameString.traits().find(
              AccessName<Trait>(traitsNameString, "six", {"two", "three"}, true))
          != traitsNameString.traits().end());
    CHECK(traitsNameString.traits().find(
              AccessName<Trait>(traitsNameString, "seven", {"four", "five"}))
          != traitsNameString.traits().end());
  }

  TEST(operatorEqualPrimary)
  {
    Model mod;
    Type tchar(mod, Type::CHAR);
    Type tshort(mod, Type::SHORT);
    Type tint(mod, Type::INT);
    Type tunsigned(mod, Type::UNSIGNED);
    Type tlong(mod, Type::LONG);
    Type tunsignedlong(mod, Type::UNSIGNED_LONG);
    Type tfloat(mod, Type::FLOAT);
    Type tdouble(mod, Type::DOUBLE);
    Type tstring(mod, Type::STRING);
    Type timplem(mod, Type::IMPLEM);
    Type tinstance(mod, Type::INSTANCE);

    Type tchar2(mod, Type::CHAR);
    Type tshort2(mod, Type::SHORT);
    Type tint2(mod, Type::INT);
    Type tunsigned2(mod, Type::UNSIGNED);
    Type tlong2(mod, Type::LONG);
    Type tunsignedlong2(mod, Type::UNSIGNED_LONG);
    Type tfloat2(mod, Type::FLOAT);
    Type tdouble2(mod, Type::DOUBLE);
    Type tstring2(mod, Type::STRING);
    Type timplem2(mod, Type::IMPLEM);
    Type tinstance2(mod, Type::INSTANCE);

    CHECK(tchar == tchar2);
    CHECK(tchar != tshort2);
    CHECK(tchar != tint2);
    CHECK(tchar != tunsigned2);
    CHECK(tchar != tlong2);
    CHECK(tchar != tunsignedlong2);
    CHECK(tchar != tfloat2);
    CHECK(tchar != tdouble2);
    CHECK(tchar != tstring2);
    CHECK(tchar != timplem2);
    CHECK(tchar != tinstance2);

    CHECK(tshort == tshort2);
    CHECK(tint == tint2);
    CHECK(tunsigned == tunsigned2);
    CHECK(tlong == tlong2);
    CHECK(tunsignedlong == tunsignedlong2);
    CHECK(tfloat == tfloat2);
    CHECK(tdouble == tdouble2);
    CHECK(tstring == tstring2);
    CHECK(timplem == timplem2);
    CHECK(tinstance == tinstance2);
  }

  TEST(copyConstruct)
  {
    Model mod;

    Type tchar(mod, Type::CHAR);
    Type traitsName(mod, {"one", "two", "three"});

    Type cchar(mod, tchar);
    Type ctraitsName(mod, traitsName);

    CHECK(tchar.primary() == Type::CHAR);

    REQUIRE(ctraitsName.primary() == Type::TRAITS);
    CHECK_EQUAL(3, ctraitsName.traits().size());
    CHECK(ctraitsName.traits().find(AccessName<Trait>(ctraitsName, "one"))
          != ctraitsName.traits().end());
    CHECK(ctraitsName.traits().find(AccessName<Trait>(ctraitsName, "two"))
          != ctraitsName.traits().end());
    CHECK(ctraitsName.traits().find(AccessName<Trait>(ctraitsName, "three"))
          != ctraitsName.traits().end());
  }

  TEST(operatorEqualAbstracts)
  {
    Model mod;
    Type tchar(mod, Type::CHAR);

    Type traitsName(mod, {"one", "two", "three"});

    Type traitsNameEq(mod, {"two", "three", "one"});

    Type traitsNameDiff1(mod, {"two", "three", "one", "four"});

    Type traitsNameDiff2(mod, {"two", "three", "four"});

    CHECK(traitsName != tchar);

    CHECK(traitsName != traitsNameDiff1);
    CHECK(traitsName != traitsNameDiff2);
    CHECK(traitsName == traitsNameEq);

    //#FIXME take in account the module it is from ?
    CHECK(false);
  }

  TEST(isAbstract)
  {
    Model mod;

    Type tchar(mod, Type::CHAR);
    Type tshort(mod, Type::SHORT);
    Type tint(mod, Type::INT);
    Type tunsigned(mod, Type::UNSIGNED);
    Type tlong(mod, Type::LONG);
    Type tunsignedlong(mod, Type::UNSIGNED_LONG);
    Type tfloat(mod, Type::FLOAT);
    Type tdouble(mod, Type::DOUBLE);
    Type tstring(mod, Type::STRING);
    Type timplem(mod, Type::IMPLEM);
    Type tinstance(mod, Type::INSTANCE);
    Type timplemW(mod, Type::IMPLEM_WEAK);
    Type tinstanceW(mod, Type::INSTANCE_WEAK);
    Type traitsName(mod, {"one", "two", "three"});
    Type tvoid(mod, Type::VOID);

    CHECK(!tchar.isAbstract());
    CHECK(!tshort.isAbstract());
    CHECK(!tint.isAbstract());
    CHECK(!tunsigned.isAbstract());
    CHECK(!tlong.isAbstract());
    CHECK(!tunsignedlong.isAbstract());
    CHECK(!tfloat.isAbstract());
    CHECK(!tdouble.isAbstract());
    CHECK(!tstring.isAbstract());
    CHECK(!timplem.isAbstract());
    CHECK(!tinstance.isAbstract());
    CHECK(!timplemW.isAbstract());
    CHECK(!tinstanceW.isAbstract());

    CHECK(traitsName.isAbstract());
    CHECK(tvoid.isAbstract());
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}