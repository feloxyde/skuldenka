/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Error.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/model/typedefs.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_ImplemAssign)
{

  class FixtureModel
  {
  public:
    FixtureModel()
    {
      model.declareTrait("trait1");
      model.declareTrait("trait2");
      model.declareTrait("trait3");
      model.declareTrait("trait4");

      model.declareImplem("implem1", 0);
      auto implem1 = model.find<Implem>("implem1");
      assert(implem1);
      implem1->t("trait1");
      implem1->defineVtable("trait1");

      implem1->requestImplem("request2").t("trait2");
      implem1->requestImplem("request3").t("trait3");
      implem1->requestImplem("request4").t("trait4");

      model.declareImplem("implem2", 0);
      auto implem2 = model.find<Implem>("implem2");
      assert(implem2);
      implem2->t("trait2");
      implem2->defineVtable("trait2");

      implem2->requestImplem("request3").t("trait3");
      implem2->requestImplem("request4").t("trait4");

      model.declareImplem("implem3", 0);
      auto implem3 = model.find<Implem>("implem3");
      assert(implem3);
      implem3->t("trait3");
      implem3->defineVtable("trait3");

      model.declareImplem("implem4", 0);
      auto implem4 = model.find<Implem>("implem4");
      assert(implem4);
      implem4->t("trait4");
      implem4->defineVtable("trait4");

      model.declareImplem("implem4bis", 0);
      auto implem4bis = model.find<Implem>("implem4bis");
      assert(implem4bis);
      implem4bis->t("trait4");
      implem4bis->defineVtable("trait4");

      ErrorLog errs;
      errs += model.check();
      assert(!errs);
    }

    ~FixtureModel()
    {}

  public:
    Model model;
  };

  TEST_FIXTURE(FixtureModel, check_implem_not_declared)
  {
    ImplemAssign assign0(model, "implem5");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");
    auto& assign0_4 = assign0.addSub("request4", "implem4");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<ImplemAssign::Error, AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<AccessName<Implem>::Error>().size()));
    auto errs2 = errs1.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<component_not_found<Implem>>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_assign_not_matching_request)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem4");
    auto& assign0_4 = assign0.addSub("request4", "implem4");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((
        errs.containsStructure<ImplemAssign::Error, assign_not_matching_request>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<assign_not_matching_request>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_non_requested_assign)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");
    auto& assign0_4 = assign0.addSub("request4", "implem4");
    auto& assign0_5 = assign0.addSub("request5", "implem4");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<ImplemAssign::Error, non_requested_assign>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<non_requested_assign>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_incomplete_assign)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<ImplemAssign::Error, incomplete_assign>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<incomplete_assign>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_duplicate_implem_assign)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");
    auto& assign0_4 = assign0.addSub("request4", "implem4");
    auto& assign0_4bis = assign0.addSub("request4", "implem4bis");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<ImplemAssign::Error, duplicate_implem_assign>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<duplicate_implem_assign>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_recursiveErr)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");
    auto& assign0_4 = assign0.addSub("request4", "implem4");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem3");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<ImplemAssign::Error, ImplemAssign::Error,
                                  assign_not_matching_request>()));
    CHECK_EQUAL(1, (errs.filter<ImplemAssign::Error>().size()));
    auto errs1 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs1.filter<ImplemAssign::Error>().size()));
    auto errs2 = errs1.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<assign_not_matching_request>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_ok)
  {
    ImplemAssign assign0(model, "implem1");
    auto& assign0_2 = assign0.addSub("request2", "implem2");
    auto& assign0_3 = assign0.addSub("request3", "implem3");
    auto& assign0_4 = assign0.addSub("request4", "implem4");

    auto& assign0_2_3 = assign0_2.addSub("request3", "implem3");
    auto& assign0_2_4 = assign0_2.addSub("request4", "implem4bis");

    ErrorLog errs;
    errs += assign0.check();

    CHECK(!errs);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}