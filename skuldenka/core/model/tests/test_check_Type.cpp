/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Type)
{

  TEST(check)
  {
    Model model;
    model.declareTrait("trait1");
    model.declareTrait("trait2");
    model.declareTrait("trait3");

    Type tint(model, Type::INT);
    Type tvoid(model, Type::VOID);

    Type traitsOK = Type(model, {"trait1", "trait2", "trait3"});
    Type traitsNOK = Type(model, {"trait3", "trait4", "trait5"});

    CHECK(!tint.check());
    CHECK(!tvoid.check());
    CHECK(!traitsOK.check());

    ErrorLog errs;
    errs += traitsNOK.check();

    CHECK(errs);

    CHECK((errs.containsStructure<Type::Error, AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errs.filter<Type::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(2, (errs1.filter<AccessName<Trait>::Error>().size()));
    auto errs2 = (*(errs1.begin()))->subLog();
    CHECK_EQUAL(1, (errs2.filter<component_not_found<Trait>>().size()));
    auto errs2_2 = (*(++(errs1.begin())))->subLog();
    CHECK_EQUAL(1, (errs2_2.filter<component_not_found<Trait>>().size()));
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}