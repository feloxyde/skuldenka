/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Vtable)
{

  class FixtureModel
  {
  public:
    FixtureModel() : model(), implem()
    {
      model.declareTrait("trait1");
      auto trait1 = model.find<Trait>("trait1");
      CHECK(trait1 != nullptr);

      model.declareTrait("trait2");
      auto trait2 = model.find<Trait>("trait2");
      CHECK(trait2 != nullptr);

      trait1->sm("op1").r(Type::VOID).p(Type::INT).p(Type::INT);
      trait1->mm("op2").r(Type::VOID).p(Type::INT).p(Type::FLOAT);

      trait2->sm("op1").r(Type::VOID).p(Type::INT).p(Type::INT);
      trait2->mm("op2").r(Type::VOID).p(Type::FLOAT).p(Type::FLOAT);

      model.declareImplem("implem1", 0);
      implem = model.find<Implem>("implem1");
      assert(implem);
      implem->t("trait1");
    }
    ~FixtureModel()
    {}

  public:
    Model model;
    std::shared_ptr<Implem> implem;
  };

  TEST_FIXTURE(FixtureModel, check_trait_not_declared)
  {
    implem->t("trait3");

    auto vtable1 = implem->defineVtable("trait3");
    ErrorLog errs;

    errs += vtable1.check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<AccessName<Trait>::Error>().size()));
    auto errs2 = (*(errs1.begin()))->subLog();
    CHECK_EQUAL(1, (errs2.filter<component_not_found<Trait>>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_redefinition_static)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_redefinition>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_redefinition>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_redefinition_member)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_redefinition>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_redefinition>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_not_declared_static)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);
    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);
    vtable1.defineMethod("op3", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_not_declared>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_not_declared>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_not_declared_member)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    vtable1.defineMethod("op3", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);
    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_not_declared>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_not_declared>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_not_defined_static)
  {
    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_not_defined>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_not_defined>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_method_not_defined_member)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Vtable::Error, method_not_defined>()));
    CHECK_EQUAL(1, (errs.filter<Vtable::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<method_not_defined>().size()));
  }

  TEST_FIXTURE(FixtureModel, check_ok)
  {

    auto& vtable1 = implem->defineVtable("trait1");

    vtable1.defineMethod("op1", mockStaticBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::INT);

    vtable1.defineMethod("op2", mockMemberBody())
        .r(Type::VOID)
        .p(Type::INT)
        .p(Type::FLOAT);

    ErrorLog errs;
    errs += vtable1.check();

    CHECK(!errs);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}