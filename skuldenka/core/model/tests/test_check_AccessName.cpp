/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_Model.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_AccessName)
{

  TEST(relative_invalid_nopath)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait4");

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(errsr);
    CHECK((errsr.containsStructure<AccessName<Trait>::Error,
                                   component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errsr.filter<AccessName<Trait>::Error>().size()));
    auto errsr1 = (*(errsr.begin()))->subLog();
    CHECK_EQUAL(1, (errsr1.filter<component_not_found<Trait>>().size()));

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Implem> nameOther(*sub_3_1, "implem1");

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(errs);
    CHECK((errs.containsStructure<AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<AccessName<Implem>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<component_not_found<Implem>>().size()));
  }

  TEST(absolute_invalid_nopath)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait4", {}, true);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(errsr);
    CHECK((errsr.containsStructure<AccessName<Trait>::Error,
                                   component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errsr.filter<AccessName<Trait>::Error>().size()));
    auto errsr1 = (*(errsr.begin()))->subLog();
    CHECK_EQUAL(1, (errsr1.filter<component_not_found<Trait>>().size()));

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Implem> nameOther(*sub_3_1, "implem1", {}, true);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(errs);
    CHECK((errs.containsStructure<AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<AccessName<Implem>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<component_not_found<Implem>>().size()));
  }

  TEST(relative_invalid_path)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1", {"sub1", "sub3"}, false);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(errsr);
    CHECK((errsr.containsStructure<AccessName<Trait>::Error,
                                   component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errsr.filter<AccessName<Trait>::Error>().size()));
    auto errsr1 = (*(errsr.begin()))->subLog();
    CHECK_EQUAL(1, (errsr1.filter<component_not_found<Trait>>().size()));

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Implem> nameOther(*sub_3_1, "implem1", {"sub3"}, false);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(errs);
    CHECK((errs.containsStructure<AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<AccessName<Implem>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<component_not_found<Implem>>().size()));
  }

  TEST(absolute_invalid_path)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1", {"sub1", "sub3"}, true);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(errsr);
    CHECK((errsr.containsStructure<AccessName<Trait>::Error,
                                   component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errsr.filter<AccessName<Trait>::Error>().size()));
    auto errsr1 = (*(errsr.begin()))->subLog();
    CHECK_EQUAL(1, (errsr1.filter<component_not_found<Trait>>().size()));

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Implem> nameOther(*sub_3_1, "implem1", {"sub1", "sub3"}, true);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(errs);
    CHECK((errs.containsStructure<AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<AccessName<Implem>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<component_not_found<Implem>>().size()));
  }

  TEST(wrong_type)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Implem> nameRoot(model, "trait1", {"sub1"}, false);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(errsr);
    CHECK((errsr.containsStructure<AccessName<Implem>::Error,
                                   component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errsr.filter<AccessName<Implem>::Error>().size()));
    auto errsr1 = (*(errsr.begin()))->subLog();
    CHECK_EQUAL(1, (errsr1.filter<component_not_found<Implem>>().size()));

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Implem> nameOther(*sub_3_1, "trait1", {"sub1", "sub2"}, true);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(errs);
    CHECK((errs.containsStructure<AccessName<Implem>::Error,
                                  component_not_found<Implem>>()));
    CHECK_EQUAL(1, (errs.filter<AccessName<Implem>::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<component_not_found<Implem>>().size()));
  }

  TEST(absolute_ok)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1", {"sub1"}, true);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(!errsr);

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Trait> nameOther(*sub_3_1, "trait1", {"sub1", "sub2"}, true);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(!errs);
  }

  TEST(relative_ok)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1", {"sub1"}, false);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(!errsr);

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Trait> nameOther(*sub_3_1, "trait1", {"sub1"}, false);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(!errs);
  }

  TEST(nopath_ok)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1");

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(!errsr);

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Trait> nameOther(*sub_3_1, "trait1");

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(!errs);
  }

  TEST(nopath_absolute_ok)
  {
    Model model;
    initQuickModel(model);
    assert(!model.check());

    AccessName<Trait> nameRoot(model, "trait1", {}, true);

    ErrorLog errsr;
    errsr += nameRoot.check();
    CHECK(!errsr);

    auto sub_3_1 = model.find<SubModule>("sub3")->find<SubModule>("sub1");
    AccessName<Trait> nameOther(*sub_3_1, "trait1", {}, true);

    ErrorLog errs;
    errs += nameOther.check();
    CHECK(!errs);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}