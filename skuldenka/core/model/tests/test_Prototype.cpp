/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/typedefs.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(Prototype)
{
  TEST(constructionMember)
  {
    Model mod;
    Prototype proto(mod, "proto");

    CHECK_EQUAL("proto", proto.name());
    CHECK_EQUAL(&mod, &(proto.owner()));
    CHECK(Type(mod, Type::Primary::VOID) == proto.returnType());
    CHECK_EQUAL(0, proto.parameters().size());
  }

  TEST(setReturnType)
  {
    CHECK(false);
  }

  TEST(addParameter)
  {
    CHECK(false);
  }

  TEST(returnAndParameterChaining)
  {
    CHECK(false);
  }

  TEST(operatorEqualMember)
  {
    Model mod;
    Prototype proto(mod, "proto");
    proto.r(Type::INT).p(Type::DOUBLE).p(Type::CHAR).p(Type::UNSIGNED).p(Type::DOUBLE);

    Prototype protoEq(mod, "proto");
    protoEq.r(Type::INT).p(Type::DOUBLE).p(Type::CHAR).p(Type::UNSIGNED).p(Type::DOUBLE);

    Prototype protoEq2(mod, "proto");
    protoEq2.r(Type::DOUBLE)
        .p(Type::DOUBLE)
        .p(Type::CHAR)
        .p(Type::UNSIGNED)
        .p(Type::DOUBLE);

    Prototype protoDiff1(mod, "protoDiff");
    protoDiff1.r(Type::INT)
        .p(Type::DOUBLE)
        .p(Type::CHAR)
        .p(Type::UNSIGNED)
        .p(Type::DOUBLE);

    Prototype protoDiff2(mod, "proto");
    protoDiff2.r(Type::INT)
        .p(Type::DOUBLE)
        .p(Type::CHAR)
        .p(Type::UNSIGNED)
        .p(Type::DOUBLE)
        .p(Type::DOUBLE);

    Prototype protoDiff3(mod, "proto");
    protoDiff3.r(Type::INT).r(Type::DOUBLE).r(Type::CHAR).r(Type::UNSIGNED);

    Prototype protoDiff4(mod, "proto");
    protoDiff4.r(Type::INT).p(Type::DOUBLE).p(Type::CHAR).p(Type::UNSIGNED).p(Type::INT);

    CHECK(proto == protoEq);
    CHECK(proto == protoEq2);
    CHECK(proto != protoDiff1);
    CHECK(proto != protoDiff2);
    CHECK(proto != protoDiff3);
    CHECK(proto != protoDiff4);

    CHECK(!(proto != protoEq));
    CHECK(!(proto != protoEq2));
    CHECK(!(proto == protoDiff1));
    CHECK(!(proto == protoDiff2));
    CHECK(!(proto == protoDiff3));
    CHECK(!(proto == protoDiff4));
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}