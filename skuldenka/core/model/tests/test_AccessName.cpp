/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_Model.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(AccessName)
{

  TEST(Construction)
  {
    Model model;

    AccessName<Implem> def(model, "def");
    CHECK_EQUAL("def", def.name());
    CHECK_EQUAL(0, def.path().size());
    CHECK(!def.isAbsolute());
    CHECK(&model == &(def.owner()));

    AccessName<Implem> path(model, "path", {"path1", "path2", "path3"});
    CHECK_EQUAL("path", path.name());
    CHECK_EQUAL(3, path.path().size());
    CHECK(!path.isAbsolute());
    auto pathIt = path.path().begin();
    CHECK_EQUAL("path1", *(pathIt++));
    CHECK_EQUAL("path2", *(pathIt++));
    CHECK_EQUAL("path3", *(pathIt++));
    CHECK(pathIt == path.path().end());
    CHECK(&model == &(path.owner()));

    AccessName<Implem> absolute(model, "absolute",
                                {"abs1", "abs2", "abs3", "abs4"}, true);
    CHECK_EQUAL("absolute", absolute.name());
    CHECK_EQUAL(4, absolute.path().size());
    CHECK(absolute.isAbsolute());
    auto absoluteIt = absolute.path().begin();
    CHECK_EQUAL("abs1", *(absoluteIt++));
    CHECK_EQUAL("abs2", *(absoluteIt++));
    CHECK_EQUAL("abs3", *(absoluteIt++));
    CHECK_EQUAL("abs4", *(absoluteIt++));
    CHECK(absoluteIt == absolute.path().end());
    CHECK(&model == &(absolute.owner()));

    AccessName<Implem> relative(model, "relative", {"rel1", "rel2", "rel3"},
                                false);
    CHECK_EQUAL("relative", relative.name());
    CHECK_EQUAL(3, relative.path().size());
    CHECK(!relative.isAbsolute());
    auto relativeIt = relative.path().begin();
    CHECK_EQUAL("rel1", *(relativeIt++));
    CHECK_EQUAL("rel2", *(relativeIt++));
    CHECK_EQUAL("rel3", *(relativeIt++));
    CHECK(relativeIt == relative.path().end());
    CHECK(&model == &(relative.owner()));

    AccessName<Implem> absName(model, "absName", {}, true);
    CHECK_EQUAL("absName", absName.name());
    CHECK_EQUAL(0, absName.path().size());
    CHECK(absName.isAbsolute());
    CHECK(&model == &(absName.owner()));
  }

  TEST(constructionFromString)
  {

    Model mod;

    // valid cases
    AccessName<Trait> noPath = AccessName<Trait>::fromString(mod, "sometrait");
    CHECK_EQUAL("sometrait", noPath.name());
    CHECK_EQUAL(0, noPath.path().size());
    CHECK(!noPath.isAbsolute());

    AccessName<Trait> noPathAbs = AccessName<Trait>::fromString(mod,
                                                                "::sometrait4");
    CHECK_EQUAL("sometrait4", noPathAbs.name());
    CHECK_EQUAL(0, noPathAbs.path().size());
    CHECK(noPathAbs.isAbsolute());

    AccessName<Trait> relPath = AccessName<Trait>::fromString(
        mod, "somestep1::somestep2::somestep3::sometrait3");
    CHECK_EQUAL("sometrait3", relPath.name());
    CHECK_EQUAL(3, relPath.path().size());
    auto relIt = relPath.path().begin();
    CHECK_EQUAL("somestep1", *(relIt++));
    CHECK_EQUAL("somestep2", *(relIt++));
    CHECK_EQUAL("somestep3", *(relIt++));
    CHECK(relIt == relPath.path().end());
    CHECK(!relPath.isAbsolute());

    AccessName<Trait> absPath = AccessName<Trait>::fromString(
        mod, "::somestep6::somestep4::sometrait2");
    CHECK_EQUAL("sometrait2", absPath.name());
    CHECK_EQUAL(2, absPath.path().size());
    auto absIt = absPath.path().begin();
    CHECK_EQUAL("somestep6", *(absIt++));
    CHECK_EQUAL("somestep4", *(absIt++));
    CHECK(absIt == absPath.path().end());
    CHECK(absPath.isAbsolute());

    // invalid cases

    // empty
    CHECK_THROW(AccessName<Trait> t = AccessName<Trait>::fromString(mod, ""),
                invalid_access_name_string);
    // finishing with ":"
    CHECK_THROW(
        AccessName<Trait> t = AccessName<Trait>::fromString(mod, "something:"),
        invalid_access_name_string);

    // starting with a single ":"
    CHECK_THROW(
        AccessName<Trait> t = AccessName<Trait>::fromString(mod, ":something"),
        invalid_access_name_string);

    // no identifier at end
    CHECK_THROW(
        AccessName<Trait> t = AccessName<Trait>::fromString(mod, "something::"),
        invalid_access_name_string);

    // invalid delimiter
    CHECK_THROW(AccessName<Trait> t = AccessName<Trait>::fromString(
                    mod, "::something::somethingelse:again"),
                invalid_access_name_string);

    // invalid delimiter
    CHECK_THROW(AccessName<Trait> t = AccessName<Trait>::fromString(
                    mod, "root:something::something"),
                invalid_access_name_string);
  }

  TEST(find)
  {
    Model model;
    initQuickModel(model);
    REQUIRE(model.check());

    AccessName<SubModule> sub(model, "sub1", {"sub3"});
    auto sub3_1 = sub.find();
    CHECK(sub3_1 != nullptr);
    CHECK(sub3_1 == model.find<SubModule>("sub3")->find<SubModule>("sub1"));

    // relative access
    assert(sub3_1);
    AccessName<Trait> trait(*sub3_1, "trait1", {"sub1"});
    auto trait3_1_1 = trait.find();
    CHECK(trait3_1_1 != nullptr);
    assert(sub3_1->find<SubModule>("sub1"));
    CHECK(trait3_1_1 == sub3_1->find<SubModule>("sub1")->find<Trait>("trait1"));

    // now absolute
    AccessName<Trait> traitabs(*sub3_1, "trait1", {"sub1"}, true);
    auto trait1 = traitabs.find();
    CHECK(trait1 != nullptr);
    assert(model.find<SubModule>("sub1"));
    CHECK(trait1 == model.find<SubModule>("sub1")->find<Trait>("trait1"));

    // wrong name
    AccessName<Trait> wrongName(model, "sub4", {"sub3", "sub1"});
    auto wname = wrongName.find();
    CHECK(wname == nullptr);

    // wrong type
    AccessName<Trait> wrongType(model, "program1", {"sub3", "sub1"});
    auto wtype = wrongType.find();
    CHECK(wtype == nullptr);

    // wrong path
    AccessName<Trait> wrongPath(model, "program1",
                                {"sub3", "sub1", "sub1", "sub2"});
    auto wpath = wrongPath.find();
    CHECK(wpath == nullptr);
  }

  TEST(absolutePath)
  {
    Model model;
    initQuickModel(model);
    REQUIRE(model.check());

    auto sub3 = model.find<SubModule>("sub3");
    assert(sub3);
    auto sub3_1 = sub3->find<SubModule>("sub1");
    assert(sub3_1);
    auto sub3_1_1 = sub3_1->find<SubModule>("sub1");
    assert(sub3_1_1);

    // absolute w/path
    AccessName<Trait> awp(*sub3_1_1, "trait", {"sub1", "sub2"}, true);
    auto awpPath = awp.absolutePath();
    CHECK_EQUAL(2, awpPath.size());
    auto awpPathIt = awpPath.begin();
    CHECK_EQUAL("sub1", *(awpPathIt++));
    CHECK_EQUAL("sub2", *(awpPathIt++));
    CHECK(awpPathIt == awpPath.end());

    // absolute no path
    AccessName<Trait> anp(*sub3_1_1, "trait", {}, true);
    auto anpPath = anp.absolutePath();
    CHECK_EQUAL(0, anpPath.size());

    // relative w/path
    AccessName<Trait> rwp(*sub3_1_1, "trait", {"sub1", "sub2"});
    auto rwpPath = rwp.absolutePath();
    CHECK_EQUAL(5, rwpPath.size());
    auto rwpPathIt = rwpPath.begin();
    CHECK_EQUAL("sub3", *(rwpPathIt++));
    CHECK_EQUAL("sub1", *(rwpPathIt++));
    CHECK_EQUAL("sub1", *(rwpPathIt++));
    CHECK_EQUAL("sub1", *(rwpPathIt++));
    CHECK_EQUAL("sub2", *(rwpPathIt++));
    CHECK(rwpPathIt == rwpPath.end());
    // absolute no path
    AccessName<Trait> rnp(*sub3_1_1, "trait");
    auto rnpPath = rnp.absolutePath();
    CHECK_EQUAL(3, rnpPath.size());
    auto rnpPathIt = rnpPath.begin();
    CHECK_EQUAL("sub3", *(rnpPathIt++));
    CHECK_EQUAL("sub1", *(rnpPathIt++));
    CHECK_EQUAL("sub1", *(rnpPathIt++));
    CHECK(rnpPathIt == rnpPath.end());
  }

  TEST(areEquivalent)
  {

    Model model;
    initQuickModel(model);
    REQUIRE(model.check());

    auto sub1 = model.find<SubModule>("sub1");
    assert(sub1);
    auto sub1_1 = sub1->find<SubModule>("sub1");
    assert(sub1_1);

    auto sub3 = model.find<SubModule>("sub3");
    assert(sub3);
    auto sub3_1 = sub3->find<SubModule>("sub1");
    assert(sub3_1);
    auto sub3_1_1 = sub3_1->find<SubModule>("sub1");
    assert(sub3_1_1);

    AccessName<Trait> sub3_abs_1_2(*sub3, "trait", {"sub1", "sub2"}, true);
    AccessName<Trait> root_abs_1_2(model, "trait", {"sub1", "sub2"}, true);
    CHECK(AccessName<Trait>::areEquivalent(root_abs_1_2, sub3_abs_1_2));

    AccessName<Trait> root_rel_1_2(model, "trait", {"sub1", "sub2"});
    CHECK(AccessName<Trait>::areEquivalent(root_rel_1_2, root_abs_1_2));
    CHECK(AccessName<Trait>::areEquivalent(root_rel_1_2, sub3_abs_1_2));

    AccessName<Trait> sub3_rel_1_2(*sub3, "trait", {"sub1", "sub2"});
    CHECK(!AccessName<Trait>::areEquivalent(sub3_rel_1_2, root_abs_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub3_rel_1_2, root_rel_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub3_rel_1_2, sub3_abs_1_2));

    AccessName<Trait> sub3_1_1_rel_4_1(*sub3_1_1, "trait", {"sub4", "sub1"});
    CHECK(!AccessName<Trait>::areEquivalent(sub3_1_1_rel_4_1, root_abs_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub3_1_1_rel_4_1, root_rel_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub3_1_1_rel_4_1, sub3_abs_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub3_1_1_rel_4_1, sub3_rel_1_2));

    AccessName<Trait> sub1_1_rel_4_1(*sub1_1, "trait", {"sub4", "sub1"});
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, root_abs_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, root_rel_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, sub3_abs_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, sub3_rel_1_2));
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, sub3_1_1_rel_4_1));

    AccessName<Trait> sub3_1_abs_1_1_4_1(*sub3_1, "trait",
                                         {"sub1", "sub1", "sub4", "sub1"}, true);
    AccessName<Trait> sub3_1_abs_1_1_4_1_other(
        *sub3_1, "implem", {"sub1", "sub1", "sub4", "sub1"}, true);
    CHECK(!AccessName<Trait>::areEquivalent(sub3_1_abs_1_1_4_1,
                                            sub3_1_abs_1_1_4_1_other));

    AccessName<Trait> sub1_1_rel_4_1_other(*sub1_1, "implem", {"sub4", "sub1"},
                                           true);
    CHECK(!AccessName<Trait>::areEquivalent(sub1_1_rel_4_1, sub1_1_rel_4_1_other));
  }

  TEST(operatorEqual)
  {
    // EASY TO CHECK, verify this doesnt depends on direct module tho.
    Model model;
    initQuickModel(model);
    REQUIRE(model.check());

    auto sub3 = model.find<SubModule>("sub3");
    assert(sub3);
    auto sub1 = model.find<SubModule>("sub1");
    assert(sub1);

    // creating refs
    AccessName<Implem> refAbs(*sub3, "implem", {"sub1", "sub2"}, true);
    AccessName<Implem> refRel(*sub3, "implem", {"sub1", "sub2"});
    AccessName<Implem> refAbsNp(*sub3, "implem", {}, true);
    AccessName<Implem> refRelNp(*sub3, "implem");
    CHECK(!(refAbs == refRel));
    CHECK(!(refAbs == refAbsNp));
    CHECK(!(refAbs == refRelNp));
    CHECK(!(refRel == refAbsNp));
    CHECK(!(refRel == refRelNp));
    CHECK(!(refAbsNp == refRelNp));

    // creating matching
    AccessName<Implem> okAbs(*sub3, "implem", {"sub1", "sub2"}, true);
    AccessName<Implem> okRel(*sub3, "implem", {"sub1", "sub2"});
    AccessName<Implem> okAbsNp(*sub3, "implem", {}, true);
    AccessName<Implem> okRelNp(*sub3, "implem");
    CHECK(okAbs == refAbs);
    CHECK(okRel == refRel);
    CHECK(okAbsNp == refAbsNp);
    CHECK(okRelNp == refRelNp);

    // different paths
    AccessName<Implem> absOtherPath(*sub3, "implem", {"sub3", "sub1", "sub2"},
                                    true);
    AccessName<Implem> relOtherPath(*sub3, "implem", {"sub3", "sub1", "sub2"});
    CHECK(!(absOtherPath == refAbs));
    CHECK(!(relOtherPath == refRel));
    CHECK(!(absOtherPath == refAbsNp));
    CHECK(!(relOtherPath == refRelNp));

    // different module
    AccessName<Implem> otherModuleAbs(*sub1, "implem", {"sub1", "sub2"}, true);
    AccessName<Implem> otherModuleRel(*sub1, "implem", {"sub1", "sub2"});
    AccessName<Implem> otherModuleAbsNp(*sub1, "implem", {}, true);
    AccessName<Implem> otherModuleRelNp(*sub1, "implem");
    CHECK(otherModuleAbs == refAbs);
    CHECK(otherModuleRel == refRel);
    CHECK(otherModuleAbsNp == refAbsNp);
    CHECK(otherModuleRelNp == refRelNp);

    // different name
    AccessName<Implem> otherNameAbs(*sub3, "trait", {"sub1", "sub2"}, true);
    AccessName<Implem> otherNameRel(*sub3, "trait", {"sub1", "sub2"});
    AccessName<Implem> otherNameAbsNp(*sub3, "trait", {}, true);
    AccessName<Implem> otherNameRelNp(*sub3, "trait");
    CHECK(!(otherNameAbs == refAbs));
    CHECK(!(otherNameRel == refRel));
    CHECK(!(otherNameAbsNp == refAbsNp));
    CHECK(!(otherNameRelNp == refRelNp));
  }

  TEST(str)
  {
    Model model;
    initQuickModel(model);
    REQUIRE(model.check());

    auto sub3 = model.find<SubModule>("sub3");
    assert(sub3);
    auto sub3_1 = sub3->find<SubModule>("sub1");
    assert(sub3_1);
    auto sub3_1_1 = sub3_1->find<SubModule>("sub1");
    assert(sub3_1_1);

    // absolute w/path
    AccessName<Trait> awp(*sub3_1_1, "trait", {"sub1", "sub2"}, true);
    CHECK_EQUAL("::sub1::sub2::trait", awp.str());
    // absolute no path
    AccessName<Trait> anp(*sub3_1_1, "trait", {}, true);
    CHECK_EQUAL("::trait", anp.str());
    // relative w/path
    AccessName<Trait> rwp(*sub3_1_1, "trait", {"sub1", "sub2"});
    CHECK_EQUAL("sub1::sub2::trait", rwp.str());
    // absolute no path
    AccessName<Trait> rnp(*sub3_1_1, "trait");
    CHECK_EQUAL("trait", rnp.str());
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}