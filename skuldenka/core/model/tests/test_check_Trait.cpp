/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_Model.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(check_Trait)
{

  TEST(check_req)
  {
    Model model;

    model.declareTrait("traitNOKreq");
    auto traitNOKreq = model.find<Trait>("traitNOKreq");
    CHECK(traitNOKreq != nullptr);
    traitNOKreq->t("trait6");

    ErrorLog errs;
    errs += traitNOKreq->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Trait::Error, AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errs.filter<Trait::Error>()).size());
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<AccessName<Trait>::Error>()).size());
    auto errs2 = (*(errs1.begin()))->subLog();
    CHECK_EQUAL(1, (errs2.filter<component_not_found<Trait>>()).size());
  }

  TEST(check_req_circular)
  {

    Model model;

    model.declareTrait("trait4");
    auto trait4 = model.find<Trait>("trait4");
    CHECK(trait4 != nullptr);
    trait4->t("traitNOK");

    model.declareTrait("trait5");
    auto trait5 = model.find<Trait>("trait5");
    CHECK(trait5 != nullptr);
    trait5->t("trait4");

    model.declareTrait("traitNOK");
    auto traitNOK = model.find<Trait>("traitNOK");
    CHECK(traitNOK != nullptr);
    traitNOK->t("trait5");

    ErrorLog errs;
    errs += traitNOK->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Trait::Error, circular_trait_dependency>()));
    CHECK_EQUAL(1, (errs.filter<Trait::Error>()).size());
    CHECK_EQUAL(
        1,
        ((*(errs.begin()))->subLog().filter<circular_trait_dependency>()).size());
  }

  TEST(check_redcl)
  {
    Model model;
    initQuickModel(model);

    model.declareTrait("traitNOKredcl");
    auto traitNOKredcl = model.find<Trait>("traitNOKredcl");
    CHECK(traitNOKredcl != nullptr);

    traitNOKredcl->mm("proto1").r(Type::INT).p(Type::INT);
    traitNOKredcl->mm("proto1").r(Type::INT).p(Type::INT);

    ErrorLog errs;
    errs += traitNOKredcl->check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Trait::Error, method_redeclaration>()));
    CHECK_EQUAL(1, (errs.filter<Trait::Error>()).size());
    CHECK_EQUAL(
        1, ((*(errs.begin()))->subLog().filter<method_redeclaration>()).size());
  }

  TEST(check_redcl_static)
  {
    Model model;
    initQuickModel(model);
    model.declareTrait("traitNOKredcl");
    auto traitNOKredcl = model.find<Trait>("traitNOKredcl");
    CHECK(traitNOKredcl != nullptr);

    traitNOKredcl->sm("proto1").r(Type::INT).p(Type::INT);
    traitNOKredcl->sm("proto1").r(Type::INT).p(Type::INT);

    ErrorLog errs;
    errs += traitNOKredcl->check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Trait::Error, method_redeclaration>()));
    CHECK_EQUAL(1, (errs.filter<Trait::Error>()).size());
    CHECK_EQUAL(
        1, ((*(errs.begin()))->subLog().filter<method_redeclaration>()).size());
  }

  TEST(check_method_invalid)
  {
    Model model;
    initQuickModel(model);

    model.declareTrait("traitNOK");
    auto traitNOK = model.find<Trait>("traitNOK");
    CHECK(traitNOK != nullptr);

    traitNOK->mm("proto").r(Type::INT).p({"traitNope", "traitNope2"});

    ErrorLog errs;
    errs += traitNOK->check();

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Trait::Error, Prototype<MemberMethod>::Error,
                                  Type::Error, AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));
    CHECK_EQUAL(1, (errs.filter<Trait::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<Prototype<MemberMethod>::Error>().size()));
    auto errs2 = (*(errs1.begin()))->subLog();
    CHECK_EQUAL(1, (errs2.filter<Type::Error>().size()));
    auto errs3 = (*(errs2.begin()))->subLog();
    CHECK_EQUAL(2, (errs3.filter<AccessName<Trait>::Error>().size()));
    auto errs4 = (*(errs3.begin()))->subLog();
    CHECK_EQUAL(1, (errs4.filter<component_not_found<Trait>>().size()));
    auto errs4_1 = (*(++(errs3.begin())))->subLog();
    CHECK_EQUAL(1, (errs4_1.filter<component_not_found<Trait>>().size()));
  }
}

TEST(check_method_invalid_static)
{
  Model model;
  initQuickModel(model);

  model.declareTrait("traitNOK");
  auto traitNOK = model.find<Trait>("traitNOK");
  CHECK(traitNOK != nullptr);

  traitNOK->sm("proto").r(Type::INT).p({"traitNope", "traitNope2"});

  ErrorLog errs;
  errs += traitNOK->check();
  CHECK_EQUAL(1, errs.size());
  CHECK((errs.containsStructure<Trait::Error, Prototype<StaticMethod>::Error,
                                Type::Error, AccessName<Trait>::Error,
                                component_not_found<Trait>>()));
  CHECK_EQUAL(1, (errs.filter<Trait::Error>().size()));
  auto errs1 = (*(errs.begin()))->subLog();
  CHECK_EQUAL(1, (errs1.filter<Prototype<StaticMethod>::Error>().size()));
  auto errs2 = (*(errs1.begin()))->subLog();
  CHECK_EQUAL(1, (errs2.filter<Type::Error>().size()));
  auto errs3 = (*(errs2.begin()))->subLog();
  CHECK_EQUAL(2, (errs3.filter<AccessName<Trait>::Error>().size()));
  auto errs4 = (*(errs3.begin()))->subLog();
  CHECK_EQUAL(1, (errs4.filter<component_not_found<Trait>>().size()));
  auto errs4_1 = (*(++(errs3.begin())))->subLog();
  CHECK_EQUAL(1, (errs4_1.filter<component_not_found<Trait>>().size()));
}

TEST(check_ok)
{

  Model model;

  model.declareTrait("trait");
  auto traitOK = model.find<Trait>("trait");
  CHECK(traitOK != nullptr);

  model.declareTrait("trait1");
  auto trait1 = model.find<Trait>("trait1");
  CHECK(trait1 != nullptr);
  trait1->t("trait3");

  model.declareTrait("trait2");
  auto trait2 = model.find<Trait>("trait2");
  CHECK(trait2 != nullptr);
  trait2->t("trait3");

  model.declareTrait("trait3");
  auto trait3 = model.find<Trait>("trait3");
  CHECK(trait3 != nullptr);
  trait3->t("trait4");

  model.declareTrait("trait4");

  traitOK->sm("protoS1").r(Type::INT).p({"trait1", "trait4"});
  traitOK->sm("protoS1").r(Type::INT).p(Type::INT).p(Type::FLOAT).p(Type::UNSIGNED);

  traitOK->mm("protoM2").r(Type::INT).p({"trait1", "trait2"});
  traitOK->mm("protoM1").r(Type::INT).p({"trait"});

  CHECK(!traitOK->check());
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}