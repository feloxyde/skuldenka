/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_Module.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(Module)
{

  TEST(construction)
  {
    MockModule mod;
    CHECK_EQUAL(0, mod.components().size());
  }

  TEST(component_declaration)
  {
    MockModule mod;
    ErrorLog errs;

    // valid declarations
    errs += mod.declareTrait("trait");
    CHECK(!errs);
    CHECK_EQUAL(1, mod.components().size());

    errs += mod.declareImplem("implem", 0);
    CHECK(!errs);
    CHECK_EQUAL(2, mod.components().size());

    errs += mod.declareSubModule("sub");
    CHECK(!errs);
    CHECK_EQUAL(3, mod.components().size());

    errs += mod.declareProgram("program", "implem");
    CHECK(!errs);
    CHECK_EQUAL(4, mod.components().size());

    errs += mod.declareTrait("trait2");
    CHECK(!errs);
    CHECK_EQUAL(5, mod.components().size());

    CHECK(dynamic_pointer_cast<Trait>(mod.components().find("trait")->second)
          != nullptr); // check type as well
    CHECK(dynamic_pointer_cast<Implem>(mod.components().find("implem")->second)
          != nullptr); // check type as well
    CHECK(dynamic_pointer_cast<SubModule>(mod.components().find("sub")->second)
          != nullptr); // check type as well
    CHECK(dynamic_pointer_cast<Program>(mod.components().find("program")->second)
          != nullptr); // check type as well
    CHECK(dynamic_pointer_cast<Trait>(mod.components().find("trait2")->second)
          != nullptr); // check type as well

    // declaration fail, same name same type
    errs += mod.declareTrait("trait");
    CHECK(errs);
    CHECK(false); // check error type
    CHECK(errs);

    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Module::Error, name_already_in_use>()));
    CHECK_EQUAL(1, (errs.filter<Module::Error>().size()));
    auto errs1 = (*(errs.begin()))->subLog();
    CHECK_EQUAL(1, (errs1.filter<name_already_in_use>().size()));

    // declaration fail, same name different type
    errs += mod.declareImplem("program", 0);
    CHECK(errs);
    CHECK(false); // check error type
  }

  TEST(find)
  {
    MockModule mod;
    mod.declareTrait("trait1");
    mod.declareTrait("trait2");
    mod.declareTrait("trait3");

    mod.declareImplem("implem1", 0);
    mod.declareImplem("implem2", 0);

    mod.declareProgram("program1", "implem1");
    mod.declareProgram("program2", "implem2");
    mod.declareProgram("program3", "implem2");
    mod.declareProgram("program4", "implem2");

    // valid name valid type
    CHECK(mod.find<Trait>("trait1") != nullptr);
    CHECK(mod.find<Trait>("trait1")->name() == "trait1");

    CHECK(mod.find<Implem>("implem2") != nullptr);
    CHECK(mod.find<Implem>("implem2")->name() == "implem2");

    CHECK(mod.find<Component>("program4") != nullptr);
    CHECK(mod.find<Program>("program4")->name() == "program4");

    // valid name invalid type
    CHECK(mod.find<Program>("trait1") == nullptr);
    CHECK(mod.find<Implem>("program1") == nullptr);

    // invalid name
    CHECK(mod.find<Program>("toilets") == nullptr);
    CHECK(mod.find<Implem>("implem3") == nullptr);
    CHECK(mod.find<Component>("implem3") == nullptr);
  }

  TEST(asPath)
  {
    MockModule mod;
    CHECK_EQUAL(0, mod.asPath().size());
  }
}

SUITE(SubModule)
{

  TEST(construction)
  {
    MockModule mod;
    SubModule sub("sub", mod);

    CHECK_EQUAL("sub", sub.name());
    CHECK_EQUAL(0, sub.components().size());
    CHECK(&mod == &(sub.parent()));
  }

  TEST(asPath)
  {
    MockModule mod;

    mod.declareSubModule("sub1");
    auto sub1 = mod.find<SubModule>("sub1");

    sub1->declareSubModule("sub2");
    auto sub2 = sub1->find<SubModule>("sub2");

    sub2->declareSubModule("sub3");
    auto sub3 = sub2->find<SubModule>("sub3");

    sub3->declareSubModule("sub4");
    auto sub4 = sub3->find<SubModule>("sub4");

    sub4->declareSubModule("sub5");
    auto sub5 = sub4->find<SubModule>("sub5");

    auto path1 = sub1->asPath();
    CHECK_EQUAL(1, path1.size());
    CHECK_EQUAL("sub1", path1.front());

    auto path2 = sub2->asPath();
    CHECK_EQUAL(2, path2.size());
    CHECK_EQUAL("sub1", path2.front());
    CHECK_EQUAL("sub2", path2.back());

    auto path5 = sub5->asPath();
    CHECK_EQUAL(5, path5.size());
    auto pit = path5.begin();
    CHECK_EQUAL("sub1", *pit);
    ++pit;
    CHECK_EQUAL("sub2", *pit);
    ++pit;
    CHECK_EQUAL("sub3", *pit);
    ++pit;
    CHECK_EQUAL("sub4", *pit);
    ++pit;
    CHECK_EQUAL("sub5", *pit);
    ++pit;
    CHECK(pit == path5.end());
  }

  TEST(str)
  {
    Model mod;
    ErrorLog errs;
    errs = mod.declareSubModule("sub1");
    assert(!errs);
    auto sub1 = mod.find<SubModule>("sub1");
    assert(sub1);
    sub1->declareSubModule("sub2");
    auto sub1_1 = sub1->find<SubModule>("sub2");
    assert(sub1_1);
    sub1_1->declareSubModule("sub3");
    auto sub1_1_1 = sub1_1->find<SubModule>("sub3");
    assert(sub1_1_1);

    CHECK_EQUAL("::sub1", sub1->str());
    CHECK_EQUAL("::sub1::sub2", sub1_1->str());
    CHECK_EQUAL("::sub1::sub2::sub3", sub1_1_1->str());
  }
}

SUITE(Model)
{
  TEST(construction)
  {
    Model mod;
    CHECK_EQUAL(0, mod.components().size());
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}