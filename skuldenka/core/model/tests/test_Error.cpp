/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <iostream>
#include <memory>
#include <skuldenka/core/model/Error.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

SUITE(Error)
{

  class MockError : public Error
  {
  public:
    MockError()
    {}
    virtual ~MockError()
    {}
    virtual std::shared_ptr<Error> copy() const
    {
      return std::make_shared<MockError>();
    }
    virtual std::string str() const
    {
      return std::string("mockerrorstring");
    }
  };

  TEST(operatorStreamPush)
  {
    MockError err;
    ostringstream os;
    os << err;
    CHECK_EQUAL(std::string("mockerrorstring\n"), os.str());
  }

  TEST(operatorStreamPushWithSubs)
  {
    CHECK(false);
  }

  TEST(indentOutput)
  {
    CHECK(false);
  }

  TEST(operatorStreamInError)
  {
    CHECK(false);
  }

  TEST(operatorStreamInErrorLog)
  {
    CHECK(false);
  }
}

SUITE(ErrorLog)
{
  class MockErrorInt : public Error
  {
  public:
    MockErrorInt(int i) : m_i(i)
    {}
    virtual ~MockErrorInt()
    {}
    virtual std::shared_ptr<Error> copy() const
    {
      return std::make_shared<MockErrorInt>(m_i);
    }
    virtual std::string str() const
    {
      return std::to_string(m_i);
    }

  protected:
    int m_i;
  };

  TEST(construction)
  {
    ErrorLog errs;
    CHECK(!errs);
    CHECK_EQUAL(0, errs.size());

    ErrorLog errs1;
    errs1 << MockErrorInt(8) << MockErrorInt(3);
    ErrorLog errs2(errs1);

    CHECK_EQUAL(2, errs2.size());
    auto err2 = errs2.begin();
    CHECK_EQUAL(to_string(8), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(3), (*err2)->str());
    ++err2;
    CHECK(err2 == errs2.end());
  }

  TEST(constuctionFromError)
  {
    CHECK(false);
  }

  TEST(operatorPlus)
  {
    ErrorLog errs1;
    errs1 << MockErrorInt(8);
    errs1 << MockErrorInt(3);
    errs1 << MockErrorInt(2);
    errs1 << MockErrorInt(4);
    ErrorLog errs2;
    errs1 << MockErrorInt(5);
    errs1 << MockErrorInt(6);
    errs1 << MockErrorInt(7);
    errs1 << MockErrorInt(1);
    errs1 << MockErrorInt(9);

    ErrorLog errs = errs1 + errs2;

    CHECK_EQUAL(9, errs.size());

    auto err = errs.begin();
    CHECK_EQUAL(to_string(8), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(3), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(2), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(4), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(5), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(6), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(7), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(1), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(9), (*err)->str());
    ++err;
    CHECK(err == errs.end());
  }

  TEST(operatorPlusSelf)
  {
    ErrorLog errs;
    errs << MockErrorInt(8);
    errs << MockErrorInt(3);
    errs << MockErrorInt(2);
    errs << MockErrorInt(4);
    ErrorLog errs2;
    errs2 << MockErrorInt(5);
    errs2 << MockErrorInt(6);
    errs2 << MockErrorInt(7);
    errs2 << MockErrorInt(1);
    errs2 << MockErrorInt(9);

    errs += errs2;

    CHECK_EQUAL(9, errs.size());

    auto err = errs.begin();
    CHECK_EQUAL(to_string(8), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(3), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(2), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(4), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(5), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(6), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(7), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(1), (*err)->str());
    ++err;
    CHECK_EQUAL(to_string(9), (*err)->str());
    ++err;
    CHECK(err == errs.end());

    errs2 += errs;

    CHECK_EQUAL(14, errs2.size());

    auto err2 = errs2.begin();
    CHECK_EQUAL(to_string(5), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(6), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(7), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(1), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(9), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(8), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(3), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(2), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(4), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(5), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(6), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(7), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(1), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(9), (*err2)->str());
    ++err2;
    CHECK(err2 == errs2.end());
  }

  TEST(operatorAssign)
  {
    ErrorLog errs;
    errs << MockErrorInt(8);
    errs << MockErrorInt(3);
    errs << MockErrorInt(2);
    errs << MockErrorInt(4);
    ErrorLog errs2;
    errs2 << MockErrorInt(5);
    errs2 << MockErrorInt(6);
    errs2 << MockErrorInt(7);
    errs2 << MockErrorInt(1);
    errs2 << MockErrorInt(9);

    errs2 = errs;

    CHECK_EQUAL(4, errs2.size());

    auto err2 = errs2.begin();
    CHECK_EQUAL(to_string(8), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(3), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(2), (*err2)->str());
    ++err2;
    CHECK_EQUAL(to_string(4), (*err2)->str());
    ++err2;
    CHECK(err2 == errs2.end());
  }

  TEST(operatorStreamPush)
  {
    ErrorLog errs;
    errs << MockErrorInt(8);
    errs << MockErrorInt(3);
    errs << MockErrorInt(2);
    errs << MockErrorInt(4);

    ostringstream oserr;
    oserr << errs;
    ostringstream res;
    res << to_string(8) << std::endl
        << to_string(3) << std::endl
        << to_string(2) << std::endl
        << to_string(4) << std::endl;
    CHECK_EQUAL(res.str(), oserr.str());
  }

  TEST(operatorBool)
  {
    ErrorLog errs0;
    ErrorLog errs1;
    errs1 << MockErrorInt(120);
    ErrorLog errs5;
    errs5 << MockErrorInt(8);
    errs5 << MockErrorInt(3);
    errs5 << MockErrorInt(2);
    errs5 << MockErrorInt(4);
    errs5 << MockErrorInt(6);

    CHECK(!errs0);
    CHECK(errs1);
    CHECK(errs5);
  }

  class MockErrorInt2 : public MockErrorInt
  {
  public:
    MockErrorInt2(int i) : MockErrorInt::MockErrorInt(i)
    {}
    virtual ~MockErrorInt2()
    {}
    virtual std::shared_ptr<Error> copy() const
    {
      return std::make_shared<MockErrorInt2>(m_i);
    }
    virtual std::string str() const
    {
      return "MEI2 : " + this->MockErrorInt::str();
    }
  };

  class MockErrorOther : public Error
  {
  public:
    MockErrorOther()
    {}
    virtual ~MockErrorOther()
    {}
    virtual std::shared_ptr<Error> copy() const
    {
      return std::make_shared<MockErrorOther>();
    }
    virtual std::string str() const
    {
      return "MockErrorOther";
    }
  };

  TEST(filter)
  {
    ErrorLog errs;
    auto e1 = std::make_shared<MockErrorInt>(1);
    auto e2 = std::make_shared<MockErrorInt2>(2);
    auto e3 = std::make_shared<MockErrorInt>(3);
    auto e4 = std::make_shared<MockErrorOther>();
    auto e5 = std::make_shared<MockErrorInt2>(5);
    auto e6 = std::make_shared<MockErrorOther>();
    auto e7 = std::make_shared<MockErrorOther>();
    auto e8 = std::make_shared<MockErrorInt>(8);
    auto e9 = std::make_shared<MockErrorInt2>(9);
    auto e10 = std::make_shared<MockErrorInt2>(10);
    auto e11 = std::make_shared<MockErrorOther>();
    auto e12 = std::make_shared<MockErrorInt2>(12);

    errs << e1 << e2 << e3 << e4 << e5 << e6 << e7 << e8 << e9 << e10 << e11
         << e12;

    auto le1 = errs.filter<MockErrorInt>();
    auto le2 = errs.filter<MockErrorInt2>();
    auto leO = errs.filter<MockErrorOther>();

    CHECK_EQUAL(8, le1.size());
    auto it1 = le1.begin();
    CHECK((*(it1++)) == e1);
    CHECK((*(it1++)) == e2);
    CHECK((*(it1++)) == e3);
    CHECK((*(it1++)) == e5);
    CHECK((*(it1++)) == e8);
    CHECK((*(it1++)) == e9);
    CHECK((*(it1++)) == e10);
    CHECK((*(it1++)) == e12);
    CHECK(it1 == le1.end());

    CHECK_EQUAL(5, le2.size());
    auto it2 = le2.begin();
    CHECK((*(it2++)) == e2);
    CHECK((*(it2++)) == e5);
    CHECK((*(it2++)) == e9);
    CHECK((*(it2++)) == e10);
    CHECK((*(it2++)) == e12);
    CHECK(it2 == le2.end());

    CHECK_EQUAL(4, leO.size());
    auto itO = leO.begin();
    CHECK((*(itO++)) == e4);
    CHECK((*(itO++)) == e6);
    CHECK((*(itO++)) == e7);
    CHECK((*(itO++)) == e11);
    CHECK(itO == leO.end());
  }

  template <typename T>
  class MockTError : public Error
  {
  public:
    MockTError(T const& value) : m_value(value)
    {}
    virtual ~MockTError()
    {}

    virtual std::shared_ptr<Error> copy() const
    {
      return std::make_shared<MockTError<T>>(m_value);
    }
    virtual std::string str() const
    {
      stringstream ss;
      ss << m_value;
      return ss.str();
    }

  private:
    T m_value;
  };

  TEST(wrap)
  {
    ErrorLog errs;
    errs << MockTError<int>(0);
    errs << MockTError<float>(1.0f);
    errs << MockTError<string>("str");

    ErrorLog errs2 = errs.wrap<MockTError<string>>("str");

    CHECK_EQUAL(1, errs2.size());
    CHECK_EQUAL(1, errs2.filter<MockTError<string>>());
    auto err2 = errs2.begin();
    CHECK_EQUAL(3, (*err2)->subLog().size());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<int>>());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<float>>());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<string>>());
  }

  TEST(wrapIfNotEmpty)
  {
    ErrorLog errs;
    errs << MockTError<int>(0);
    errs << MockTError<float>(1.0f);
    errs << MockTError<string>("str");

    ErrorLog errs2 = errs.wrapIfNotEmpty<MockTError<string>>("str");

    CHECK_EQUAL(1, errs2.size());
    CHECK_EQUAL(1, errs2.filter<MockTError<string>>());
    auto err2 = errs2.begin();
    CHECK_EQUAL(3, (*err2)->subLog().size());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<int>>());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<float>>());
    CHECK_EQUAL(1, (*err2)->subLog().filter<MockTError<string>>());

    ErrorLog errs3;
    ErrorLog errs4 = errs3.wrapIfNotEmpty<MockTError<string>>("str");

    CHECK_EQUAL(0, errs4.size());
  }

  TEST(containsStructure)
  {

    /*
    errs2
      - string
        - float
          - int
          - int
      - float
      - int -3
        - unsigned
        - string
    */

    ErrorLog errs0;
    errs0 << MockTError<int>(1);
    errs0 << MockTError<int>(2);
    ErrorLog errs1 = errs0.wrap<MockTError<float>>(1.95f);
    ErrorLog errs2 = errs1.wrap<MockTError<std::string>>("string");
    errs2 << MockTError<float>(1.0f);
    ErrorLog errs3;
    errs3 << MockTError<std::string>("error");
    errs3 << MockTError<unsigned>(3);
    errs2 += errs3.wrap<MockTError<int>>(-3);
    ErrorLog errs4;

    CHECK((!errs4.containsStructure<MockTError<unsigned>>()));
    CHECK((!errs4.containsStructure<Error>()));
    CHECK((!errs2.containsStructure<MockTError<unsigned>>()));
    CHECK((!errs2.containsStructure<MockTError<int>, MockTError<float>>()));
    CHECK((!errs2.containsStructure<MockTError<int>, MockTError<string>,
                                    MockTError<float>, MockTError<int>,
                                    MockTError<int>>()));
    CHECK((!errs2.containsStructure<
            skuldenka::core::model::Error, skuldenka::core::model::Error,
            skuldenka::core::model::Error, skuldenka::core::model::Error,
            skuldenka::core::model::Error>()));

    CHECK((errs2.containsStructure<skuldenka::core::model::Error,
                                   skuldenka::core::model::Error,
                                   skuldenka::core::model::Error>()));

    CHECK((errs2.containsStructure<skuldenka::core::model::Error,
                                   skuldenka::core::model::Error>()));
    CHECK((errs2.containsStructure<MockTError<string>, MockTError<float>,
                                   MockTError<int>>()));
    CHECK((errs2.containsStructure<MockTError<string>, MockTError<float>>()));
    CHECK((errs2.containsStructure<MockTError<int>, MockTError<unsigned>>()));
    CHECK((errs2.containsStructure<MockTError<float>>()));
  }

  TEST(output_with_hierarchy)
  {
    /*
      string2(string1(1, 2)), string3 , -3(string4, 3))

    */

    ErrorLog errs0;
    errs0 << MockTError<int>(1);
    errs0 << MockTError<int>(2);
    ErrorLog errs1 = errs0.wrap<MockTError<std::string>>("string1");
    ErrorLog errs2 = errs1.wrap<MockTError<std::string>>("string2");
    errs2 << MockTError<std::string>("string3");
    ErrorLog errs3;
    errs3 << MockTError<std::string>("string4");
    errs3 << MockTError<unsigned>(3);
    errs2 += errs3.wrap<MockTError<int>>(-3);

    stringstream ss;
    ss << errs2;

    std::string expected;
    expected += "string2\n";
    expected += "\tstring1\n";
    expected += "\t\t1\n";
    expected += "\t\t2\n";
    expected += "string3\n";
    expected += "-3\n";
    expected += "\tstring4\n";
    expected += "\t3\n";

    CHECK(ss.str() == expected);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}