/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include <assert.h>
#include <iostream>
#include <memory>

#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/model/typedefs.hpp>
#include <string>
#include <unordered_set>
#include <vector>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::model;

//#FIXME check for implem error raise when new error architecture is implemented
// on a sub failure (as vtable check fail)

SUITE(check_Implem)
{

  class FixtureModel
  {
  public:
    FixtureModel()
    {
      model.declareTrait("trait1");
      model.declareTrait("trait2");
      auto trait2 = model.find<Trait>("trait2");
      assert(trait2 != nullptr);
      trait2->t("trait1").t("trait3");

      model.declareTrait("trait3");
      model.declareTrait("trait4");
      auto trait4 = model.find<Trait>("trait4");
      trait4->t("trait2").t("trait3");

      ErrorLog errs;
      errs += model.check();
      assert(!errs);
    }

    ~FixtureModel()
    {}

  public:
    Model model;
  };

  TEST_FIXTURE(FixtureModel, check_trait_invalid)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait5");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait5");

    ErrorLog errs;
    errs += implem->check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errs.filter<Implem::Error>()).size());
    auto errs2 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<AccessName<Trait>::Error>()).size());
    auto errs3 = errs2.front()->subLog();
    CHECK_EQUAL(1, (errs3.filter<component_not_found<Trait>>()));
  }

  TEST_FIXTURE(FixtureModel, check_duplicate_implem_request)
  {

    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2");

    implem->requestImplem("implem2").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    ErrorLog errs;
    errs += implem->check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, duplicate_implem_request>()));
    CHECK_EQUAL(
        1, (errs.front()->subLog().filter<duplicate_implem_request>()).size());
  }

  TEST_FIXTURE(FixtureModel, check_wrong_implem_request)
  {

    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait5").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait3");

    ErrorLog errs;
    errs += implem->check();
    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, ImplemRequest::Error,
                                  AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errs.filter<Implem::Error>()).size());
    auto errs2 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<ImplemRequest::Error>()).size());
    auto errs3 = errs2.front()->subLog();
    CHECK_EQUAL(1, (errs3.filter<AccessName<Trait>::Error>()).size());
    auto errs4 = errs3.front()->subLog();
    CHECK_EQUAL(1, (errs4.filter<component_not_found<Trait>>()).size());
  }

  TEST_FIXTURE(FixtureModel, check_wrong_trait_request)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait3");

    implem->requestTrait("trait1");
    implem->requestTrait("trait5");

    ErrorLog errs;
    errs += implem->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, TraitRequest::Error,
                                  AccessName<Trait>::Error,
                                  component_not_found<Trait>>()));

    CHECK_EQUAL(1, (errs.filter<Implem::Error>()).size());
    auto errs2 = errs.front()->subLog();
    CHECK_EQUAL(1, (errs2.filter<TraitRequest::Error>()).size());
    auto errs3 = errs2.front()->subLog();
    CHECK_EQUAL(1, (errs3.filter<AccessName<Trait>::Error>()).size());
    auto errs4 = errs3.front()->subLog();
    CHECK_EQUAL(1, (errs4.filter<component_not_found<Trait>>()).size());
  }

  TEST_FIXTURE(FixtureModel, check_too_many_vtables)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait3");
    implem->defineVtable("trait1");

    ErrorLog errs;
    errs += implem->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());

    CHECK((errs.containsStructure<Implem::Error, too_many_vtables>()));
    CHECK_EQUAL(1, (errs.front()->subLog().filter<too_many_vtables>()).size());
  }

  TEST_FIXTURE(FixtureModel, check_trait_has_no_vtable)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");

    ErrorLog errs;
    errs += implem->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, trait_has_no_vtable>()));
    CHECK_EQUAL(2, (errs.front()->subLog().filter<trait_has_no_vtable>()).size());
  }

  TEST_FIXTURE(FixtureModel, check_vtable_has_no_trait)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait3");
    implem->defineVtable("trait4");

    ErrorLog errs;
    errs += implem->check();

    CHECK(errs);
    CHECK_EQUAL(1, errs.size());
    CHECK((errs.containsStructure<Implem::Error, vtable_has_no_trait>()));
    CHECK_EQUAL(1, (errs.front()->subLog().filter<vtable_has_no_trait>()).size());
  }

  TEST(check_vtable_invalid_err_forwarding)
  {
    CHECK(false);
  }

  TEST_FIXTURE(FixtureModel, check_ok)
  {
    model.declareImplem("implem", 0);
    auto implem = model.find<Implem>("implem");
    CHECK(implem != nullptr);
    implem->t("trait1").t("trait2").t("trait3");

    implem->requestImplem("implem1").t("trait1").t("trait2");
    implem->requestImplem("implem2").t("trait2").t("trait3");

    implem->defineVtable("trait1");
    implem->defineVtable("trait2");
    implem->defineVtable("trait3");

    ErrorLog errs;
    errs += implem->check();

    CHECK(!errs);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}