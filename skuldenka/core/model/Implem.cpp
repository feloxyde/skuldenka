/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Implem.hpp"
#include "Method.hpp"
#include "Trait.hpp"
#include "Vtable.hpp"
#include <algorithm>
#include <limits>
#include <variant>

namespace skuldenka::core::model {
ImplemRequest::ImplemRequest(Implem const& implem, Identifier name) :
 NamedComponentOf<Implem, ImplemRequest>::NamedComponentOf(name, implem),
 m_traits()
{}

ImplemRequest::~ImplemRequest()
{}

ErrorLog ImplemRequest::check() const
{
  ErrorLog errs;

  for (auto const& tname : m_traits) {
    errs += tname.check();
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

std::string ImplemRequest::str() const
{
  std::string desc = "ImplemRequest " + m_name;
  return desc;
}

std::unordered_set<std::shared_ptr<Trait>> ImplemRequest::unpack() const
{
  bool incomplete;
  return unpack(incomplete);
}

std::unordered_set<std::shared_ptr<Trait>> ImplemRequest::unpack(bool& incomplete) const
{
  std::unordered_set<std::shared_ptr<Trait>> traits;

  for (auto const& tname : m_traits) {
    auto trait = tname.find();
    if (!trait) {
      incomplete = true;
    } else {
      auto upack = tname.find()->unpack(incomplete);
      traits.insert(upack.begin(), upack.end());
    }
  }
  return traits;
}

/* ---------------------------- */

TraitRequest::~TraitRequest()
{}

ErrorLog TraitRequest::check() const
{
  ErrorLog errs;

  errs += m_trait.check();

  return errs.wrapIfNotEmpty<Error>(this->str());
}

std::string TraitRequest::str() const
{
  std::string desc = "TraitRequest ";

  desc += m_trait.str();

  return desc;
}

AccessName<Trait> const& TraitRequest::trait()
{
  return m_trait;
}

/* ----------------------------- */

Implem::Implem(Module const& module, Identifier name, SlotCount slotCount) :
 ModuleComponent<Implem>::ModuleComponent(name, module),
 m_slotCount(slotCount),
 m_traits(),
 m_vtables(),
 m_implemRequests(),
 m_traitRequests()
{}

Implem::~Implem()
{}

void Implem::defineDestructor(std::shared_ptr<Destructor> destructor)
{
  m_destructor = destructor;
}

ImplemRequest& Implem::requestImplem(Identifier name)
{
  m_implemRequests.push_back(std::make_unique<ImplemRequest>(*this, name));
  return *(m_implemRequests.back());
}

ErrorLog Implem::checkLinkRequests() const
{
  ErrorLog errs;

  // check unicity of implemRequests by name
  for (auto it = m_implemRequests.begin(); it != m_implemRequests.end();) {
    auto const& request = *it;
    if (std::find_if(++it, m_implemRequests.end(),
                     [&request](std::unique_ptr<ImplemRequest> const& val) {
                       return request->name() == val->name();
                     })
        != m_implemRequests.end()) {
      errs << duplicate_implem_request(request->name(), this->str());
    }
  }

  // check every request (both implem and trait)
  for (auto const& il : m_implemRequests) {
    errs += il->check();
  }

  for (auto const& tl : m_traitRequests) {
    errs += tl->check();
  }

  return errs;
}

ErrorLog Implem::check() const
{
  ErrorLog errs;

  for (auto const& rname : m_traits) {
    errs += rname.check();
  }

  if (errs) {
    return errs.wrapIfNotEmpty<Error>(this->str());
  }

  // check request requests
  errs += checkLinkRequests();

  if (m_destructor) {
    errs += m_destructor->check();
  }

  // listing traits, this can only be done if traits are valid
  auto traits = this->unpack();

  // checking for each trait that there is only one vtable
  for (auto const& trait : traits) {
    size_t count = std::count_if(m_vtables.begin(), m_vtables.end(),
                                 [&trait](std::unique_ptr<Vtable> const& val) {
                                   return AccessName<Trait>::areEquivalent(
                                       trait->absoluteName(), val->trait());
                                 });
    if (count == 0) {
      errs << trait_has_no_vtable(trait->str(), this->str());
    } else if (count >= 2) {
      errs << too_many_vtables(trait->str(), this->str());
    }
  }

  // check for each vtable there is a trait
  for (auto const& vtable : m_vtables) {
    if (std::find_if(traits.begin(), traits.end(),
                     [&vtable](std::shared_ptr<Trait> const& val) {
                       return AccessName<Trait>::areEquivalent(
                           vtable->trait(), val->absoluteName());
                     })
        == traits.end()) {
      errs << vtable_has_no_trait(vtable->str(), this->str());
    }
  }

  for (auto const& mt : m_vtables) {
    errs += mt->check();
  }

  //#FIXME check for signals when implemented  ? !
  return errs.wrapIfNotEmpty<Error>(this->str());
} // namespace skuldenka

std::string Implem::str() const
{
  return this->name();
}

std::vector<std::unique_ptr<Vtable>> const& Implem::vtables() const
{
  return m_vtables;
}

std::vector<std::unique_ptr<ImplemRequest>> const& Implem::implemRequests() const
{
  return m_implemRequests;
}

std::vector<std::unique_ptr<TraitRequest>> const& Implem::traitRequests() const
{
  return m_traitRequests;
}

std::unordered_set<std::shared_ptr<Trait>> Implem::unpack() const
{
  bool incomplete;
  return unpack(incomplete);
}

std::unordered_set<std::shared_ptr<Trait>> Implem::unpack(bool& incomplete) const
{
  std::unordered_set<std::shared_ptr<Trait>> traits;

  for (auto const& tname : m_traits) {
    auto trait = tname.find();
    if (!trait) {
      incomplete = true;
    } else {
      auto upack = tname.find()->unpack(incomplete);
      traits.insert(upack.begin(), upack.end());
    }
  }
  return traits;
}

/* ---------- */
duplicate_implem_request::duplicate_implem_request(
    std::string const& requestName, std::string const& implemname) noexcept :
 Implem::Error::Error(implemname), m_requestName(requestName)
{}

duplicate_implem_request::duplicate_implem_request(
    duplicate_implem_request const& other) noexcept :
 duplicate_implem_request::duplicate_implem_request(other.m_requestName,
                                                    other.componentStr())
{}

duplicate_implem_request::~duplicate_implem_request()
{}

std::shared_ptr<skuldenka::core::model::Error> duplicate_implem_request::copy() const
{
  return std::make_shared<duplicate_implem_request>(m_requestName,
                                                    this->componentStr());
}

std::string duplicate_implem_request::str() const
{
  std::string msg = "Implem request " + m_requestName
                    + " requested more than once in implem "
                    + this->componentStr();
  return msg;
}

std::string duplicate_implem_request::name()
{
  return m_requestName;
}

/* ---------- */

too_many_vtables::too_many_vtables(std::string const& traitname,
                                   std::string const& implemname) noexcept :
 Implem::Error::Error(implemname), m_traitname(traitname)
{}

too_many_vtables::too_many_vtables(too_many_vtables const& other) noexcept :
 too_many_vtables(other.m_traitname, other.componentStr())
{}

too_many_vtables::~too_many_vtables()
{}

std::shared_ptr<skuldenka::core::model::Error> too_many_vtables::copy() const
{
  return std::make_shared<too_many_vtables>(m_traitname, this->componentStr());
}

std::string too_many_vtables::str() const
{
  std::string msg = "Implem " + this->componentStr()
                    + " provides more than one vtable for trait " + m_traitname;
  return msg;
}

std::string too_many_vtables::name()
{
  return m_traitname;
}

/* ---------- */
trait_has_no_vtable::trait_has_no_vtable(std::string const& traitname,
                                         std::string const& implemname) noexcept :
 Implem::Error::Error(implemname), m_traitname(traitname)
{}

trait_has_no_vtable::trait_has_no_vtable(trait_has_no_vtable const& other) noexcept :
 trait_has_no_vtable::trait_has_no_vtable(other.m_traitname, other.componentStr())
{}

trait_has_no_vtable::~trait_has_no_vtable()
{}

std::shared_ptr<skuldenka::core::model::Error> trait_has_no_vtable::copy() const
{
  return std::make_shared<trait_has_no_vtable>(m_traitname, this->componentStr());
}

std::string trait_has_no_vtable::str() const
{
  std::string msg = "Implem " + this->componentStr()
                    + " declared as implementing trait " + m_traitname
                    + " but no vtable is provided";
  return msg;
}

std::string trait_has_no_vtable::name()
{
  return m_traitname;
}

/* ---------- */

vtable_has_no_trait::vtable_has_no_trait(std::string const& vtablename,
                                         std::string const& implemname) noexcept :
 Implem::Error::Error(implemname), m_vtablename(vtablename)
{}

vtable_has_no_trait::vtable_has_no_trait(vtable_has_no_trait const& other) noexcept :
 vtable_has_no_trait::vtable_has_no_trait(other.m_vtablename,
                                          other.componentStr())
{}

vtable_has_no_trait::~vtable_has_no_trait()
{}

std::shared_ptr<skuldenka::core::model::Error> vtable_has_no_trait::copy() const
{
  return std::make_shared<vtable_has_no_trait>(m_vtablename,
                                               this->componentStr());
}

std::string vtable_has_no_trait::str() const
{
  std::string msg = "Implem " + this->componentStr()
                    + " provides vtable for not implemented trait "
                    + m_vtablename;
  return msg;
}

std::string vtable_has_no_trait::name()
{
  return m_vtablename;
}
} // namespace skuldenka::core::model