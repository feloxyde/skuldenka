/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Method.hpp"
#include "Trait.hpp"
#include <algorithm>
#include <assert.h>
#include <limits>

namespace skuldenka::core::model {

Trait::Trait(Module const& module, Identifier name) :
 // not yet processed
 ModuleComponent<Trait>::ModuleComponent(name, module),
 m_requirements(),
 m_methods(),
 m_staticMethods()
{}

Trait::~Trait()
{}

AccessNameSet<Trait> const& Trait::requirements() const
{
  return m_requirements;
}

ErrorLog Trait::check() const
{

  ErrorLog errs;

  errs += this->verifyRequirements();

  errs += checkNoDuplicateMethod<MemberMethod>();
  errs += checkNoDuplicateMethod<StaticMethod>();

  // now check each prototypes independently
  for (auto const& sm : m_staticMethods) {
    errs += sm->check();
  }

  for (auto const& mm : m_methods) {
    errs += mm->check();
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

std::string Trait::str() const
{
  return this->name();
}

bool Trait::check(std::shared_ptr<Implem> implem)
{
  throw not_yet_implemented("Trait::checkImplem");
  return false; //#FIXME Implement this when implementing tests !
} // check if implem is matching Trait prototypes and test set

/* TEMPLATE SPECIALIZATIONS */

template <>
std::vector<std::shared_ptr<Prototype<MemberMethod>>> const&
Trait::methods<MemberMethod>() const
{
  return m_methods;
}
template <>
std::vector<std::shared_ptr<Prototype<StaticMethod>>> const&
Trait::methods<StaticMethod>() const
{
  return m_staticMethods;
}

std::unordered_set<std::shared_ptr<Trait>> Trait::listRequirements() const
{
  bool incomplete;
  return listRequirements(incomplete);
}

std::unordered_set<std::shared_ptr<Trait>>
Trait::listRequirements(bool& incomplete) const
{
  std::unordered_set<std::shared_ptr<Trait>> reqs;
  for (auto const& rname : m_requirements) {
    auto ptr = rname.find();
    if (ptr) {
      reqs.emplace(ptr);
    } else {
      incomplete = true;
    }
  }
  return reqs;
}

std::unordered_set<std::shared_ptr<Trait>> Trait::unpack() const
{
  bool incomplete;
  return unpack(incomplete);
}

std::unordered_set<std::shared_ptr<Trait>> Trait::unpack(bool& incomplete) const
{
  //#FIXME it supposes that there is no circular requirements !!! we need to
  // ensure an exclusion list in order to avoid infinite recursion !!!
  std::unordered_set<std::shared_ptr<Trait>> reqs;
  reqs.emplace(this->module().find<Trait>(this->name()));
  for (auto const& req : this->listRequirements(incomplete)) {
    auto subset = req->unpack();
    reqs.insert(subset.begin(), subset.end());
  }
  return reqs;
}

ErrorLog Trait::verifyRequirements() const
{
  ErrorLog errs;
  for (auto const& rname : requirements()) {
    ErrorLog errsub = rname.check();
    if (!errsub) {
      errs += rname.find()->verifyRequirements(this->absoluteName(),
                                               this->absoluteName());
    } else {
      errs += errsub;
    }
  }
  return errs;
}

ErrorLog Trait::verifyRequirements(AccessName<Trait> const& basename,
                                   AccessName<Trait> const& previous) const
{
  ErrorLog errs;

  if (AccessName<Trait>::areEquivalent(this->absoluteName(), basename)) {
    errs << circular_trait_dependency(basename.str(), previous.str());

  } else {
    for (auto rname : requirements()) {
      ErrorLog errsub = rname.check();
      if (!errsub) {
        errs += rname.find()->verifyRequirements(basename, this->absoluteName());
      }
    }
  }
  return errs;
}

// quick declare member method
Prototype<MemberMethod>& Trait::mm(Identifier name)
{
  return this->declareMethod<MemberMethod>(name);
}

// quick declare static method
Prototype<StaticMethod>& Trait::sm(Identifier name)
{
  return this->declareMethod<StaticMethod>(name);
}

template <>
std::vector<std::shared_ptr<Prototype<MemberMethod>>>& Trait::methods()
{
  return m_methods;
}

template <>
std::vector<std::shared_ptr<Prototype<StaticMethod>>>& Trait::methods()
{
  return m_staticMethods;
}

/* ------ */

method_redeclaration::method_redeclaration(std::string const& name,
                                           std::string const& tname) noexcept :
 Trait::Error::Error(tname), m_name(name)
{}

method_redeclaration::method_redeclaration(method_redeclaration const& other) noexcept :
 method_redeclaration::method_redeclaration(other.m_name, other.componentStr())
{}

method_redeclaration::~method_redeclaration()
{}

std::shared_ptr<skuldenka::core::model::Error> method_redeclaration::copy() const
{
  return std::make_shared<method_redeclaration>(m_name, this->componentStr());
}

std::string method_redeclaration::str() const
{
  std::string message = "Method " + m_name + " declared more than once in trait "
                        + this->componentStr();
  return message;
}

std::string method_redeclaration::name()
{
  return m_name;
}

/* ----- */

circular_trait_dependency::circular_trait_dependency(
    std::string const& name, std::string const& tname) noexcept :
 Trait::Error::Error(tname), m_name(name)
{}

circular_trait_dependency::circular_trait_dependency(
    circular_trait_dependency const& other) noexcept :
 circular_trait_dependency::circular_trait_dependency(other.m_name,
                                                      other.componentStr())
{}

circular_trait_dependency::~circular_trait_dependency()
{}

std::shared_ptr<skuldenka::core::model::Error> circular_trait_dependency::copy() const
{
  return std::make_shared<circular_trait_dependency>(m_name,
                                                     this->componentStr());
}

std::string circular_trait_dependency::str() const
{
  std::string message = "Traits " + m_name + " and " + this->componentStr()
                        + " are trying to require each other.";
  return message;
}

std::string circular_trait_dependency::name()
{
  return m_name;
}

} // namespace skuldenka::core::model