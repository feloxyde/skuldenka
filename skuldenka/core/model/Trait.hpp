/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_TRAIT_HPP
#define SKULDENKA_CORE_MODEL_TRAIT_HPP

#include "Component.hpp"
//#include "Resource.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <algorithm>
#include <initializer_list>
#include <map>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

namespace skuldenka::core::runtime {
class ImplemNode;
class Instance;
} // namespace skuldenka::core::runtime

namespace skuldenka::core::model {

class Implem;
class Adapter;

template <typename ThisType>
class Method;

typedef Method<runtime::ImplemNode> StaticMethod;
typedef Method<runtime::Instance> MemberMethod;

template <typename ThisType>
class Method;

template <typename methodType>
class Prototype;

class Trait final : public ModuleComponent<Trait>
{
public:
  Trait(Module const& module, Identifier name);
  virtual ~Trait();

  template <typename... TraitAccessNameParams>
  void requireTrait(TraitAccessNameParams... traitAccessNameArgs);

  template <typename... TraitAccessNameParams>
  Trait& t(TraitAccessNameParams... traitAccessNameArgs);

  AccessNameSet<Trait> const& requirements() const; // change return type

  template <typename methodType = MemberMethod>
  std::vector<std::shared_ptr<Prototype<methodType>>> const& methods() const;
  // std::vector<std::shared_ptr<Prototype<StaticMethod>>> const& staticMethods() const;

  template <typename methodType = MemberMethod>
  std::vector<std::shared_ptr<Prototype<methodType>>>& methods();

  template <typename methodType = MemberMethod>
  std::shared_ptr<Prototype<methodType>>
  findMethod(Prototype<methodType> const& proto, bool local = true) const;

  template <typename methodType = MemberMethod>
  std::unordered_set<std::shared_ptr<Prototype<methodType>>>
  findMethods(Prototype<methodType> const& proto, bool local = true) const;

  virtual ErrorLog check() const;
  virtual std::string str() const;

  void declare(std::shared_ptr<Implem> implem);

  template <typename methodType = MemberMethod>
  Prototype<methodType>& declareMethod(Identifier name);

  // quick declare member method
  Prototype<MemberMethod>& mm(Identifier name);

  // quick declare static method
  Prototype<StaticMethod>& sm(Identifier name);

  bool check(std::shared_ptr<Implem>
                 implem); // check if implem is matching Trait prototypes and test set

  std::unordered_set<std::shared_ptr<Trait>> listRequirements() const;
  std::unordered_set<std::shared_ptr<Trait>> listRequirements(bool& incomplete) const;

  std::unordered_set<std::shared_ptr<Trait>> unpack() const;
  std::unordered_set<std::shared_ptr<Trait>> unpack(bool& incomplete) const;

  typedef ModuleComponent<Trait>::Error Error;

private:
  ErrorLog verifyRequirements() const;
  ErrorLog verifyRequirements(AccessName<Trait> const& basename,
                              AccessName<Trait> const& previous) const;
  template <typename methodType = MemberMethod>
  ErrorLog checkNoDuplicateMethod() const;

private:
private:
  AccessNameSet<Trait> m_requirements;
  std::vector<std::shared_ptr<Prototype<MemberMethod>>> m_methods;
  std::vector<std::shared_ptr<Prototype<StaticMethod>>> m_staticMethods;
  // add implem verifs here
};

class method_redeclaration : public Trait::Error
{
public:
  method_redeclaration(std::string const& name, std::string const& tname) noexcept;
  method_redeclaration(method_redeclaration const& other) noexcept;

  virtual ~method_redeclaration();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_name;
};

class circular_trait_dependency : public Trait::Error
{
public:
  circular_trait_dependency(std::string const& name,
                            std::string const& tname) noexcept;
  circular_trait_dependency(circular_trait_dependency const& other) noexcept;

  virtual ~circular_trait_dependency();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_name;
};

/* TEMPLATE IMPLEM */

template <typename... TraitAccessNameParams>
void Trait::requireTrait(TraitAccessNameParams... traitAccessNameArgs)
{
  m_requirements.emplace(*this, traitAccessNameArgs...);
}

template <typename... TraitAccessNameParams>
Trait& Trait::t(TraitAccessNameParams... traitAccessNameArgs)
{
  this->requireTrait(traitAccessNameArgs...);
  return *this;
}

template <typename methodType>
std::shared_ptr<Prototype<methodType>>
Trait::findMethod(Prototype<methodType> const& proto, bool local) const
{
  auto it = std::find_if(
      this->methods<methodType>().begin(), this->methods<methodType>().end(),
      [&proto](std::shared_ptr<methodType> const& val) { proto == *val; });

  if (it != this->methods<methodType>().end()) {
    return *it;
  } else if (local) {
    return std::shared_ptr<Prototype<methodType>>();
  }

  auto reqs = this->listRequirements();
  for (auto& req : reqs) {
    auto ptr = req->findMethod(proto);
    if (ptr) {
      return ptr;
    }
  }

  return std::shared_ptr<Prototype<methodType>>();
}

template <typename methodType>
std::unordered_set<std::shared_ptr<Prototype<methodType>>>
Trait::findMethods(Prototype<methodType> const& proto, bool local) const
{
  throw not_yet_implemented("Trait::findMethods");
}

template <typename methodType>
Prototype<methodType>& Trait::declareMethod(Identifier name)
{
  auto proto = std::make_shared<Prototype<methodType>>(*this, name);
  this->methods<methodType>().push_back(proto);
  return *proto;
}

template <typename methodType>
ErrorLog Trait::checkNoDuplicateMethod() const
{

  ErrorLog errs;
  for (auto it = methods<methodType>().begin();
       it != methods<methodType>().end();) {
    auto prototype = *it;
    if (find_if(++it, methods<methodType>().end(),
                [&prototype](std::shared_ptr<Prototype<methodType>> val) {
                  return (*val) == (*prototype);
                })
        != methods<methodType>().end()) {
      errs << method_redeclaration(prototype->str(), this->name());
    }
  }
  return errs;
}

} // namespace skuldenka::core::model

#endif
