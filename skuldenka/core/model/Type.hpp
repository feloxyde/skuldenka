/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_TYPE_HPP
#define SKULDENKA_CORE_MODEL_TYPE_HPP

#include "Component.hpp"
#include "typedefs.hpp"
#include <initializer_list>
#include <memory>
#include <string>
#include <typeindex>
#include <unordered_set>

namespace skuldenka::core::runtime {
class Instance;
}

namespace skuldenka::core::model {

class Trait;
class Implem;

class Type final : public ComponentOf<Component, Type>
{
public:
  enum Primary
  {
    //#FIXME maybe simply remove those and replace them by fully trait-characterized types ?
    // all base Types, maybe add some more later
    // integers like
    CHAR,
    SHORT,
    INT,
    UNSIGNED,
    LONG,
    UNSIGNED_LONG,
    // float like
    FLOAT,
    DOUBLE,
    // composite Types
    STRING,
    // pointers, only two possible
    IMPLEM,   // Pointer to an implem, not sure if needed ?
    INSTANCE, // pointer to an instance
    IMPLEM_WEAK,
    INSTANCE_WEAK,
    //"abstract" Types : cannot be passed through Variant
    TRAITS, // List of traits
    VOID    // no value
  };

public:
  template <typename T>
  static constexpr bool isAllowed();

  template <typename T>
  static constexpr Type::Primary id();

public:
  Type(Component const& owner, Type::Primary const& primary);
  Type(Component const& owner, AccessNameSet<Trait> const& traitsNames);
  Type(Component const& owner,
       std::unordered_set<std::string> const& namestrings);
  Type(Type const& origin);
  Type(Component const& newOwner, Type const& origin);

  virtual ~Type();

  bool isAbstract();

  bool operator==(Type const& other) const;
  bool operator!=(Type const& other) const;
  Type::Primary primary() const;

  // #FIXME add error code etc
  std::unordered_set<std::shared_ptr<Trait>> const& listTraits() const;

  AccessNameSet<Trait> const& traits() const;

  virtual ErrorLog check() const;
  virtual std::string str() const;

  typedef ComponentOf<Component, Type>::Error Error;

  Type& operator=(Type const& origin);

private:
  Primary m_primary;
  //#FIXME make a triple possibility ? like  primary, name or trait ptr ?
  AccessNameSet<Trait> m_traits;
};
/* DISABLED BECAUSE IRRELEVANT WITH NEW ARCH
template <typename T>
Type type()
{
  return Type(Type::id<T>());
}
*/
/*** TEMPLATE IMPLEM ***/

template <>
constexpr bool Type::isAllowed<char>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<short>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<int>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<unsigned>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<long>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<unsigned long>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<float>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<double>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<std::string>()
{
  return true;
}

//#FIXME this is invalid should be runtime::implemnode!
template <>
constexpr bool Type::isAllowed<std::shared_ptr<Implem>>()
{
  return true;
}

template <>
constexpr bool
Type::isAllowed<std::shared_ptr<skuldenka::core::runtime::Instance>>()
{
  return true;
}

//#FIXME this is invalid should be runtime::implemnode!
template <>
constexpr bool Type::isAllowed<std::weak_ptr<Implem>>()
{
  return true;
}

template <>
constexpr bool Type::isAllowed<std::weak_ptr<skuldenka::core::runtime::Instance>>()
{
  return true;
}

/* id */
/*
template <typename T>
constexpr Type::Primary Type::id()
{
  return Type::VOID;
}
*/

template <>
constexpr Type::Primary Type::id<char>()
{
  return Type::CHAR;
}

template <>
constexpr Type::Primary Type::id<short>()
{
  return Type::SHORT;
}

template <>
constexpr Type::Primary Type::id<int>()
{
  return Type::INT;
}

template <>
constexpr Type::Primary Type::id<unsigned>()
{
  return Type::UNSIGNED;
}

template <>
constexpr Type::Primary Type::id<long>()
{
  return Type::LONG;
}

template <>
constexpr Type::Primary Type::id<unsigned long>()
{
  return Type::UNSIGNED_LONG;
}

template <>
constexpr Type::Primary Type::id<float>()
{
  return Type::FLOAT;
}

template <>
constexpr Type::Primary Type::id<double>()
{
  return Type::DOUBLE;
}

template <>
constexpr Type::Primary Type::id<std::string>()
{
  return Type::STRING;
}

//#FIXME this is invalid should be runtime::implemnode!
template <>
constexpr Type::Primary Type::id<std::shared_ptr<Implem>>()
{
  return Type::IMPLEM;
}

template <>
constexpr Type::Primary
Type::id<std::shared_ptr<skuldenka::core::runtime::Instance>>()
{
  return Type::INSTANCE;
}

//#FIXME this is invalid should be runtime::implemnode!
template <>
constexpr Type::Primary Type::id<std::weak_ptr<Implem>>()
{
  return Type::IMPLEM_WEAK;
}

template <>
constexpr Type::Primary
Type::id<std::weak_ptr<skuldenka::core::runtime::Instance>>()
{
  return Type::INSTANCE_WEAK;
}

template <>
constexpr Type::Primary Type::id<void>()
{
  return Type::VOID;
}

/*
----
*/

} // namespace skuldenka::core::model

#endif
