/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_EXCEPTION_CPP
#define SKULDENKA_CORE_EXCEPTION_CPP

#include <cstring>
#include <exception>
#include <string>

namespace skuldenka::core {
class exception : public std::exception
{
public:
  exception() noexcept
  {}

  exception(exception const&) noexcept
  {}

  exception& operator=(exception const&) noexcept
  {
    return *this;
  }

  virtual ~exception()
  {}

  virtual std::string str() const noexcept
  {
    return "skuldenka::exception";
  }

  virtual char const* what() const noexcept
  {
    std::string message = this->str();
    char* copy = static_cast<char*>(malloc(sizeof(char) * (message.size() + 1)));
    strcpy(copy, message.c_str());
    return copy;
  }
};

class not_yet_implemented : public exception
{
public:
  not_yet_implemented(std::string const& name) : m_name(name)
  {}
  not_yet_implemented(exception const&) noexcept
  {}

  not_yet_implemented& operator=(not_yet_implemented const& other) noexcept
  {
    this->m_name = other.m_name;
    return *this;
  }

  virtual ~not_yet_implemented()
  {}

  virtual std::string str() const noexcept
  {
    std::string message("skuldenka::not_yet_implemented : ");
    message += m_name;
    return message;
  }

  std::string name()
  {
    return m_name;
  }

public:
  std::string m_name;
};

} // namespace skuldenka::core

#endif