/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Program.hpp"
#include "exception.hpp"
#include <algorithm>
#include <cassert>

namespace skuldenka::core::model {

Program::~Program()
{}

ImplemAssign& Program::implem()
{
  return m_implem;
}

AccessName<Trait> const& Program::trait() const
{
  return m_trait;
}

Identifier const& Program::method() const
{
  return m_method;
}

ErrorLog Program::check() const
{
  // check that main Implem is OK,
  // then check that mainMethod is present in main implem and matches the
  // "program launch" (void -> void) prototype
  //#FIXME need to later enable other types of prototypes for entry

  ErrorLog errs;

  errs += m_implem.check();

  if (errs) {
    return errs.wrapIfNotEmpty<Error>(this->str());
  }

  auto implemptr = m_implem.implem().find();
  assert(implemptr);

  // trying to find the vtable corresponding to trait of entrypoint

  auto vtable = std::find_if(
      implemptr->vtables().begin(), implemptr->vtables().end(),
      [this](std::unique_ptr<Vtable> const& val) {
        return AccessName<Trait>::areEquivalent(this->m_trait, val->trait());
      });

  if (vtable == implemptr->vtables().end()) {
    errs << main_method_trait_not_implemented(this->str(), m_trait.str(),
                                              implemptr->name());

    return errs.wrapIfNotEmpty<Error>(this->str());
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

std::string Program::str() const
{
  std::string msg = "program " + this->name();
  return msg;
}

/* ------ */

main_method_trait_not_implemented::main_method_trait_not_implemented(
    std::string const& programStr, std::string const& traitStr,
    std::string const& implemStr) noexcept :
 Program::Error::Error(programStr), m_traitStr(traitStr), m_implemStr(implemStr)
{}

main_method_trait_not_implemented::main_method_trait_not_implemented(
    main_method_trait_not_implemented const& other) noexcept :
 main_method_trait_not_implemented::main_method_trait_not_implemented(
     other.componentStr(), other.m_traitStr, other.m_implemStr)
{}

main_method_trait_not_implemented::~main_method_trait_not_implemented()
{}

std::shared_ptr<skuldenka::core::model::Error>
main_method_trait_not_implemented::copy() const
{
  return std::make_shared<main_method_trait_not_implemented>(*this);
}

std::string main_method_trait_not_implemented::str() const
{
  std::string msg = "Program " + this->componentStr()
                    + " requires main method of trait " + m_traitStr
                    + " but main implem " + m_implemStr
                    + " does not implement it";
  return msg;
}
} // namespace skuldenka::core::model
