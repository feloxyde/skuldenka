/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_METHOD_HPP
#define SKULDENKA_CORE_MODEL_METHOD_HPP

#include "Component.hpp"
#include "Trait.hpp"
#include "Type.hpp"
#include "Variant.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <assert.h>
#include <memory>
#include <string.h>
#include <typeinfo>
#include <variant>
#include <vector>

namespace skuldenka::core::runtime {

class ImplemNode;
class Instance;
} // namespace skuldenka::core::runtime

namespace skuldenka::core::model {

class Implem;
class Vtable;

template <typename ThisType>
class Method;

template <typename methodType = MemberMethod>
class Prototype final : public NamedComponentOf<Component, Prototype<methodType>>
{
public:
  // add a way to add args later on instead of directly adding them !
  Prototype(Component const& owner, Identifier const& name);
  Prototype(Prototype<methodType> const& origin);
  ~Prototype();

  bool operator==(Prototype<methodType> const& other) const;
  bool operator!=(Prototype<methodType> const& other) const;

  void setReturnType(Type::Primary const& primary);
  void setReturnType(AccessNameSet<Trait> const& traits);
  void setReturnType(Type const& origin);

  void addParameter(Type::Primary const& primary);
  void addParameter(AccessNameSet<Trait> const& traits);
  void addParameter(Type const& origin);

  Prototype<methodType>& r(Type::Primary const& primary);
  Prototype<methodType>& r(Type const& origin);
  Prototype<methodType>& r(std::unordered_set<std::string> const& namestrings);

  Prototype<methodType>& p(Type::Primary const& primary);
  Prototype<methodType>& p(Type const& origin);
  Prototype<methodType>& p(std::unordered_set<std::string> const& namestrings);

  Type const& returnType() const;
  std::vector<std::shared_ptr<Type>> const& parameters();

  virtual ErrorLog check() const;
  virtual std::string str() const;

  typedef
      typename NamedComponentOf<Component, Prototype<methodType>>::Error Error;

private:
  std::shared_ptr<Type> m_returnType;
  std::vector<std::shared_ptr<Type>> m_parameters;
};

template <typename ThisType>
class Method final : public ComponentOf<Vtable, Method<ThisType>>
{
public:
  class Body
  {
  public:
    Body(){};
    virtual ~Body(){};

    virtual std::shared_ptr<Body> copy() = 0;

    virtual Variant operator()(std::shared_ptr<ThisType> thisptr) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 1> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 2> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 3> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 4> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 5> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 6> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 7> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 8> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 9> const& args) = 0;
    virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                               std::array<Variant, 10> const& args) = 0;
  };

public:
  Method(Vtable const& vtable, Identifier const& name,
         std::shared_ptr<Method<ThisType>::Body>&& body);
  virtual ~Method();

  Vtable const& vtable();

  virtual ErrorLog check() const;
  virtual std::string str() const;

  Prototype<Method<ThisType>> const& prototype() const;
  Prototype<Method<ThisType>>& prototype();

  std::shared_ptr<Method<ThisType>::Body> const& body() const;

  typedef typename ComponentOf<Vtable, Method<ThisType>>::Error Error;

private:
  Prototype<Method<ThisType>> m_prototype;
  std::shared_ptr<Method<ThisType>::Body> m_body;
};

// DOESNT WORK BECAUSE OF USING STATICMETHOD directly inside
typedef Method<runtime::ImplemNode> StaticMethod;
typedef Method<runtime::Instance> MemberMethod;

template <typename ThisType>
class undeclared_method_definition : public Method<ThisType>::Error
{
public:
  undeclared_method_definition(Identifier name, Identifier vtable) noexcept :
   Method<ThisType>::Error::Error(name), m_vtable(vtable)
  {}

  undeclared_method_definition(undeclared_method_definition const& other) noexcept :
   undeclared_method_definition::undeclared_method_definition(
       other.componentStr(), other.m_vtable)
  {}

  virtual ~undeclared_method_definition()
  {}

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const
  {
    return std::make_shared<undeclared_method_definition<ThisType>>(*this);
  }

  virtual std::string str() const
  {
    std::string message = "method " + this->componentStr() + " is defined in "
                          + m_vtable + " but not declared in corresponding trait";

    return message;
  }

private:
  identifier<Implem> m_vtable;
};

template <typename ThisType>
class invalid_method_body : public Method<ThisType>::Error
{
public:
  invalid_method_body(Identifier name, Identifier vtable) noexcept :
   Method<ThisType>::Error::Error(name), m_vtable(vtable)
  {}

  invalid_method_body(invalid_method_body const& other) noexcept :
   invalid_method_body::invalid_method_body(other.componentStr(), other.m_vtable)
  {}

  virtual ~invalid_method_body()
  {}

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const
  {
    return std::make_shared<invalid_method_body<ThisType>>(*this);
  }

  virtual std::string str() const
  {
    std::string message = "method " + this->componentStr() + " is defined in "
                          + m_vtable + "but don't have a valid body";

    return message;
  }

private:
  identifier<Implem> m_vtable;
};

/* PROTOTYPE IMPLEM */

template <typename methodType>
Prototype<methodType>::Prototype(Component const& owner, Identifier const& name) :
 NamedComponentOf<Component, Prototype<methodType>>(name, owner),
 m_returnType(std::make_unique<Type>(*this, Type::VOID)),
 m_parameters()
{}

template <typename methodType>
Prototype<methodType>::Prototype(Prototype<methodType> const& origin) :
 NamedComponentOf<Component, Prototype<methodType>>(origin.name(),
                                                    origin.owner()),
 m_returnType(std::make_unique<Type>(*this, origin.returnType())),
 m_parameters()
{
  for (auto const& param : origin.m_parameters()) {
    m_parameters.push_back(std::make_unique<Type>(*this, *param));
  }
}

template <typename methodType>
Prototype<methodType>::~Prototype()
{}

template <typename methodType>
bool Prototype<methodType>::operator==(Prototype const& other) const
{

  if (this->name() != other.name()
      || m_parameters.size() != other.m_parameters.size()) {
    return false;
  }
  for (auto thisp(m_parameters.begin()), otherp(other.m_parameters.begin());
       thisp != m_parameters.end(); ++thisp, ++otherp) {
    if (*(*thisp) != *(*otherp)) {
      return false;
    }
  }
  return true;
}

template <typename methodType>
bool Prototype<methodType>::operator!=(Prototype const& other) const
{
  return !((*this) == other);
}

template <typename methodType>
void Prototype<methodType>::setReturnType(Type::Primary const& primary)
{
  m_returnType = std::make_unique<Type>(*this, primary);
}

template <typename methodType>
void Prototype<methodType>::setReturnType(AccessNameSet<Trait> const& traits)
{
  m_returnType = std::make_unique<Type>(*this, traits);
}

template <typename methodType>
void Prototype<methodType>::setReturnType(Type const& origin)
{
  m_returnType = std::make_unique<Type>(*this, origin);
}

template <typename methodType>
void Prototype<methodType>::addParameter(Type::Primary const& primary)
{
  m_parameters.push_back(std::make_unique<Type>(*this, primary));
}

template <typename methodType>
void Prototype<methodType>::addParameter(AccessNameSet<Trait> const& traits)
{
  m_parameters.push_back(std::make_unique<Type>(*this, traits));
}

template <typename methodType>
void Prototype<methodType>::addParameter(Type const& origin)
{
  m_parameters.push_back(std::make_unique<Type>(*this, origin));
}

template <typename methodType>
Prototype<methodType>& Prototype<methodType>::r(Type::Primary const& primary)
{
  setReturnType(primary);
  return *this;
}

template <typename methodType>
Prototype<methodType>& Prototype<methodType>::r(Type const& origin)
{
  setReturnType(origin);
  return *this;
}

template <typename methodType>
Prototype<methodType>&
Prototype<methodType>::r(std::unordered_set<std::string> const& namestrings)
{
  m_returnType = std::make_unique<Type>(*this, namestrings);
  return *this;
}

template <typename methodType>
Prototype<methodType>& Prototype<methodType>::p(Type::Primary const& primary)
{
  addParameter(primary);
  return *this;
}

template <typename methodType>
Prototype<methodType>& Prototype<methodType>::p(Type const& origin)
{
  addParameter(origin);
  return *this;
}

template <typename methodType>
Prototype<methodType>&
Prototype<methodType>::p(std::unordered_set<std::string> const& namestrings)
{
  m_parameters.push_back(std::make_unique<Type>(*this, namestrings));
  return *this;
}

template <typename methodType>
Type const& Prototype<methodType>::returnType() const
{
  return *m_returnType;
}

template <typename methodType>
std::vector<std::shared_ptr<Type>> const& Prototype<methodType>::parameters()
{
  return m_parameters;
}

template <typename methodType>
ErrorLog Prototype<methodType>::check() const
{

  ErrorLog errs;

  errs += m_returnType->check();
  for (auto const& param : m_parameters) {
    errs += param->check();
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

template <typename methodType>
std::string Prototype<methodType>::str() const
{

  std::string str;
  if constexpr (std::is_base_of<StaticMethod, methodType>::value) {
    str += "[S]";
  } else if constexpr (std::is_base_of<MemberMethod, methodType>::value) {
    str += "[M]";
  }

  //#FIXME we need to know the trait for good output ?

  str += this->name();
  str += "(";
  bool first = true;
  for (auto const& param : m_parameters) {
    if (first) {
      first = false;
    } else {
      str += ", ";
    }
    str += param->str();
  }
  str += ")->";
  str += m_returnType->str();
  return str;
}

/* ---------- */

template <typename ThisType>
Method<ThisType>::Method(Vtable const& vtable, Identifier const& name,
                         std::shared_ptr<Method<ThisType>::Body>&& body) :
 ComponentOf<Vtable, Method<ThisType>>::ComponentOf(vtable),
 m_prototype(*this, name),
 m_body(std::move(body))
{}

template <typename ThisType>
Method<ThisType>::~Method()
{}

template <typename ThisType>
Vtable const& Method<ThisType>::vtable()
{
  return this->owner();
}

template <typename ThisType>
ErrorLog Method<ThisType>::check() const
{
  ErrorLog errs;
  errs += m_prototype.check();
  if (m_body == nullptr) {
    errs += invalid_method_body<ThisType>(this->prototype().name(),
                                          this->owner().str());
  }
  return errs.wrapIfNotEmpty<Error>(this->str());
}

template <typename ThisType>
Prototype<Method<ThisType>> const& Method<ThisType>::prototype() const
{
  return m_prototype;
}

template <typename ThisType>
Prototype<Method<ThisType>>& Method<ThisType>::prototype()
{
  return m_prototype;
}

template <typename ThisType>
std::string Method<ThisType>::str() const
{
  std::string message = "(" + this->owner().implem().name() + ")";
  message += m_prototype.str();
  return message;
}

template <typename ThisType>
std::shared_ptr<typename Method<ThisType>::Body> const& Method<ThisType>::body() const
{
  return m_body;
}

} // namespace skuldenka::core::model

#endif