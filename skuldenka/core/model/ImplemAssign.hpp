/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_IMPLEMASSIGN_HPP
#define SKULDENKA_CORE_MODEL_IMPLEMASSIGN_HPP

#include "Implem.hpp"
#include "Method.hpp"
#include "typedefs.hpp"
#include <vector>

namespace skuldenka::core::model {

class ImplemSubAssign;
class ImplemSubAssignHasher;

class ImplemAssign : public ComponentOf<Component, ImplemAssign>
{
public:
  ImplemAssign(Component const& owner, Identifier const& name,
               std::list<Identifier> const& path = {}, bool isAbsolute = false);
  ImplemAssign(Component const& owner, ImplemAssign const& origin);

  virtual ~ImplemAssign();

  virtual ErrorLog check() const;
  virtual std::string str() const;

  ImplemSubAssign& addSub(Identifier const& request, Identifier const& name,
                          std::list<Identifier> const& path = {},
                          bool isAbsolute = false);

  std::vector<std::shared_ptr<ImplemSubAssign>> const& subs() const;

  AccessName<Implem> const& implem() const;
  std::shared_ptr<Implem> findImplem() const;

  typedef ComponentOf<Component, ImplemAssign>::Error Error;

private:
  AccessName<Implem> m_implem;
  std::vector<std::shared_ptr<ImplemSubAssign>> m_subs;
};

class ImplemSubAssign : public ImplemAssign
{
public:
  ImplemSubAssign(ImplemAssign const& owner, Identifier const& request,
                  Identifier const& name, std::list<Identifier> const& path = {},
                  bool isAbsolute = false);
  ImplemSubAssign(ImplemAssign const& owner, ImplemSubAssign const& origin);
  ImplemSubAssign(ImplemAssign const& owner, Identifier const& request,
                  ImplemAssign const& origin);

  virtual ~ImplemSubAssign();

  virtual std::string str() const;

  Identifier const& request() const;
  std::shared_ptr<ImplemRequest> findRequest()
      const; // FIXME could not return shared_ptr ! return optional<ref> instead !

  typedef ImplemAssign::Error Error;

  bool operator==(ImplemSubAssign const& other);

private:
  Identifier m_request;
};

class non_requested_assign : public ImplemAssign::Error
{
public:
  non_requested_assign(std::string const& assignStr) noexcept;
  non_requested_assign(non_requested_assign const& other) noexcept;

  virtual ~non_requested_assign();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;
};

class incomplete_assign : public ImplemAssign::Error
{
public:
  incomplete_assign(std::string const& requestStr) noexcept;
  incomplete_assign(incomplete_assign const& other) noexcept;

  virtual ~incomplete_assign();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;
};

class duplicate_implem_assign : public ImplemAssign::Error
{
public:
  duplicate_implem_assign(std::string const& requestStr) noexcept;
  duplicate_implem_assign(duplicate_implem_assign const& other) noexcept;

  virtual ~duplicate_implem_assign();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;
};

class assign_not_matching_request : public ImplemAssign::Error
{
public:
  assign_not_matching_request(std::string const& assignStr,
                              std::string const& requestStr,
                              std::vector<std::string> missingTraits) noexcept;
  assign_not_matching_request(assign_not_matching_request const& other) noexcept;

  virtual ~assign_not_matching_request();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string const& requestStr() const;

  std::vector<std::string> const& missingTraits();

private:
  std::string m_requestStr;
  std::vector<std::string> m_missingTraits;
};

} // namespace skuldenka::core::model

#endif // SKULDENKA_IMPLEMASSIGN_HPP