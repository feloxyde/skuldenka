/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "ImplemAssign.hpp"
#include "exception.hpp"
#include <algorithm>
#include <cassert>

namespace skuldenka::core::model {

ImplemAssign::ImplemAssign(Component const& owner, Identifier const& name,
                           std::list<Identifier> const& path, bool isAbsolute) :
 ComponentOf<Component, ImplemAssign>::ComponentOf(owner),
 m_implem(*this, name, path, isAbsolute),
 m_subs()
{}

//#FIXME is dynamic cast needed here ?
ImplemAssign::ImplemAssign(Component const& owner, ImplemAssign const& origin) :
 ComponentOf<Component, ImplemAssign>::ComponentOf(owner),
 m_implem(dynamic_cast<Component const&>(*this), origin.m_implem),
 m_subs()
{
  for (auto const& sub : origin.m_subs) {
    m_subs.push_back(std::make_shared<ImplemSubAssign>(*this, *sub));
  }
}

ImplemAssign::~ImplemAssign()
{}

ErrorLog ImplemAssign::check() const
{

  ErrorLog errs;

  // check assigned implem exists

  errs += m_implem.check();

  if (errs) {
    return errs.wrapIfNotEmpty<Error>(this->str());
  }

  auto implemptr = m_implem.find();
  assert(implemptr);

  // check all requests are fullfilled by subs for assign
  for (auto const& subrequest : implemptr->implemRequests()) {
    if (std::find_if(m_subs.begin(), m_subs.end(),
                     [&subrequest](std::shared_ptr<ImplemSubAssign> const& val) {
                       return subrequest->name() == val->request();
                     })
        == m_subs.end()) {
      errs << incomplete_assign(implemptr->name() + "::" + subrequest->name());
    }
  }

  for (auto subassignIt = m_subs.begin(); subassignIt != m_subs.end();) {
    auto& req = *subassignIt;
    if (std::find_if(++subassignIt, m_subs.end(),
                     [&req](std::shared_ptr<ImplemSubAssign> const& val) {
                       return req->request() == val->request();
                     })
        != m_subs.end()) {
      errs << duplicate_implem_assign(req->request());
    }
  }

  // check all subs are fullfilling a request
  for (auto const& subassign : m_subs) {
    if (std::find_if(implemptr->implemRequests().begin(),
                     implemptr->implemRequests().end(),
                     [&subassign](std::unique_ptr<ImplemRequest> const& val) {
                       return subassign->request() == val->name();
                     })
        == implemptr->implemRequests().end()) {
      errs << non_requested_assign(subassign->request());
    }
  }

  // check subs are matching requests trait requirements #FIXME this can be
  // factorized into Implem i think
  for (auto const& subassign : m_subs) {

    auto requestIt = std::find_if(
        implemptr->implemRequests().begin(), implemptr->implemRequests().end(),
        [&subassign](std::unique_ptr<ImplemRequest> const& val) {
          return subassign->request() == val->name();
        });

    if (requestIt != implemptr->implemRequests().end()) {

      auto requestTraits = (*requestIt)->unpack();
      auto subImplem = subassign->findImplem();
      if (subImplem != nullptr) {
        auto implemTraits = subImplem->unpack();

        // checking the requestTraits is a subset of implemTraits
        std::vector<std::string> missingTraits;
        for (auto const& trait : requestTraits) {
          if (implemTraits.find(trait) == implemTraits.end()) {
            missingTraits.push_back(trait->name());
          }
        }
        if (missingTraits.size() > 0) {
          errs << assign_not_matching_request(
              subassign->str(), (*requestIt)->str(), missingTraits);
        }
      }
    }
  }

  for (auto const& sub : m_subs) {
    errs += sub->check();
  }

  return errs.wrapIfNotEmpty<Error>(this->ImplemAssign::str());
}

std::string ImplemAssign::str() const
{
  std::string msg = "Implem assign : ";

  msg += m_implem.str();

  return msg;
}

std::vector<std::shared_ptr<ImplemSubAssign>> const& ImplemAssign::subs() const
{
  return m_subs;
}

AccessName<Implem> const& ImplemAssign::implem() const
{
  return m_implem;
}
std::shared_ptr<Implem> ImplemAssign::findImplem() const
{
  return m_implem.find();
}

/* ----- */

ImplemSubAssign::ImplemSubAssign(ImplemAssign const& owner,
                                 Identifier const& request,
                                 Identifier const& name,
                                 std::list<Identifier> const& path,
                                 bool isAbsolute) :
 ImplemAssign::ImplemAssign(owner, name, path, isAbsolute), m_request(request)
{}

ImplemSubAssign::ImplemSubAssign(ImplemAssign const& owner,
                                 ImplemSubAssign const& origin) :
 ImplemAssign::ImplemAssign(owner, *this), m_request(origin.m_request)
{}

ImplemSubAssign::ImplemSubAssign(ImplemAssign const& owner,
                                 Identifier const& request,
                                 ImplemAssign const& origin) :
 ImplemAssign::ImplemAssign(owner, *this), m_request(request)
{}

ImplemSubAssign::~ImplemSubAssign()
{}

std::string ImplemSubAssign::str() const
{
  std::string msg =
      "ImplemSubAssign : " + this->implem().str() + " -> "
      + dynamic_cast<const ImplemAssign&>(this->owner()).implem().str()
      + "::" + this->request();
  return msg;
}

Identifier const& ImplemSubAssign::request() const
{
  return m_request;
}

ImplemSubAssign& ImplemAssign::addSub(Identifier const& request,
                                      Identifier const& name,
                                      std::list<Identifier> const& path,
                                      bool isAbsolute)
{

  m_subs.push_back(std::make_shared<ImplemSubAssign>(*this, request, name, path,
                                                     isAbsolute));
  return *(m_subs.back());
}

/* ---- */

non_requested_assign::non_requested_assign(std::string const& assignStr) noexcept :
 ImplemSubAssign::Error::Error(assignStr)
{}

non_requested_assign::non_requested_assign(non_requested_assign const& other) noexcept :
 non_requested_assign::non_requested_assign(other.componentStr())
{}

non_requested_assign::~non_requested_assign()
{}

std::shared_ptr<skuldenka::core::model::Error> non_requested_assign::copy() const
{
  return std::make_shared<non_requested_assign>(*this);
}

std::string non_requested_assign::str() const
{
  std::string msg = "Implem assign " + this->componentStr()
                    + " does not refer to a valid implem link request";
  return msg;
}

/* ---- */

incomplete_assign::incomplete_assign(std::string const& requestStr) noexcept :
 ImplemAssign::Error::Error(requestStr)
{}

incomplete_assign::incomplete_assign(incomplete_assign const& other) noexcept :
 incomplete_assign::incomplete_assign(other.componentStr())
{}

incomplete_assign::~incomplete_assign()
{}

std::shared_ptr<skuldenka::core::model::Error> incomplete_assign::copy() const
{
  return std::make_shared<incomplete_assign>(*this);
}

std::string incomplete_assign::str() const
{
  std::string msg = "Implem link request " + this->componentStr()
                    + " is not associated to an assign";
  return msg;
}

/* ---- */

duplicate_implem_assign::duplicate_implem_assign(
    std::string const& requestStr) noexcept :
 ImplemAssign::Error::Error(requestStr)
{}

duplicate_implem_assign::duplicate_implem_assign(
    duplicate_implem_assign const& other) noexcept :
 duplicate_implem_assign::duplicate_implem_assign(other.componentStr())
{}

duplicate_implem_assign::~duplicate_implem_assign()
{}

std::shared_ptr<skuldenka::core::model::Error> duplicate_implem_assign::copy() const
{
  return std::make_shared<duplicate_implem_assign>(*this);
}

std::string duplicate_implem_assign::str() const
{
  std::string msg = "Implem link request " + this->componentStr()
                    + " is associated to more than one implem assign";
  return msg;
}

/* ---- */

assign_not_matching_request::assign_not_matching_request(
    std::string const& assignStr, std::string const& requestStr,
    std::vector<std::string> missingTraits) noexcept :
 ImplemSubAssign::Error::Error(assignStr),
 m_requestStr(requestStr),
 m_missingTraits(missingTraits)
{}

assign_not_matching_request::assign_not_matching_request(
    assign_not_matching_request const& other) noexcept :
 assign_not_matching_request::assign_not_matching_request(
     other.componentStr(), other.m_requestStr, other.m_missingTraits)
{}

assign_not_matching_request::~assign_not_matching_request()
{}

std::shared_ptr<skuldenka::core::model::Error> assign_not_matching_request::copy() const
{
  return std::make_shared<assign_not_matching_request>(*this);
}

std::string assign_not_matching_request::str() const
{
  std::string msg = "Implem assign " + this->componentStr()
                    + " does not fulfill link request " + m_requestStr
                    + " : missing traits {";
  if (m_missingTraits.size() > 0) {
    auto tname = m_missingTraits.begin();
    msg += *(tname++);
    while (tname != m_missingTraits.end()) {
      msg += ", " + *(tname++);
    }
  }
  msg += "}";
  return msg;
}

std::string const& assign_not_matching_request::requestStr() const
{
  return m_requestStr;
}

std::vector<std::string> const& assign_not_matching_request::missingTraits()
{
  return m_missingTraits;
}

} // namespace skuldenka::core::model