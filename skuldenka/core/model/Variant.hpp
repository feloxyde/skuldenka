/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

/* Variant idea highly inspired from Godot Engine's variant class, very useful to abstract types */
#ifndef SKULDENKA_CORE_MODEL_VARIANT_HPP
#define SKULDENKA_CORE_MODEL_VARIANT_HPP

#include <any>
#include <memory>
#include <skuldenka/core/model/Type.hpp>
#include <string>
#include <variant>
#include <vector>

//#FIXME variant manipulations shall be implemented later into runtime:: namespace as procedures.

namespace skuldenka::core::runtime {
class Instance;
class ImplemNode;
} // namespace skuldenka::core::runtime

namespace skuldenka::core::model {
class Trait;
class Implem;

class Variant final
{
public:
  Variant();
  Variant(Variant const& other);

  template <typename T>
  Variant(T v);

  template <typename T>
  T const& as() const;

  template <typename T>
  T& as();

  bool isVoid() const;

  template <typename T>
  bool is() const;

  template <typename T>
  Variant& operator=(
      T& other); //#FIXME NEEDS TO BE SPECIALIZED and maybe exported into runtime ?!

private:
  //#FIXME change this for inheritance from std::optional instead of composition ?
  std::variant<std::monostate, char, short, int, unsigned, long, unsigned long,
               float, double, std::string,
               std::shared_ptr<skuldenka::core::runtime::ImplemNode>,
               std::shared_ptr<skuldenka::core::runtime::Instance>,
               std::weak_ptr<skuldenka::core::runtime::ImplemNode>,
               std::weak_ptr<skuldenka::core::runtime::Instance>, AnyShared, AnyWeak>
      m_value;
};

/* TEMPLATE IMPLEM */

template <typename T>
Variant::Variant(T v) : m_value(v)
{}

template <typename T>
T const& Variant::as() const
{
  return std::get<T>(m_value);
}

template <typename T>
T& Variant::as()
{
  return std::get<T>(m_value);
}

template <typename T>
bool Variant::is() const
{
  if constexpr (std::is_same<T, void>::value) {
    return this->isVoid();
  } else {
    if (this->isVoid()) {
      return false;
    }
  }

  return std::holds_alternative<T>(m_value);
}

template <typename T>
Variant& Variant::operator=(T& other)
{
  m_value.emplace(other);
  return *this;
}

} // namespace skuldenka::core::model

#endif