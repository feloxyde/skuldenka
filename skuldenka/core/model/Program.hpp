/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_PROGRAM_HPP
#define SKULDENKA_CORE_MODEL_PROGRAM_HPP

#include "Component.hpp"
#include "Implem.hpp"
#include "ImplemAssign.hpp"
#include "Method.hpp"
#include "Vtable.hpp"
#include "typedefs.hpp"
#include <vector>

/* #FIXME will need to change the assign method in the future to allow adding Adapters */

namespace skuldenka::core::model {

/** A program has a "main" implem, and calls implem with several parameters
 *  for calling implem, the program has to call a static method of the implem implem
 *
 *  it also maps every ImplemLink for every Implem with an Implem ?;
 */
//#FIXME PROGRAM IS THE ONE CHECKING DEPENDENCIES ??? OR IMPLEM ASSIGN IS ?

class Program : public ModuleComponent<Program>
{
public:
  template <typename... ImplemAccessNameParams>
  Program(Module const& module, Identifier name,
          ImplemAccessNameParams... implemAccessNameArgs);

  virtual ~Program();

  ImplemAssign& implem();
  AccessName<Trait> const& trait() const;
  Identifier const& method() const;

  virtual ErrorLog check() const;
  virtual std::string str() const;

  typedef ModuleComponent<Program>::Error Error;

private:
  ImplemAssign m_implem; // not sure if only one is possible here ?
  AccessName<Trait> m_trait;
  Identifier m_method;
};

class main_method_trait_not_implemented : public Program::Error
{
public:
  main_method_trait_not_implemented(std::string const& programStr,
                                    std::string const& traitStr,
                                    std::string const& implemStr) noexcept;
  main_method_trait_not_implemented(
      main_method_trait_not_implemented const& other) noexcept;

  virtual ~main_method_trait_not_implemented();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

private:
  std::string m_traitStr;
  std::string m_implemStr;
};

/* TEMPLATE IMPLEMENTATION */

template <typename... ImplemAccessNameParams>
Program::Program(Module const& module, Identifier name,
                 ImplemAccessNameParams... implemAccessNameArgs) :
 ModuleComponent<Program>::ModuleComponent(name, module),
 m_implem(*this, implemAccessNameArgs...),
 m_trait(*this, "main", {}, true),
 m_method("main")
{}

} // namespace skuldenka::core::model

#endif