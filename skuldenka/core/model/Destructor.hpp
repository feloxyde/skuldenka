/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_DESCTRUCTOR_HPP
#define SKULDENKA_CORE_MODEL_DESCTRUCTOR_HPP

#include "Component.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <assert.h>
#include <memory>
#include <string>
#include <typeinfo>
#include <variant>
#include <vector>

//#FIXME add a destructor::body a bit like method bodies !

namespace skuldenka::core::runtime {
class Instance;
}

namespace skuldenka::core::model {
class Implem;

class Destructor : public ComponentOf<Implem, Destructor>
{
public:
  Destructor(Implem const& implem);
  virtual ~Destructor();

  Implem const& implem() const;

  virtual ErrorLog check() const;
  virtual std::string str() const;

  virtual void
  operator()(std::shared_ptr<skuldenka::core::runtime::Instance> instance) = 0;

  typedef ComponentOf<Implem, Destructor>::Error Error;

private:
  std::weak_ptr<Implem> m_implem;
};
} // namespace skuldenka::core::model

#endif
