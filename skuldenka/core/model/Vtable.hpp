/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_VTABLE_HPP
#define SKULDENKA_CORE_MODEL_VTABLE_HPP

#include "Component.hpp"
#include "Error.hpp"
#include "Method.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <algorithm>
#include <assert.h>
#include <memory>

namespace skuldenka::core::runtime {
class ImplemNode;
class Instance;
} // namespace skuldenka::core::runtime

namespace skuldenka::core::model {

class Implem;

class Trait;

class Vtable final : public ComponentOf<Implem, Vtable>
{
public:
  template <typename... TraitAccessNameParams>
  Vtable(Implem const& implem, TraitAccessNameParams... traitAccessNameArgs);
  ~Vtable();

  Prototype<MemberMethod>& defineMethod(Identifier const& name,
                                        std::shared_ptr<MemberMethod::Body> body);
  Prototype<StaticMethod>& defineMethod(Identifier const& name,
                                        std::shared_ptr<StaticMethod::Body> body);

  template <typename methodType = MemberMethod>
  std::vector<std::shared_ptr<methodType>> const& methods() const;

  virtual ErrorLog check() const;
  virtual std::string str() const;

  std::shared_ptr<Trait> findTrait() const;
  AccessName<Trait> const& trait() const;

  Implem const& implem() const;

  typedef ComponentOf<Implem, Vtable>::Error Error;

private:
  // these are defined in the .cpp file because used only there !
  template <typename methodType>
  ErrorLog checkDuplicates() const;
  template <typename methodType>
  ErrorLog checkNotDefined() const;
  template <typename methodType>
  ErrorLog checkNotDeclared() const;

private:
  AccessName<Trait> m_trait;
  std::vector<std::shared_ptr<MemberMethod>> m_methods;
  std::vector<std::shared_ptr<StaticMethod>> m_staticMethods;
};

class method_redefinition : public Vtable::Error
{
public:
  method_redefinition(std::string const& vtableStr,
                      std::string const& name) noexcept;
  method_redefinition(method_redefinition const& other) noexcept;

  virtual ~method_redefinition();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_name;
};

class method_not_declared : public Vtable::Error
{
public:
  method_not_declared(std::string const& vtableStr,
                      std::string const& name) noexcept;
  method_not_declared(method_not_declared const& other) noexcept;

  virtual ~method_not_declared();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_name;
};

class method_not_defined : public Vtable::Error
{
public:
  method_not_defined(std::string const& vtableStr,
                     std::string const& name) noexcept;
  method_not_defined(method_not_defined const& other) noexcept;

  virtual ~method_not_defined();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_name;
};

/* TEMPLATE IMPLEMENTATION */

template <typename... TraitAccessNameParams>
Vtable::Vtable(Implem const& implem,
               TraitAccessNameParams... traitAccessNameArgs) :
 ComponentOf<Implem, Vtable>::ComponentOf(implem),
 m_trait(*this, traitAccessNameArgs...),
 m_methods(),
 m_staticMethods()
{}

} // namespace skuldenka::core::model

#endif