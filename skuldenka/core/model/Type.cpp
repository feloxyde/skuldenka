/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Trait.hpp"
#include "Type.hpp"
#include <algorithm>
#include <assert.h>
#include <iostream>

namespace skuldenka::core::model {

Type::Type(Component const& owner, Type::Primary const& primary) :
 ComponentOf<Component, Type>::ComponentOf(owner), m_primary(primary), m_traits()
{}

Type::Type(Component const& owner, AccessNameSet<Trait> const& traitsNames) :
 ComponentOf<Component, Type>::ComponentOf(owner),
 m_primary(Type::Primary::TRAITS),
 m_traits()
{
  for (auto const& tn : traitsNames) {
    m_traits.emplace(*this, tn);
  }
}

Type::Type(Component const& owner,
           std::unordered_set<std::string> const& namestrings) :
 ComponentOf<Component, Type>::ComponentOf(owner),
 m_primary(Type::Primary::TRAITS),
 m_traits()
{
  for (auto const& str : namestrings) {
    m_traits.insert(AccessName<Trait>::fromString(*this, str));
  }
}

Type::Type(Type const& origin) :
 ComponentOf<Component, Type>::ComponentOf(origin.owner()),
 m_primary(origin.m_primary),
 m_traits(origin.m_traits)
{}

Type::Type(Component const& newOwner, Type const& origin) :
 ComponentOf<Component, Type>::ComponentOf(newOwner),
 m_primary(origin.m_primary),
 m_traits(origin.m_traits)
{}

Type::~Type()
{}

bool Type::isAbstract()
{
  return m_primary >= Type::TRAITS;
}

bool Type::operator==(Type const& other) const
{
  //#FIXME this is much much trickier to implement than before because type
  // has to take into consideration the fact that access name depends of the module
  if (m_primary != Type::Primary::TRAITS) {
    return m_primary == other.m_primary;
  }
  if (other.m_primary != Type::Primary::TRAITS) {
    return false;
  }

  return isSubsetOf(other.m_traits, m_traits)
         && isSubsetOf(m_traits, other.m_traits);
}

bool Type::operator!=(Type const& other) const
{
  return !((*this) == other);
}

Type::Primary Type::primary() const
{
  return m_primary;
}

std::unordered_set<std::shared_ptr<Trait>> const& Type::listTraits() const
{
  throw not_yet_implemented("Type::listTraits");
}

AccessNameSet<Trait> const& Type::traits() const
{
  return m_traits;
}

ErrorLog Type::check() const
{
  ErrorLog errs;
  for (auto const& traitname : m_traits) {
    errs += traitname.check();
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

Type& Type::operator=(Type const& origin)
{
  //#FIXME beware ! this does not change owner of the type !
  m_primary = origin.m_primary;
  m_traits = origin.m_traits;
  return *this;
}

std::string Type::str() const
{
  std::string str;
  switch (m_primary) {
  case Type::CHAR:
    str = "char";
    break;
  case Type::SHORT:
    str = "short";
    break;
  case Type::INT:
    str = "int";
    break;
  case Type::UNSIGNED:
    str = "unsigned";
    break;
  case Type::LONG:
    str = "long";
    break;
  case Type::UNSIGNED_LONG:
    str = "unsigned_long";
    break;
  case Type::FLOAT:
    str = "float";
    break;
  case Type::DOUBLE:
    str = "double";
    break;
  case Type::STRING:
    str = "string";
    break;
  case Type::IMPLEM:
    str = "implem";
    break;
  case Type::INSTANCE:
    str = "instance";
    break;
  case Type::IMPLEM_WEAK:
    str = "implemw";
    break;
  case Type::INSTANCE_WEAK:
    str = "instancew";
    break;
  case Type::TRAITS: {
    str = "{";
    bool first = true;
    for (auto traitname : m_traits) {
      if (first) {
        first = false;
      } else {
        str += ", ";
      }
      str += traitname.str();
    }
    str += "}";
  } break;
  default:
    str = "void";
    break;
  }
  return str;
}

} // namespace skuldenka::core::model