/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Error.hpp"
#include <cassert>

namespace skuldenka::core::model {

ErrorLog::ErrorLog() : std::list<std::shared_ptr<Error>>::list()
{}

ErrorLog::ErrorLog(ErrorLog const& other) : ErrorLog::ErrorLog()
{
  for (auto err : other) {
    *this << err;
  }
}

ErrorLog::ErrorLog(Error const& err) : ErrorLog::ErrorLog()
{
  *this << err;
}

ErrorLog::~ErrorLog()
{}

ErrorLog::operator bool() const
{
  return !(this->empty());
}

ErrorLog ErrorLog::operator+(ErrorLog const& other) const
{
  ErrorLog lel;
  lel += *this;
  lel += other;
  return lel;
}

ErrorLog& ErrorLog::operator+=(ErrorLog const& other)
{
  this->insert(this->end(), other.begin(), other.end());
  return *this;
}

ErrorLog& ErrorLog::operator=(ErrorLog const& other)
{
  this->clear();
  for (auto err : other) {
    *this << err;
  }
  return *this;
}

ErrorLog& ErrorLog::operator<<(std::shared_ptr<Error> const& error)
{
  this->push_back(error);
  return *this;
}

ErrorLog& ErrorLog::operator<<(Error const& error)
{
  *this << error.copy();
  return *this;
}

std::ostream& operator<<(std::ostream& os, ErrorLog const& log)
{
  for (auto const& err : log) {
    os << *err;
  }
  return os;
}

std::string Error::Indent::str()
{
  std::string istr;
  for (size_t i = 0; i < Error::Indent::count; ++i) {
    istr += Error::Indent::string;
  }
  return istr;
}

void Error::Indent::push()
{
  Error::Indent::count++;
}

void Error::Indent::pop()
{
  assert(Error::Indent::count > 0);
  Error::Indent::count--;
}

size_t Error::Indent::count = 0;

Error::Error() : m_subLog()
{}

Error::Error(ErrorLog const& subLog) : m_subLog(subLog)
{}

Error::~Error()
{}

ErrorLog const& Error::subLog() const
{
  return m_subLog;
}

Error& Error::operator<<(std::shared_ptr<Error> const& error)
{
  m_subLog << error;
  return *this;
}

Error& Error::operator<<(Error const& error)
{
  m_subLog << error;
  return *this;
}

Error& Error::operator<<(ErrorLog const& errorLog)
{
  m_subLog += errorLog;
  return *this;
}

std::ostream& operator<<(std::ostream& os, Error const& e)
{
  os << Error::Indent::str();
  os << e.str() << Error::Indent::ret;
  if (e.subLog().size() > 0) {
    Error::Indent::push();
    os << e.subLog();
    Error::Indent::pop();
  }
  return os;
}

} // namespace skuldenka::core::model