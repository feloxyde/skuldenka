/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_ERROR_HPP
#define SKULDENKA_CORE_MODEL_ERROR_HPP
#include "typedefs.hpp"

#include <iostream>
#include <list>
#include <memory>
#include <string>

//#FIXME maybe move log manipulation template like filter outside of log class and/or outside of the file  ?

namespace skuldenka::core::model {

class Error;

std::ostream& operator<<(std::ostream& os, Error const& e);

class ErrorLog final : public std::list<std::shared_ptr<Error>>
{
public:
  static char constexpr separator[] = "\n";

public:
  ErrorLog();
  ErrorLog(ErrorLog const& other);
  ErrorLog(Error const& err);
  virtual ~ErrorLog();

  operator bool() const;
  ErrorLog operator+(ErrorLog const& other) const;
  ErrorLog& operator+=(ErrorLog const& other);
  ErrorLog& operator=(ErrorLog const& other);

  ErrorLog& operator<<(std::shared_ptr<Error> const& error);
  ErrorLog& operator<<(Error const& error);

  template <typename ErrType, typename... argsT>
  ErrorLog wrap(argsT... args);

  template <typename ErrType, typename... argsT>
  ErrorLog wrapIfNotEmpty(argsT... args);

  /** returns a new error log containing only errors of ErrType from this*/
  template <typename ErrType>
  ErrorLog filter() const;

  /** returns true if there exists AT LEAST ONE structure matching the specified
    set of error types meaning that
    ErrorLog.containsStructure<Err1, Err2, Err3, Err4> will return true if ErrorLog contains
    at least one Err1 having folliing sub arch :
    Err1{Err2 { Err3{Err4}}}
  */
  template <typename first, typename... argsT>
  bool containsStructure() const;

  /** returns true if at least one error in log is of type errtype and has no
 sublog */
  // template <typename ErrType>
  // bool hasOrigin() const;

  /** returns true if at least one error in log is of type errtype and has no
  sublog */
  // template <typename Errtype>
  // bool countOrigins() const;
};

std::ostream& operator<<(std::ostream& os, ErrorLog const& log);

/* --- */

class Error
{
public:
  class Indent
  {
  public:
    static std::string str();

    static void push();
    static void pop();

    static char constexpr string[] = "\t";
    static char constexpr ret[] = "\n";

  private:
    static size_t count;
  };

public:
  Error();
  Error(ErrorLog const& subLog);
  virtual ~Error();

  virtual std::shared_ptr<Error> copy() const = 0;
  virtual std::string str() const = 0;

  ErrorLog const& subLog() const;

  Error& operator<<(std::shared_ptr<Error> const& error);
  Error& operator<<(Error const& error);
  Error& operator<<(ErrorLog const& errorLog);

private:
  ErrorLog m_subLog;
};

/* template implem */

template <typename ErrType, typename... argsT>
ErrorLog ErrorLog::wrap(argsT... args)
{
  ErrorLog errs;
  auto err = std::make_shared<ErrType>(args...);
  err->operator<<(*this);
  errs.operator<<(err);
  return errs;
}

template <typename ErrType, typename... argsT>
ErrorLog ErrorLog::wrapIfNotEmpty(argsT... args)
{
  if (this->size() > 0) {
    return this->wrap<ErrType>(args...);
  } else {
    return ErrorLog(*this);
  }
}

template <typename ErrType>
ErrorLog ErrorLog::filter() const
{

  static_assert(
      std::is_base_of<Error, ErrType>::value,
      "Filtering error log can only be done on Error deriveds classes");

  ErrorLog matches;
  for (auto err : *this) {
    if (auto ptr = std::dynamic_pointer_cast<ErrType>(err)) {
      matches.push_back(ptr);
    }
  }
  return matches;
}

template <typename first, typename... argsT>
bool ErrorLog::containsStructure() const
{
  for (auto const& e : *this) {

    auto eptr = std::dynamic_pointer_cast<first>(e);
    if constexpr (sizeof...(argsT) > 0) {
      if (eptr && eptr->subLog().template containsStructure<argsT...>()) {
        return true;
      }
    } else { // exit condition : only one type in param
      if (eptr) {
        return true;
      }
    }
  }
  return false;
}

} // namespace skuldenka::core::model

#endif