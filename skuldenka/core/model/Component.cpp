/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Component.hpp"
#include "Implem.hpp"
#include "Program.hpp"
#include "Trait.hpp"
#include "exception.hpp"

namespace skuldenka::core::model {

/* Component Implementation */

Component::Component()
{}

Component::~Component()
{}

/* Component Error implementation */

Component::Error::Error(std::string const& componentStr) :
 skuldenka::core::model::Error::Error(), m_componentStr(componentStr)
{}

Component::Error::~Error()
{}

std::string const& Component::Error::componentStr() const
{
  return m_componentStr;
}

std::shared_ptr<skuldenka::core::model::Error> Component::Error::copy() const
{
  return std::make_shared<Component::Error>(this->m_componentStr);
}

std::string Component::Error::str() const
{
  std::string msg = m_componentStr + " is invalid";
  return msg;
}

/* ---------- */

invalid_access_name_string::invalid_access_name_string(
    std::string const& string, std::string const& message) noexcept :
 m_string(string), m_message(message)
{}

invalid_access_name_string::invalid_access_name_string(
    invalid_access_name_string const& origin) noexcept :
 m_string(origin.m_string), m_message(origin.m_message)
{}

invalid_access_name_string& invalid_access_name_string::operator=(
    invalid_access_name_string const& other) noexcept
{
  m_string = other.m_string;
  m_message = other.m_message;
  return *this;
}

invalid_access_name_string::~invalid_access_name_string()
{}

std::string invalid_access_name_string::str() const noexcept
{
  std::string message = "skuldenka::invalid_access_name_string : AccessName "
                        "init attempt with invalid string "
                        + m_string + " : " + m_message;
  return m_message;
}

/* --------- */

Module::~Module()
{}

ErrorLog Module::declareSubModule(Identifier const& name)
{
  //#FIXME this code is redundant
  ErrorLog errs;
  errs += isNameFree(name);
  if (errs) {
    return errs;
  }
  m_components.emplace(name, std::make_shared<SubModule>(name, *this));

  return errs;
}

ErrorLog Module::declareTrait(Identifier const& name)
{
  //#FIXME this code is redundant
  ErrorLog errs;
  errs += isNameFree(name);
  if (errs) {
    return errs;
  }
  m_components.emplace(name, std::make_shared<Trait>(*this, name));

  return errs;
}
ErrorLog Module::declareImplem(Identifier const& name, SlotCount slots)
{
  //#FIXME this code is redundant
  ErrorLog errs;
  errs += isNameFree(name);
  if (errs) {
    return errs;
  }
  m_components.emplace(name, std::make_shared<Implem>(*this, name, slots));

  return errs;
}

ErrorLog Module::declareProgram(Identifier const& name, Identifier implem,
                                std::list<Identifier> const& implempath,
                                bool isAbsolute)
{
  //#FIXME this code is redundant
  ErrorLog errs;
  errs += isNameFree(name);
  if (errs) {
    return errs;
  }
  m_components.emplace(name, std::make_shared<Program>(*this, name, implem,
                                                       implempath, isAbsolute));
  return errs;
}

std::unordered_map<Identifier, std::shared_ptr<Component>> const&
Module::components()
{
  return m_components;
}

bool Module::isRoot() const
{
  return true;
}

Module const& Module::root() const
{
  return *this;
}

Module const& Module::parent() const
{
  return *this; // has no parent
}

Module const& Module::module() const
{
  return *this;
}

std::list<Identifier> Module::asPath() const
{
  return std::list<Identifier>();
}

ErrorLog Module::check() const
{
  ErrorLog errs;

  for (auto const& component : m_components) {
    errs += component.second->check();
  }

  return errs;
}

std::string Module::str() const
{
  return "Module";
}

Module::Module()
{}

ErrorLog Module::isNameFree(Identifier const& name)
{

  ErrorLog errs;
  auto comp = m_components.find(name);

  if (comp != m_components.end()) {
    errs << name_already_in_use(*this, name, *(comp->second));
    return errs.wrapIfNotEmpty<Error>(this->str());
  }
  return errs;
}

/* ----------- */
name_already_in_use::name_already_in_use(Module const& module, Identifier name,
                                         Component const& owner) :
 Module::Error::Error(module.str()), m_name(name), m_ownerString(owner.str())
{}
name_already_in_use::name_already_in_use(name_already_in_use const& origin) :
 Module::Error::Error(origin),
 m_name(origin.m_name),
 m_ownerString(origin.m_ownerString)
{}

name_already_in_use::~name_already_in_use()
{}

std::shared_ptr<skuldenka::core::model::Error> name_already_in_use::copy() const
{
  return std::make_shared<name_already_in_use>(*this);
}

std::string name_already_in_use::str() const
{
  std::string msg = this->componentStr() + " already holds a component with name "
                    + m_name + " : " + m_ownerString;
  return msg;
}

/*  ---------- */

SubModule::SubModule(Identifier name, Module const& parent) :
 Module::Module(), ModuleComponent<SubModule>::ModuleComponent(name, parent)
{}

SubModule::~SubModule()
{}

bool SubModule::isRoot() const
{
  return false;
}

Module const& SubModule::module() const
{
  return *this;
}

Module const& SubModule::root() const
{
  if (this->parent().isRoot()) {
    return this->parent();
  } else {
    return this->parent().root();
  };
}

Module const& SubModule::parent() const
{
  return this->owner();
}

std::string SubModule::str() const
{
  return this->absoluteName().str();
}

std::list<Identifier> SubModule::asPath() const
{
  auto prevPath = this->parent().asPath();
  prevPath.push_back(this->name());
  return prevPath;
}

/* ----------- */

Model::Model()
{}
Model::~Model()
{}

std::string Model::str() const
{
  return "Model";
}
} // namespace skuldenka::core::model