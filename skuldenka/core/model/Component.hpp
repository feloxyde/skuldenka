/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_COMPONENT_HPP
#define SKULDENKA_CORE_MODEL_COMPONENT_HPP

#include "Error.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>

namespace skuldenka::core::model {

class Model;
class Module;
class Component;

class Component
{
public:
  virtual ~Component();

  virtual ErrorLog check() const = 0;
  virtual std::string str() const = 0;
  virtual Module const& module() const = 0;

protected:
  Component();

public:
  class Error : public skuldenka::core::model::Error
  {
  public:
    Error(std::string const& componentStr);

    virtual ~Error();

    std::string const& componentStr() const;

    virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

    virtual std::string str() const;

  private:
    std::string m_componentStr;
  };
};

template <class Owner, class Type>
class ComponentOf : public virtual Component
{
public:
  ComponentOf(Owner const& owner);
  virtual ~ComponentOf();

  Owner const& owner() const;

  virtual Module const& module() const;

public:
  class Error : public Component::Error
  {
  public:
    Error(std::string const& componentStr);

    Error(Error const& other);

    virtual ~Error();

    virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;
  };

private:
  Owner const& m_owner;
};

template <typename ComponentT = Component>
class AccessName final : public ComponentOf<Component, AccessName<ComponentT>>
{
public:
  static bool areEquivalent(AccessName<ComponentT> const& n1,
                            AccessName<ComponentT> const& n2);

  static AccessName fromString(Component const& owner,
                               std::string const& nameAndPath);

public:
  AccessName(Component const& owner, Identifier name,
             std::list<Identifier> const& path = {}, bool isAbsolute = false);

  AccessName(AccessName<ComponentT> const& origin);
  AccessName(Component const& owner, AccessName<ComponentT> const& origin);

  ~AccessName();

  Identifier name() const;
  std::list<Identifier> const& path() const;
  std::list<Identifier> absolutePath() const;

  bool isAbsolute() const;

  virtual ErrorLog check() const;
  virtual std::string str() const;
  virtual std::string absoluteStr() const;

  bool operator==(AccessName<ComponentT> const& other) const;

  std::shared_ptr<ComponentT> find() const;

  typedef typename ComponentOf<Component, AccessName<ComponentT>>::Error Error;

private:
  Identifier m_name;
  std::list<Identifier> m_path;
  bool m_isAbsolute;
};

template <typename ComponentT>
class component_not_found : public AccessName<ComponentT>::Error
{
public:
  component_not_found(AccessName<ComponentT> const& access);
  component_not_found(component_not_found<ComponentT> const& origin);
  virtual ~component_not_found();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;
};

class invalid_access_name_string : public skuldenka::core::exception
{
public:
  invalid_access_name_string(std::string const& string,
                             std::string const& message) noexcept;

  invalid_access_name_string(invalid_access_name_string const&) noexcept;

  invalid_access_name_string& operator=(invalid_access_name_string const&) noexcept;

  virtual ~invalid_access_name_string();
  virtual std::string str() const noexcept;

private:
  std::string m_string;
  std::string m_message;
};

template <typename ComponentT = Component>

class AccessNameHasher
{
public:
  size_t operator()(AccessName<ComponentT> const& obj) const;
};

template <typename ComponentT = Component>
using AccessNameSet =
    std::unordered_set<AccessName<ComponentT>, AccessNameHasher<ComponentT>>;

template <typename ComponentT = Component>
bool isSubsetOf(AccessNameSet<ComponentT> const& subset,
                AccessNameSet<ComponentT> const& set);

template <class Owner, class Type>
class NamedComponentOf : public ComponentOf<Owner, Type>
{
public:
  NamedComponentOf(Identifier name, Owner const& owner);
  virtual ~NamedComponentOf();
  Identifier name() const;
  AccessName<Type> absoluteName() const;

private:
  Identifier m_name;
};

template <class Type>
class ModuleComponent : public NamedComponentOf<Module, Type>
{
public:
  ModuleComponent(Identifier name, Module const& module);
  virtual ~ModuleComponent();

  virtual Module const& module() const;
};

class SubModule;
class Implem;
class Program;
class Trait;

class Module : public virtual Component
{
public:
  virtual ~Module();

  ErrorLog declareSubModule(Identifier const& name);
  ErrorLog declareTrait(Identifier const& name);
  ErrorLog declareImplem(Identifier const& name, SlotCount slots);
  ErrorLog declareProgram(Identifier const& name, Identifier implem,
                          std::list<Identifier> const& implempath = {},
                          bool isAbsolute = false);

  template <typename ComponentT = Component>
  std::shared_ptr<ComponentT> find(Identifier const& name) const;

  std::unordered_map<Identifier, std::shared_ptr<Component>> const& components();

  virtual bool isRoot() const;
  virtual Module const& root() const;

  virtual Module const& parent() const;

  virtual Module const& module() const;

  virtual std::list<Identifier> asPath() const;

  virtual ErrorLog check() const;

  virtual std::string str() const;

  ErrorLog isNameFree(Identifier const& name);

  typedef Component::Error Error;

protected:
  Module();

private:
  std::unordered_map<Identifier, std::shared_ptr<Component>> m_components;
};

class name_already_in_use : public Module::Error
{
public:
  name_already_in_use(Module const& module, Identifier name,
                      Component const& owner);
  name_already_in_use(name_already_in_use const& origin);
  virtual ~name_already_in_use();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

private:
  std::string m_name;
  std::string m_ownerString;
};

class SubModule final : public Module, public ModuleComponent<SubModule>
{
public:
  SubModule(Identifier name, Module const& parent);
  virtual ~SubModule();

  virtual bool isRoot() const;

  virtual Module const& root() const;

  virtual Module const& parent() const;

  virtual Module const& module() const;

  virtual std::list<Identifier> asPath() const;

  virtual std::string str() const;
};

class Model final : public Module
{
public:
  Model();
  virtual ~Model();

  virtual std::string str() const;
};

/* template implems */

template <class Owner, class Type>
ComponentOf<Owner, Type>::ComponentOf(Owner const& owner) :
 Component::Component(), m_owner(owner)
{}

template <class Owner, class Type>
ComponentOf<Owner, Type>::~ComponentOf()
{}

template <class Owner, class Type>
Owner const& ComponentOf<Owner, Type>::owner() const
{
  return m_owner;
}

template <class Owner, class Type>
Module const& ComponentOf<Owner, Type>::module() const
{

  return this->owner().module();
}

/* ------ */
template <class Owner, class Type>
ComponentOf<Owner, Type>::Error::Error(std::string const& componentStr) :
 Component::Error::Error(componentStr)
{}

template <class Owner, class Type>
ComponentOf<Owner, Type>::Error::Error(Error const& other) :
 Error::Error(other.componentStr())
{}

template <class Owner, class Type>
ComponentOf<Owner, Type>::Error::~Error()
{}

template <class Owner, class Type>
std::shared_ptr<skuldenka::core::model::Error>
ComponentOf<Owner, Type>::Error::copy() const
{
  return std::make_shared<ComponentOf<Owner, Type>::Error>(this->componentStr());
}

/* ---------------- */

template <typename ComponentT>
bool AccessName<ComponentT>::areEquivalent(AccessName<ComponentT> const& n1,
                                           AccessName<ComponentT> const& n2)
{
  // two access names are equivalent if they point to the same thing.
  return n1.name() == n2.name() && n1.absolutePath() == n2.absolutePath();
}

template <typename ComponentT>
AccessName<ComponentT> AccessName<ComponentT>::fromString(
    Component const& owner, std::string const& nameAndPath)
{
  // Parse String
  if (nameAndPath.size() == 0) {
    throw invalid_access_name_string(nameAndPath,
                                     "cannot initialize from empty string");
  }
  if (nameAndPath.back() == ':') {
    throw invalid_access_name_string(nameAndPath,
                                     "string has to end with valid identifier");
  }

  size_t current = 0;
  bool absolute = false;
  if (nameAndPath[0] == ':') {
    if (nameAndPath.size() < 3 || nameAndPath[1] != ':') {
      throw invalid_access_name_string(
          nameAndPath, "delimiter between steps is ::, found :");
    } else {
      current = 2;
      absolute = true;
    }
  }
  std::string currentToken = "";
  std::list<Identifier> path;
  while (current < nameAndPath.size()) {
    if (nameAndPath[current] == ':') {
      ++current;
      if (nameAndPath[current++] != ':') {
        throw invalid_access_name_string(
            nameAndPath, "delimiter between steps is ::, found :");
      } else {
        path.push_back(
            Identifier(currentToken)); //#FIXMe maybe catch exception and
                                       // rethrow to match AccessName exception ?
        currentToken = "";
      }
    } else {
      currentToken += nameAndPath[current++];
    }
  }

  return AccessName<ComponentT>(owner, Identifier(currentToken), path, absolute);
}

template <typename ComponentT>
AccessName<ComponentT>::AccessName(Component const& owner, Identifier name,
                                   std::list<Identifier> const& path,
                                   bool isAbsolute) :
 ComponentOf<Component, AccessName<ComponentT>>::ComponentOf(owner),
 m_name(name),
 m_path(path),
 m_isAbsolute(isAbsolute)
{}

template <typename ComponentT>
AccessName<ComponentT>::AccessName(AccessName<ComponentT> const& origin) :
 ComponentOf<Component, AccessName<ComponentT>>::ComponentOf(origin.owner()),
 m_name(origin.m_name),
 m_path(origin.m_path),
 m_isAbsolute(origin.m_isAbsolute)
{}

template <typename ComponentT>
AccessName<ComponentT>::AccessName(Component const& owner,
                                   AccessName<ComponentT> const& origin) :
 ComponentOf<Component, AccessName<ComponentT>>::ComponentOf(owner),
 m_name(origin.m_name),
 m_path(origin.m_path),
 m_isAbsolute(origin.m_isAbsolute)
{}

template <typename ComponentT>
AccessName<ComponentT>::~AccessName()
{}

template <typename ComponentT>
Identifier AccessName<ComponentT>::name() const
{
  return m_name;
}

template <typename ComponentT>
std::list<Identifier> const& AccessName<ComponentT>::path() const
{
  return m_path;
}

template <typename ComponentT>
std::list<Identifier> AccessName<ComponentT>::absolutePath() const
{
  if (isAbsolute()) {
    return this->path();
  } else {
    auto aPath = this->module().asPath();
    auto rPath = this->path();
    aPath.splice(aPath.end(), rPath);
    return aPath;
  }
}

template <typename ComponentT>
bool AccessName<ComponentT>::isAbsolute() const
{
  return m_isAbsolute;
}

template <typename ComponentT>
ErrorLog AccessName<ComponentT>::check() const
{
  ErrorLog errs;
  if (!(this->find())) {
    errs << component_not_found<ComponentT>(*this);
  }
  return errs.wrapIfNotEmpty<Error>(this->str());
}

template <typename ComponentT>
std::string AccessName<ComponentT>::str() const
{
  std::string string = "";
  if (m_isAbsolute) {
    string += "::";
  }

  for (auto s : m_path) {
    string += s + "::";
  }

  string += m_name;

  return string;
} // namespace skuldenka

template <typename ComponentT>
std::string AccessName<ComponentT>::absoluteStr() const
{
  std::string string = "::";

  for (auto s : this->absolutePath()) {
    string += s + "::";
  }

  string += m_name;
  return string;
}

template <typename ComponentT>
bool AccessName<ComponentT>::operator==(AccessName<ComponentT> const& other) const
{
  return this->name() == other.name() && this->isAbsolute() == other.isAbsolute()
         && this->path() == other.path();
}

template <typename ComponentT>
std::shared_ptr<ComponentT> AccessName<ComponentT>::find() const
{
  // if no path :
  if (this->path().size() == 0) {

    return this->isAbsolute() ?
               this->module().root().template find<ComponentT>(this->name()) :
               this->module().template find<ComponentT>(this->name());
  }

  // if path, we navigate through loop
  // init loop
  auto subIt = this->path().begin();
  std::shared_ptr<Module> current =
      this->isAbsolute() ?
          this->module().root().template find<SubModule>(*subIt) :
          this->module().template find<SubModule>(*subIt);

  while (++subIt != this->path().end() && current) {
    current = current->find<SubModule>(*subIt);
  }
  if (!current) {
    return std::shared_ptr<ComponentT>();
  }
  assert(current);
  return current->find<ComponentT>(this->name());
}

/* ----------- */

template <typename ComponentT>
component_not_found<ComponentT>::component_not_found(
    AccessName<ComponentT> const& access) :
 AccessName<ComponentT>::Error::Error(access.str())
{}

template <typename ComponentT>
component_not_found<ComponentT>::component_not_found(
    component_not_found<ComponentT> const& origin) :
 AccessName<ComponentT>::Error::Error(origin)
{}

template <typename ComponentT>
component_not_found<ComponentT>::~component_not_found()
{}

template <typename ComponentT>
std::shared_ptr<skuldenka::core::model::Error>
component_not_found<ComponentT>::copy() const
{
  return std::make_shared<component_not_found<ComponentT>>(*this);
}

template <typename ComponentT>
std::string component_not_found<ComponentT>::str() const
{
  std::string msg = this->componentStr() + " does not refer to a component";
  return msg;
}

/* --------- */

template <typename ComponentT>
size_t AccessNameHasher<ComponentT>::operator()(AccessName<ComponentT> const& obj) const
{
  return std::hash<std::string>()(obj.str());
}

/* ------ */

template <typename ComponentT>
bool isSubsetOf(AccessNameSet<ComponentT> const& subset,
                AccessNameSet<ComponentT> const& set)
{
  // all of the subset shall have equivalents in the set
  for (auto const& traitname : subset) {
    if (std::find_if(set.begin(), set.end(),
                     [&traitname](AccessName<Trait> const& trait) {
                       return AccessName<Trait>::areEquivalent(traitname, trait);
                     })
        == set.end()) {
      return false;
    }
  }

  return true;
}

/* ----------- */

template <class Owner, class Type>
NamedComponentOf<Owner, Type>::NamedComponentOf(Identifier name,
                                                Owner const& owner) :
 ComponentOf<Owner, Type>::ComponentOf(owner), m_name(name)
{}

template <class Owner, class Type>
NamedComponentOf<Owner, Type>::~NamedComponentOf()
{}

template <class Owner, class Type>
Identifier NamedComponentOf<Owner, Type>::name() const
{
  return m_name;
}

template <class Owner, class Type>
AccessName<Type> NamedComponentOf<Owner, Type>::absoluteName() const
{
  return AccessName<Type>(*this, m_name, this->owner().module().asPath(), true);
}

/* ----------- */

template <class Type>
ModuleComponent<Type>::ModuleComponent(Identifier name, Module const& module) :
 NamedComponentOf<Module, Type>::NamedComponentOf(name, module)
{}

template <class Type>
ModuleComponent<Type>::~ModuleComponent()
{}

template <class Type>
Module const& ModuleComponent<Type>::module() const
{
  return this->owner();
}

/* ----------- */

template <typename ComponentT>
std::shared_ptr<ComponentT> Module::find(Identifier const& name) const
{
  auto it = m_components.find(name);
  if (it == m_components.end()) {
    return std::shared_ptr<ComponentT>();
  } else {
    return std::dynamic_pointer_cast<ComponentT>(it->second);
  }
}

} // namespace skuldenka::core::model

#endif