/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_MODEL_IMPLEM_HPP
#define SKULDENKA_CORE_MODEL_IMPLEM_HPP

#include "Destructor.hpp"
#include "Method.hpp"
#include "Type.hpp"
#include "typedefs.hpp"
#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_set>
#include <variant>
#include <vector>

namespace skuldenka::core::model {

class Vtable;
class Trait;

class Adapter;

class ImplemRequest final : public NamedComponentOf<Implem, ImplemRequest>
{
public:
  ImplemRequest(Implem const& implem, Identifier name);
  virtual ~ImplemRequest();

  virtual ErrorLog check() const;
  virtual std::string str() const;

  template <typename... AccessNameParams>
  void addTrait(AccessNameParams... accessNameArgs);

  template <typename... AccessNameParams>
  ImplemRequest& t(AccessNameParams... accessNameArgs);

  std::unordered_set<std::shared_ptr<Trait>> unpack() const;
  std::unordered_set<std::shared_ptr<Trait>> unpack(bool& incomplete) const;

  typedef ComponentOf<Implem, ImplemRequest>::Error Error;

private:
  Identifier m_name;
  AccessNameSet<Trait> m_traits;
};

class TraitRequest final : public ComponentOf<Implem, TraitRequest>
{
public:
  template <typename... TraitAccesNameParams>
  TraitRequest(Implem const& implem, TraitAccesNameParams... traitAccessNameArgs);
  virtual ~TraitRequest();

  virtual ErrorLog check() const;
  virtual std::string str() const;

  typedef ComponentOf<Implem, TraitRequest>::Error Error;

  AccessName<Trait> const& trait();

private:
  AccessName<Trait> m_trait;
};

class Implem final : public ModuleComponent<Implem>
{
public:
  Implem(Module const& module, Identifier name, SlotCount slotCount);
  virtual ~Implem();

  void defineDestructor(std::shared_ptr<Destructor> destructor);

  template <typename... AccessNameParams>
  void addTrait(AccessNameParams... accessNameArgs);

  template <typename... AccessNameParams>
  Implem& t(AccessNameParams... accessNameArgs);

  template <typename... TraitAccessNameParams>
  Vtable& defineVtable(
      TraitAccessNameParams... traitAccessNameArgs); //#FIXME maybe create auto
                                                     // vtable when trait is declared as requirement ?

  ImplemRequest& requestImplem(Identifier name);

  template <typename... TraitAccesNameParams>
  void requestTrait(TraitAccesNameParams... traitAccessNameArgs);

  virtual ErrorLog check() const;
  virtual std::string str() const;

  std::vector<std::unique_ptr<Vtable>> const& vtables() const;
  std::vector<std::unique_ptr<ImplemRequest>> const& implemRequests() const;
  std::vector<std::unique_ptr<TraitRequest>> const& traitRequests() const;

  typedef ModuleComponent<Implem>::Error Error;

  std::unordered_set<std::shared_ptr<Trait>> unpack() const;
  std::unordered_set<std::shared_ptr<Trait>> unpack(bool& incomplete) const;

private:
  ErrorLog checkLinkRequests() const;

private:
  SlotCount m_slotCount;
  AccessNameSet<Trait> m_traits;
  // FIXME use set instead of vector to prevent duplicates ? Or even unordered_map ?
  std::vector<std::unique_ptr<Vtable>> m_vtables;
  std::shared_ptr<Destructor> m_destructor;
  std::vector<std::unique_ptr<ImplemRequest>> m_implemRequests;
  std::vector<std::unique_ptr<TraitRequest>> m_traitRequests;
};

class duplicate_implem_request : public Implem::Error
{
public:
  duplicate_implem_request(std::string const& requestName,
                           std::string const& implemname) noexcept;
  duplicate_implem_request(duplicate_implem_request const& other) noexcept;

  virtual ~duplicate_implem_request();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_requestName;
};

class too_many_vtables : public Implem::Error
{
public:
  too_many_vtables(std::string const& traitname,
                   std::string const& implemname) noexcept;
  too_many_vtables(too_many_vtables const& other) noexcept;

  virtual ~too_many_vtables();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_traitname;
};

class trait_has_no_vtable : public Implem::Error
{
public:
  trait_has_no_vtable(std::string const& traitname,
                      std::string const& implemname) noexcept;
  trait_has_no_vtable(trait_has_no_vtable const& other) noexcept;

  virtual ~trait_has_no_vtable();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_traitname;
};

class vtable_has_no_trait : public Implem::Error
{
public:
  vtable_has_no_trait(std::string const& vtablename,
                      std::string const& implemname) noexcept;
  vtable_has_no_trait(vtable_has_no_trait const& other) noexcept;

  virtual ~vtable_has_no_trait();

  virtual std::shared_ptr<skuldenka::core::model::Error> copy() const;

  virtual std::string str() const;

  std::string name();

private:
  std::string m_vtablename;
};

/* TEMPLATE IMPLEMENTATION */

template <typename... AccessNameParams>
void ImplemRequest::addTrait(AccessNameParams... accessNameArgs)
{
  m_traits.emplace(*this, accessNameArgs...);
}

template <typename... AccessNameParams>
ImplemRequest& ImplemRequest::t(AccessNameParams... accessNameArgs)
{
  this->addTrait(accessNameArgs...);
  return *this;
}

/* -------- */

template <typename... TraitAccesNameParams>
TraitRequest::TraitRequest(Implem const& implem,
                           TraitAccesNameParams... traitAccessNameArgs) :
 ComponentOf<Implem, TraitRequest>::ComponentOf(implem),
 m_trait(*this, traitAccessNameArgs...)
{}

/* --------- */

template <typename... AccessNameParams>
void Implem::addTrait(AccessNameParams... accessNameArgs)
{
  m_traits.emplace(*this, accessNameArgs...);
}

template <typename... AccessNameParams>
Implem& Implem::t(AccessNameParams... accessNameArgs)
{
  this->addTrait(accessNameArgs...);
  return *this;
}

template <typename... TraitAccessNameParams>
Vtable& Implem::defineVtable(TraitAccessNameParams... traitAccessNameArgs)
{
  m_vtables.push_back(std::make_unique<Vtable>(*this, traitAccessNameArgs...));
  return *(m_vtables.back());
}

template <typename... TraitAccesNameParams>
void Implem::requestTrait(TraitAccesNameParams... traitAccessNameArgs)
{
  m_traitRequests.push_back(
      std::make_unique<TraitRequest>(*this, traitAccessNameArgs...));
}
} // namespace skuldenka::core::model

#endif