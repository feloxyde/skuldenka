/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Implem.hpp"
#include "Method.hpp"
#include "Trait.hpp"
#include "Vtable.hpp"
#include <algorithm>

namespace skuldenka::core::model {

Vtable::~Vtable()
{}

Prototype<MemberMethod>& Vtable::defineMethod(
    Identifier const& name, std::shared_ptr<MemberMethod::Body> body)
{
  m_methods.push_back(
      std::make_shared<MemberMethod>(*this, name, std::move(body)));
  return m_methods.back()->prototype();
}

Prototype<StaticMethod>& Vtable::defineMethod(
    Identifier const& name, std::shared_ptr<StaticMethod::Body> body)
{
  m_staticMethods.push_back(
      std::make_shared<StaticMethod>(*this, name, std::move(body)));
  return m_staticMethods.back()->prototype();
}

template <>
std::vector<std::shared_ptr<MemberMethod>> const& Vtable::methods<MemberMethod>() const
{
  return m_methods;
}

template <>
std::vector<std::shared_ptr<StaticMethod>> const& Vtable::methods<StaticMethod>() const
{
  return m_staticMethods;
}

template <typename methodType>
ErrorLog Vtable::checkDuplicates() const
{
  ErrorLog errs;
  // looking for duplicate
  for (auto it = methods<methodType>().begin();
       it != methods<methodType>().end();) {
    auto method = *it;
    if (std::find_if(++it, methods<methodType>().end(),
                     [&method](std::shared_ptr<methodType> const& val) {
                       return val->prototype() == method->prototype();
                     })
        != methods<methodType>().end()) {
      errs << method_redefinition(this->str(), method->str());
    }
  }
  return errs;
}

template <typename methodType>
ErrorLog Vtable::checkNotDefined() const
{
  ErrorLog errs;
  // checking that there is no declared but not implemented methods
  auto trait = m_trait.find();
  assert(trait);

  for (auto it = trait->template methods<methodType>().begin();
       it != trait->template methods<methodType>().end(); ++it) {
    auto proto = *it;
    if (std::find_if(methods<methodType>().begin(), methods<methodType>().end(),
                     [&proto](std::shared_ptr<methodType> val) {
                       return val->prototype() == *proto;
                     })
        == methods<methodType>().end()) {
      errs << method_not_defined(this->str(), (*it)->str());
    }
  }
  return errs;
}

template <typename methodType>
ErrorLog Vtable::checkNotDeclared() const
{
  ErrorLog errs;
  // checking that there is no implemented but not declared methods
  auto trait = m_trait.find();
  assert(trait);
  for (auto it = methods<methodType>().begin();
       it != methods<methodType>().end(); ++it) {
    auto method = *it;
    if (std::find_if(trait->template methods<methodType>().begin(),
                     trait->template methods<methodType>().end(),
                     [&method](std::shared_ptr<Prototype<methodType>> val) {
                       return (*val) == method->prototype();
                     })
        == trait->template methods<methodType>().end()) {
      errs << method_not_declared(this->str(), method->str());
    }
  }
  return errs;
}

ErrorLog Vtable::check() const
{
  ErrorLog errs;

  errs += m_trait.check();
  if (errs) {
    return errs.wrapIfNotEmpty<Error>(this->str());
  }

  auto trait = m_trait.find();
  assert(trait);

  errs += checkDuplicates<MemberMethod>();
  errs += checkDuplicates<StaticMethod>();

  errs += checkNotDeclared<MemberMethod>();
  errs += checkNotDeclared<StaticMethod>();

  errs += checkNotDefined<MemberMethod>();
  errs += checkNotDefined<StaticMethod>();

  for (auto const& method : m_methods) {
    errs += method->check();
  }

  for (auto const& method : m_staticMethods) {
    errs += method->check();
  }

  return errs.wrapIfNotEmpty<Error>(this->str());
}

std::string Vtable::str() const
{
  std::string str = "Vtable(I:" + owner().name() + ", T:";

  str += m_trait.str();

  str += ")";
  return str;
}

std::shared_ptr<Trait> Vtable::findTrait() const
{
  return this->m_trait.find();
}

AccessName<Trait> const& Vtable::trait() const
{
  return m_trait;
}

Implem const& Vtable::implem() const
{
  return this->owner();
}

/* ----- */

method_redefinition::method_redefinition(std::string const& vtableStr,
                                         std::string const& name) noexcept :
 Vtable::Error::Error(vtableStr), m_name(name)
{}

method_redefinition::method_redefinition(method_redefinition const& other) noexcept :
 method_redefinition::method_redefinition(other.componentStr(), other.m_name)
{}

method_redefinition::~method_redefinition()
{}

std::shared_ptr<skuldenka::core::model::Error> method_redefinition::copy() const
{
  return std::make_shared<method_redefinition>(this->componentStr(), m_name);
}

std::string method_redefinition::str() const
{
  std::string message = "Method " + m_name
                        + " defineMethodd more than once in Vtable "
                        + this->componentStr();
  return message;
}

std::string method_redefinition::name()
{
  return m_name;
}

/* ---- */

method_not_declared::method_not_declared(std::string const& vtableStr,
                                         std::string const& name) noexcept :
 Vtable::Error::Error(vtableStr), m_name(name)
{}

method_not_declared::method_not_declared(method_not_declared const& other) noexcept :
 method_not_declared::method_not_declared(other.componentStr(), other.m_name)
{}

method_not_declared::~method_not_declared()
{}

std::shared_ptr<skuldenka::core::model::Error> method_not_declared::copy() const
{
  return std::make_shared<method_not_declared>(this->componentStr(), m_name);
}

std::string method_not_declared::str() const
{
  std::string message = "Method " + m_name + " in Vtable " + this->componentStr()
                        + " does not match any declaration in Trait ";
  return message;
}

std::string method_not_declared::name()
{
  return m_name;
}

/* ------ */

method_not_defined::method_not_defined(std::string const& vtableStr,
                                       std::string const& name) noexcept :
 Vtable::Error::Error(vtableStr), m_name(name)
{}

method_not_defined::method_not_defined(method_not_defined const& other) noexcept :
 method_not_defined::method_not_defined(other.componentStr(), other.m_name)
{}

method_not_defined::~method_not_defined()
{}

std::shared_ptr<skuldenka::core::model::Error> method_not_defined::copy() const
{
  return std::make_shared<method_not_defined>(this->componentStr(), this->m_name);
}

std::string method_not_defined::str() const
{
  std::string message = "Method " + m_name + " is not defineMethodd in Vtable "
                        + this->componentStr() + " but declared in Trait";
  return message;
}

std::string method_not_defined::name()
{
  return m_name;
}
} // namespace skuldenka::core::model