/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <iostream>
#include <memory>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::runtime;

SUITE(Instance)
{
  class InstanceFixture
  {
  public:
    InstanceFixture() : vtables()
    {
      member0 = 0;
      member1 = 1;
      member2 = 2;
      member3 = 3;
      member4 = 4;
      static0 = 0;
      static1 = 1;
      static2 = 2;
      static3 = 3;
      trait0 = 0;
      trait1 = 1;
      trait2 = 2;
      trait3 = 3;

      vtables.push_back(generateVtable("trait0::"));
      vtables.push_back(generateVtable("trait1::"));
      vtables.push_back(generateVtable("trait2::"));
      vtables.push_back(generateVtable("trait3::"));

      std::vector<traitId_t> traitReqs;
      std::vector<std::shared_ptr<ImplemNode>> subs;

      node = ImplemNode::create(vtables, 0, subs, traitReqs);
    }

    ~InstanceFixture()
    {}

    Vtable generateVtable(std::string prefix)
    {
      std::vector<std::shared_ptr<MemberMethod::Body>> methods;
      std::vector<std::shared_ptr<StaticMethod::Body>> staticMethods;

      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member0", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member1", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member2", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member3", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member4", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static0", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static1", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static2", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static3", output));
      return Vtable(staticMethods, methods);
    }

  public:
    std::vector<std::string> output;
    std::vector<Vtable> vtables;
    std::shared_ptr<ImplemNode> node;
    methodId_t member0;
    methodId_t member1;
    methodId_t member2;
    methodId_t member3;
    methodId_t member4;
    methodId_t static0;
    methodId_t static1;
    methodId_t static2;
    methodId_t static3;
    traitId_t trait0;
    traitId_t trait1;
    traitId_t trait2;
    traitId_t trait3;
  };

  class InstanceSlotFixture
  {
  public:
    InstanceSlotFixture() : vtables()
    {
      member0 = 0;
      member1 = 1;
      member2 = 2;
      member3 = 3;
      member4 = 4;
      static0 = 0;
      static1 = 1;
      static2 = 2;
      static3 = 3;
      trait0 = 0;
      trait1 = 1;
      trait2 = 2;
      trait3 = 3;

      vtables.push_back(generateVtable("trait0::"));
      vtables.push_back(generateVtable("trait1::"));
      vtables.push_back(generateVtable("trait2::"));
      vtables.push_back(generateVtable("trait3::"));

      std::vector<traitId_t> traitReqs;
      std::vector<std::shared_ptr<ImplemNode>> subs;
      node = ImplemNode::create(vtables, 0, subs, traitReqs);
    }

    ~InstanceSlotFixture()
    {}

    Vtable generateVtable(std::string prefix)
    {
      std::vector<std::shared_ptr<MemberMethod::Body>> methods;
      std::vector<std::shared_ptr<StaticMethod::Body>> staticMethods;

      methods.push_back(
          std::make_shared<MockMemberMethodBodyWS>(prefix + "member0", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBodyWS>(prefix + "member1", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBodyWS>(prefix + "member2", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBodyWS>(prefix + "member3", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBodyWS>(prefix + "member4", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static0", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static1", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static2", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static3", output));
      return Vtable(staticMethods, methods);
    }

  public:
    std::vector<std::string> output;
    std::vector<Vtable> vtables;
    std::shared_ptr<ImplemNode> node;
    methodId_t member0;
    methodId_t member1;
    methodId_t member2;
    methodId_t member3;
    methodId_t member4;
    methodId_t static0;
    methodId_t static1;
    methodId_t static2;
    methodId_t static3;
    traitId_t trait0;
    traitId_t trait1;
    traitId_t trait2;
    traitId_t trait3;
  };

  TEST_FIXTURE(InstanceFixture, construction)
  {
    auto instance = Instance::create(node, 0);
    CHECK(instance != nullptr);

    auto instance3s = Instance::create(node, 3);
    CHECK(instance3s != nullptr);
    CHECK((*instance3s)[0].isVoid());
    CHECK((*instance3s)[1].isVoid());
    CHECK((*instance3s)[2].isVoid());
  }

  TEST_FIXTURE(InstanceFixture, callOperator0A)
  {
    auto instance = Instance::create(node, 0);

    Variant res1 = (*instance)(trait0, member3);
    Variant res2 = (*instance)(trait2, member0);
    Variant res3 = (*instance)(trait1, member2);
    Variant res4 = (*instance)(trait3, member1);

    CHECK_EQUAL(4, output.size());
    CHECK_EQUAL("trait0::member3(0)", output[0]);
    CHECK_EQUAL("trait2::member0(0)", output[1]);
    CHECK_EQUAL("trait1::member2(0)", output[2]);
    CHECK_EQUAL("trait3::member1(0)", output[3]);
  }

  TEST_FIXTURE(InstanceFixture, callOperatorNA)
  {
    auto instance = Instance::create(node, 0);

    Variant res1 = (*instance)(trait0, member3, std::array<Variant, 4>());
    Variant res2 = (*instance)(trait2, member0, std::array<Variant, 6>());
    Variant res3 = (*instance)(trait1, member2, std::array<Variant, 3>());
    Variant res4 = (*instance)(trait3, member1, std::array<Variant, 7>());

    CHECK_EQUAL(4, output.size());
    CHECK_EQUAL("trait0::member3(4)", output[0]);
    CHECK_EQUAL("trait2::member0(6)", output[1]);
    CHECK_EQUAL("trait1::member2(3)", output[2]);
    CHECK_EQUAL("trait3::member1(7)", output[3]);
  }

  TEST_FIXTURE(InstanceFixture, slotandOpSubset)
  {
    auto instance3s = Instance::create(node, 3);
    CHECK(instance3s != nullptr);
    CHECK((*instance3s)[0].isVoid());
    CHECK((*instance3s)[1].isVoid());
    CHECK((*instance3s)[2].isVoid());
    (*instance3s)[0] = (int)189;
    (*instance3s)[1] = std::string("someString");
    (*instance3s)[2] = (float)1.4f;

    CHECK((*instance3s)[0].is<int>());
    CHECK(!(*instance3s)[0].is<std::string>());
    CHECK(!(*instance3s)[0].is<float>());

    CHECK(!(*instance3s)[1].is<int>());
    CHECK((*instance3s)[1].is<std::string>());
    CHECK(!(*instance3s)[1].is<float>());

    CHECK(!(*instance3s)[2].is<int>());
    CHECK(!(*instance3s)[2].is<std::string>());
    CHECK((*instance3s)[2].is<float>());

    CHECK_EQUAL(189, (*instance3s)[0].as<int>());
    CHECK_EQUAL("someString", (*instance3s)[1].as<std::string>());
    CHECK_EQUAL(1.4f, (*instance3s)[2].as<float>());

    CHECK_EQUAL(189, instance3s->slot<int>(0));
    CHECK_EQUAL("someString", instance3s->slot<std::string>(1));
    CHECK_EQUAL(1.4f, instance3s->slot<float>(2));
  }

  TEST_FIXTURE(InstanceSlotFixture, callingMethodsWhichUseSlot)
  {
    auto instance1 = Instance::create(node, 1);
    (*instance1)[0] = std::string("instance1");
    auto instance2 = Instance::create(node, 1);
    (*instance2)[0] = std::string("instance2");

    Variant res1 = (*instance1)(trait0, member3);
    Variant res2 = (*instance1)(trait2, member0, std::array<Variant, 6>());
    Variant res3 = (*instance2)(trait1, member2, std::array<Variant, 3>());
    Variant res4 = (*instance1)(trait3, member1, std::array<Variant, 7>());
    Variant res5 = (*instance2)(trait3, member1, std::array<Variant, 7>());

    CHECK_EQUAL(5, output.size());
    CHECK_EQUAL("instance1->trait0::member3(0)", output[0]);
    CHECK_EQUAL("instance1->trait2::member0(6)", output[1]);
    CHECK_EQUAL("instance2->trait1::member2(3)", output[2]);
    CHECK_EQUAL("instance1->trait3::member1(7)", output[3]);
    CHECK_EQUAL("instance2->trait3::member1(7)", output[4]);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}