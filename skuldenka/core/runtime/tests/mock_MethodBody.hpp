/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_RUNTIME_TEST_MOCK_METHODBODY_HPP
#define SKULDENKA_CORE_RUNTIME_TEST_MOCK_METHODBODY_HPP

#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <skuldenka/core/runtime/Instance.hpp>
#include <string>
#include <vector>

using skuldenka::core::model::MemberMethod;
using skuldenka::core::model::StaticMethod;
using skuldenka::core::model::Variant;

using skuldenka::core::runtime::ImplemNode;
using skuldenka::core::runtime::Instance;

class MockMemberMethodBody : public MemberMethod::Body
{
public:
  MockMemberMethodBody(std::string name, std::vector<std::string>& output) :
   m_output(output), m_name(name)
  {}
  virtual ~MockMemberMethodBody()
  {}

  virtual std::shared_ptr<MemberMethod::Body> copy()
  {
    return std::make_shared<MockMemberMethodBody>(m_name, m_output);
  };

  virtual Variant operator()(std::shared_ptr<Instance> thisptr)
  {
    m_output.push_back(m_name + "(0)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 1> const& args)
  {
    m_output.push_back(m_name + "(1)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 2> const& args)
  {
    m_output.push_back(m_name + "(2)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 3> const& args)
  {
    m_output.push_back(m_name + "(3)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 4> const& args)
  {
    m_output.push_back(m_name + "(4)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 5> const& args)
  {
    m_output.push_back(m_name + "(5)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 6> const& args)
  {
    m_output.push_back(m_name + "(6)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 7> const& args)
  {
    m_output.push_back(m_name + "(7)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 8> const& args)
  {
    m_output.push_back(m_name + "(8)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 9> const& args)
  {
    m_output.push_back(m_name + "(9)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 10> const& args)
  {
    m_output.push_back(m_name + "(10)");
    return Variant();
  }

public:
  std::vector<std::string>& m_output;
  std::string m_name;
};

class MockStaticMethodBody : public StaticMethod::Body
{
public:
  MockStaticMethodBody(std::string name, std::vector<std::string>& output) :
   m_output(output), m_name(name)
  {}
  virtual ~MockStaticMethodBody()
  {}

  virtual std::shared_ptr<StaticMethod::Body> copy()
  {
    return std::make_shared<MockStaticMethodBody>(m_name, m_output);
  };

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr)
  {
    m_output.push_back(m_name + "(0)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 1> const& args)
  {
    m_output.push_back(m_name + "(1)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 2> const& args)
  {
    m_output.push_back(m_name + "(2)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 3> const& args)
  {
    m_output.push_back(m_name + "(3)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 4> const& args)
  {
    m_output.push_back(m_name + "(4)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 5> const& args)
  {
    m_output.push_back(m_name + "(5)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 6> const& args)
  {
    m_output.push_back(m_name + "(6)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 7> const& args)
  {
    m_output.push_back(m_name + "(7)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 8> const& args)
  {
    m_output.push_back(m_name + "(8)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 9> const& args)
  {
    m_output.push_back(m_name + "(9)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<ImplemNode> thisptr,
                             std::array<Variant, 10> const& args)
  {
    m_output.push_back(m_name + "(10)");
    return Variant();
  }

public:
  std::vector<std::string>& m_output;
  std::string m_name;
};

class MockMemberMethodBodyWS : public MemberMethod::Body
{
public:
  MockMemberMethodBodyWS(std::string name, std::vector<std::string>& output) :
   m_output(output), m_name(name)
  {}
  virtual ~MockMemberMethodBodyWS()
  {}

  virtual std::shared_ptr<MemberMethod::Body> copy()
  {
    return std::make_shared<MockMemberMethodBodyWS>(m_name, m_output);
  };

  virtual Variant operator()(std::shared_ptr<Instance> thisptr)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(0)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 1> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(1)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 2> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(2)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 3> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(3)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 4> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(4)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 5> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(5)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 6> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(6)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 7> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(7)");
    return Variant();
  }

  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 8> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(8)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 9> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(9)");
    return Variant();
  }
  virtual Variant operator()(std::shared_ptr<Instance> thisptr,
                             std::array<Variant, 10> const& args)
  {
    m_output.push_back(thisptr->slot<std::string>(0) + "->" + m_name + "(10)");
    return Variant();
  }

public:
  std::vector<std::string>& m_output;
  std::string m_name;
};

#endif