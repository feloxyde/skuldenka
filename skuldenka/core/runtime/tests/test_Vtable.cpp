/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <array>
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <skuldenka/core/runtime/Instance.hpp>
#include <skuldenka/core/runtime/Vtable.hpp>
#include <sstream>
#include <string>

using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::runtime;
using skuldenka::core::model::Variant;

SUITE(Vtable)
{

  class VtableFixture
  {
  public:
    VtableFixture() :
     output(), implemNode(), instance(), methods(), staticMethods()
    {
      // implemnode and instance ptr are null. SHOULD NEVER BE in usual use case !
      methods.push_back(std::make_shared<MockMemberMethodBody>("member0", output));
      methods.push_back(std::make_shared<MockMemberMethodBody>("member1", output));
      methods.push_back(std::make_shared<MockMemberMethodBody>("member2", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>("static0", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>("static1", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>("static2", output));
      member0 = 0;
      member1 = 1;
      member2 = 2;
      static0 = 0;
      static1 = 1;
      static2 = 2;
    }
    ~VtableFixture()
    {}

  public:
    std::vector<std::string> output;
    std::shared_ptr<Instance> instance;
    std::shared_ptr<ImplemNode> implemNode;
    std::vector<std::shared_ptr<MemberMethod::Body>> methods;
    std::vector<std::shared_ptr<StaticMethod::Body>> staticMethods;
    methodId_t member0;
    methodId_t member1;
    methodId_t member2;
    methodId_t static0;
    methodId_t static1;
    methodId_t static2;
  };
  TEST_FIXTURE(VtableFixture, construction)
  {
    Vtable vtable(staticMethods, methods);
    //#FIXME check that methods have been copied ?
  }

  TEST_FIXTURE(VtableFixture, callOperator_staticMethod0A)
  {
    Vtable vtable(staticMethods, methods);
    Variant res1 = vtable(static1, implemNode);
    Variant res0 = vtable(static0, implemNode);
    Variant res2 = vtable(static2, implemNode);

    CHECK_EQUAL(3, output.size());
    CHECK_EQUAL("static1(0)", output[0]);
    CHECK_EQUAL("static0(0)", output[1]);
    CHECK_EQUAL("static2(0)", output[2]);
  }

  TEST_FIXTURE(VtableFixture, callOperator_staticMethodNA)
  {
    Vtable vtable(staticMethods, methods);
    Variant res1 = vtable(static1, implemNode, std::array<Variant, 3>());
    Variant res0 = vtable(static0, implemNode, std::array<Variant, 6>());
    Variant res2 = vtable(static2, implemNode, std::array<Variant, 7>());

    CHECK_EQUAL(3, output.size());
    CHECK_EQUAL("static1(3)", output[0]);
    CHECK_EQUAL("static0(6)", output[1]);
    CHECK_EQUAL("static2(7)", output[2]);
  }

  TEST_FIXTURE(VtableFixture, callOperator_memberMethod0A)
  {
    Vtable vtable(staticMethods, methods);
    Variant res2 = vtable(member2, instance);
    Variant res0 = vtable(member0, instance);
    Variant res1 = vtable(member1, instance);

    CHECK_EQUAL(3, output.size());
    CHECK_EQUAL("member2(0)", output[0]);
    CHECK_EQUAL("member0(0)", output[1]);
    CHECK_EQUAL("member1(0)", output[2]);
  }

  TEST_FIXTURE(VtableFixture, callOperator_memberMethodNA)
  {
    Vtable vtable(staticMethods, methods);
    Variant res2 = vtable(member2, instance, std::array<Variant, 3>());
    Variant res0 = vtable(member0, instance, std::array<Variant, 5>());
    Variant res1 = vtable(member1, instance, std::array<Variant, 8>());

    CHECK_EQUAL(3, output.size());
    CHECK_EQUAL("member2(3)", output[0]);
    CHECK_EQUAL("member0(5)", output[1]);
    CHECK_EQUAL("member1(8)", output[2]);
  }

  TEST(operatorAssign)
  {
    CHECK(false);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}