/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "mock_MethodBody.hpp"
#include <array>
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <skuldenka/core/runtime/Instance.hpp>
#include <skuldenka/core/runtime/Vtable.hpp>
#include <sstream>
#include <string>

using skuldenka::core::model::Variant;
using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::runtime;

SUITE(ImplemNode)
{
  class ImplemNodeFixture
  {
  public:
    ImplemNodeFixture() : vtables()
    {
      member0 = 0;
      member1 = 1;
      member2 = 2;
      member3 = 3;
      member4 = 4;
      static0 = 0;
      static1 = 1;
      static2 = 2;
      static3 = 3;
      trait0 = 0;
      trait1 = 1;
      trait2 = 2;
      trait3 = 3;

      vtables.push_back(generateVtable("trait0::"));
      vtables.push_back(generateVtable("trait1::"));
      vtables.push_back(generateVtable("trait2::"));
      vtables.push_back(generateVtable("trait3::"));
    }

    ~ImplemNodeFixture()
    {}

    Vtable generateVtable(std::string prefix)
    {
      std::vector<std::shared_ptr<MemberMethod::Body>> methods;
      std::vector<std::shared_ptr<StaticMethod::Body>> staticMethods;

      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member0", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member1", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member2", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member3", output));
      methods.push_back(
          std::make_shared<MockMemberMethodBody>(prefix + "member4", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static0", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static1", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static2", output));
      staticMethods.push_back(
          std::make_shared<MockStaticMethodBody>(prefix + "static3", output));
      return Vtable(staticMethods, methods);
    }

  public:
    std::vector<std::string> output;
    std::vector<Vtable> vtables;
    methodId_t member0;
    methodId_t member1;
    methodId_t member2;
    methodId_t member3;
    methodId_t member4;
    methodId_t static0;
    methodId_t static1;
    methodId_t static2;
    methodId_t static3;
    traitId_t trait0;
    traitId_t trait1;
    traitId_t trait2;
    traitId_t trait3;
  };

  TEST_FIXTURE(ImplemNodeFixture, construction)
  {

    std::vector<traitId_t> traitReqs;
    std::vector<std::shared_ptr<ImplemNode>> subs;

    auto node = ImplemNode::create(vtables, 0, subs, traitReqs);
    //#FIXME check for more ??? Like traitId reduction is right ?
  }

  TEST_FIXTURE(ImplemNodeFixture, callOperator0A)
  {
    std::vector<traitId_t> traitReqs;
    std::vector<std::shared_ptr<ImplemNode>> subs;

    auto node = ImplemNode::create(vtables, 0, subs, traitReqs);
    Variant res0 = (*node)(trait0, static3);
    Variant res1 = (*node)(trait1, static2);
    Variant res2 = (*node)(trait3, static0);
    Variant res3 = (*node)(trait0, static1);

    CHECK_EQUAL(4, output.size());
    CHECK_EQUAL("trait0::static3(0)", output[0]);
    CHECK_EQUAL("trait1::static2(0)", output[1]);
    CHECK_EQUAL("trait3::static0(0)", output[2]);
    CHECK_EQUAL("trait0::static1(0)", output[3]);
  }

  TEST_FIXTURE(ImplemNodeFixture, callOperatorNA)
  {
    std::vector<traitId_t> traitReqs;
    std::vector<std::shared_ptr<ImplemNode>> subs;

    auto node = ImplemNode::create(vtables, 0, subs, traitReqs);
    Variant res1 = (*node)(trait0, static3, std::array<Variant, 2>());
    Variant res2 = (*node)(trait1, static2, std::array<Variant, 4>());
    Variant res3 = (*node)(trait3, static0, std::array<Variant, 1>());
    Variant res4 = (*node)(trait0, static1, std::array<Variant, 7>());

    CHECK_EQUAL(4, output.size());
    CHECK_EQUAL("trait0::static3(2)", output[0]);
    CHECK_EQUAL("trait1::static2(4)", output[1]);
    CHECK_EQUAL("trait3::static0(1)", output[2]);
    CHECK_EQUAL("trait0::static1(7)", output[3]);
  }

  TEST(subsetOperator)
  {
    CHECK(false);
  }

  TEST(sub)
  {
    CHECK(false);
  }

  TEST(traitReqs)
  {
    CHECK(false);
  }

  //#FIXME need to make some verification about implemnode passing itself as
  // call argument to vtable ?
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}