/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_RUNTIME_IMPLEMNODE_HPP
#define SKULDENKA_CORE_RUNTIME_IMPLEMNODE_HPP

// this class is the holder of the implem assign structure to link to Instance

#include "ImplemNode.hpp"
#include "Vtable.hpp"
#include "typedefs.hpp"
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <vector>

namespace skuldenka::core::runtime {
class Instance;
class Vtable;
class Implem;
//#FIXME add an "initializer" method for model::Implem that could later be used
// for init runtime::Implem or implemNode ?
//#FIXME add SlotCount as params ? YES probably !
//#FIXME add methods to alloc instances like "std::shared_ptr<Instance> newInstance()"

//#FIXME missing slot count !
class ImplemNode final : public std::enable_shared_from_this<ImplemNode>
{

public:
  static std::shared_ptr<ImplemNode>
  create(std::vector<Vtable> const& vtables, traitId_t traitIdReduction,
         std::vector<std::shared_ptr<ImplemNode>> const& subImplems,
         std::vector<traitId_t> treqs);

public:
  ~ImplemNode();

  Variant operator()(traitId_t traitId, methodId_t methodId);
  template <std::size_t argC>
  Variant operator()(traitId_t traitId, methodId_t methodId,
                     std::array<Variant, argC> const& args);

  Vtable const& operator[](traitId_t traitId) const;

  std::shared_ptr<ImplemNode> sub(implemRequestId_t implemId);

  std::shared_ptr<Instance> instance();

  traitId_t traitId(size_t requestId);

private:
  ImplemNode(std::vector<Vtable> const& vtables, traitId_t traitIdReduction,
             std::vector<std::shared_ptr<ImplemNode>> const& subImplems,
             std::vector<traitId_t> treqs);

private:
  traitId_t m_traitIdReduction;
  std::vector<Vtable> m_vtables;
  std::vector<std::shared_ptr<ImplemNode>> m_subImplems;
  std::vector<traitId_t> m_traitReqsId;
};

/* TEMPLATE IMPLEMENTATION */
template <std::size_t argC>
Variant ImplemNode::operator()(traitId_t traitId, methodId_t methodId,
                               std::array<Variant, argC> const& args)
{
  return (*this)[traitId](methodId, this->shared_from_this(), args);
}

} // namespace skuldenka::core::runtime

#endif
