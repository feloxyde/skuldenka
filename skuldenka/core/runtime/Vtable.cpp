/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "Vtable.hpp"
#include <iostream>

namespace skuldenka::core::runtime {

Vtable::Vtable() : m_staticMethods(), m_methods()
{}

Vtable::Vtable(std::vector<std::shared_ptr<StaticMethod::Body>> staticMethods,
               std::vector<std::shared_ptr<MemberMethod::Body>> methods) :
 m_staticMethods(staticMethods), m_methods(methods)
{}

Vtable::Vtable(Vtable const& origin) :
 m_staticMethods(origin.m_staticMethods), m_methods(origin.m_methods)
{}

Vtable::~Vtable()
{}

Variant Vtable::operator()(methodId_t methodId,
                           std::shared_ptr<Instance> instance) const
{
  return m_methods[methodId]->operator()(instance);
}

Variant Vtable::operator()(methodId_t methodId,
                           std::shared_ptr<ImplemNode> implem) const
{
  return m_staticMethods[methodId]->operator()(implem);
}

Vtable& Vtable::operator=(Vtable const& other)
{
  this->m_methods = other.m_methods;
  this->m_staticMethods = other.m_staticMethods;
  return *this;
}

} // namespace skuldenka::core::runtime