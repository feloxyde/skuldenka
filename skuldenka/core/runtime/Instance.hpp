/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_RUNTIME_INSTANCE_HPP
#define SKULDENKA_CORE_RUNTIME_INSTANCE_HPP

#include "ImplemNode.hpp"
#include "typedefs.hpp"
#include <any>
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <vector>

namespace skuldenka::core::runtime {
using skuldenka::core::model::Variant;

class Instance final : public std::enable_shared_from_this<Instance>
{
public:
  static std::shared_ptr<Instance> create(std::shared_ptr<ImplemNode> implem,
                                          SlotCount slotCount);

public:
  ~Instance();

  template <typename T>
  T const& slot(SlotCount index);

  Variant operator()(traitId_t traitId, methodId_t methodId);
  template <std::size_t argC>
  Variant operator()(traitId_t traitId, methodId_t methodId,
                     std::array<Variant, argC> const& args);

  Variant& operator[](SlotCount index);

  std::shared_ptr<ImplemNode> node();

private:
  Instance(std::shared_ptr<ImplemNode> implem, SlotCount slotCount);

private:
  std::shared_ptr<ImplemNode> m_implemNode;
  std::vector<Variant> m_slots;
};

/* TEMPLATE IMPLEMENTATION */
template <std::size_t argC>
Variant Instance::operator()(traitId_t traitId, methodId_t methodId,
                             std::array<Variant, argC> const& args)
{
  return (*m_implemNode)[traitId](methodId, this->shared_from_this(), args);
}

template <typename T>
T const& Instance::slot(SlotCount index)
{
  return (*this)[index].as<T>();
}

} // namespace skuldenka::core::runtime

#endif