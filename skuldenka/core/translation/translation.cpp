/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "translation.hpp"

#include <cassert>
#include <memory>
#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/ImplemAssign.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/runtime/Program.hpp>
#include <skuldenka/core/runtime/typedefs.hpp>
#include <skuldenka/core/translation/ImplemNodeBuilder.hpp>
#include <skuldenka/core/translation/LinkingContext.hpp>
#include <vector>

using namespace skuldenka::core;

namespace skuldenka::core::translation {

std::unique_ptr<runtime::Program> buildProgram(
    std::shared_ptr<model::Model> model, std::shared_ptr<model::Program> program)
{
  auto lc = std::make_shared<LinkingContext>(model, program);
  auto inb = std::make_shared<ImplemNodeBuilder>(lc);

  auto mainNode = inb->requestNode(program->implem());

  // finding traitId for main
  auto mainTrait = model->find<model::Trait>("main");
  assert(mainTrait);
  runtime::traitId_t mainTraitId = lc->traitId(mainTrait);

  // method id for main is 0
  runtime::methodId_t mainMethodId = 0;

  return std::make_unique<runtime::Program>(mainNode, mainTraitId, mainMethodId);
}

} // namespace skuldenka::core::translation