/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_TRANSLATION
#define SKULDENKA_CORE_TRANSLATION

#include <map>
#include <memory>
#include <skuldenka/core/runtime/Vtable.hpp>
#include <skuldenka/core/runtime/typedefs.hpp>
#include <vector>

namespace skuldenka::core::runtime {
class Program;
}

namespace skuldenka::core::model {
class Program;
class ImplemAssign;
class Trait;
class Model;
class Implem;
} // namespace skuldenka::core::model

namespace skuldenka::core::translation {

void listTraits(
    skuldenka::core::model::ImplemAssign const& assign,
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Trait>>&
        traits); //#FIXME should be moved into ImplemAssign and/or program ?

void listImplems(
    skuldenka::core::model::ImplemAssign const& assign,
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Implem>>&
        implems); //#FIXME should be moved into ImplemAssign and/or program ?

class LinkingContext;

struct Implem final
{
  Implem(std::shared_ptr<skuldenka::core::model::Implem> implem,
         LinkingContext const& context);
  Implem(Implem const& origin);
  ~Implem();
  std::vector<skuldenka::core::runtime::Vtable> m_vtables;
  std::vector<skuldenka::core::runtime::traitId_t> m_traitsReq;
  size_t traitIdReduction;
  Implem& operator=(Implem const& other);
};

class LinkingContext final
{
public:
  LinkingContext(std::shared_ptr<skuldenka::core::model::Model> model,
                 std::shared_ptr<skuldenka::core::model::Program> program);
  ~LinkingContext();

  skuldenka::core::runtime::traitId_t const&
      traitId(std::shared_ptr<skuldenka::core::model::Trait>) const;
  Implem const& implem(std::shared_ptr<skuldenka::core::model::Implem>) const;

  size_t traitCount() const;

  std::shared_ptr<skuldenka::core::model::Model> model() const;

private:
  std::shared_ptr<skuldenka::core::model::Model> m_model;
  std::map<std::shared_ptr<skuldenka::core::model::Trait>,
           skuldenka::core::runtime::traitId_t>
      m_traitIds;
  std::map<std::shared_ptr<skuldenka::core::model::Implem>, Implem> m_implems;
};

//#FIXME traitId() and implem() should throw exceptions when not found !?

} // namespace skuldenka::core::translation
#endif
