/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "ImplemNodeBuilder.hpp"
#include "LinkingContext.hpp"
#include <skuldenka/core/model/ImplemAssign.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>

using skuldenka::core::model::ImplemAssign;
using skuldenka::core::model::ImplemSubAssign;
using skuldenka::core::runtime::ImplemNode;

namespace skuldenka::core::translation {

std::string ImplemNodeBuilder::computeAssignKey(
    skuldenka::core::model::ImplemAssign const& assign)
{
  // first is the name of the implem
  std::string key("");
  key += assign.implem().absoluteStr();
  // then is, between {}, the key of each sub assign, sorted by their appearance in Implem
  key += "{";

  auto implem = assign.implem().find();
  assert(implem);

  for (auto const& subname : implem->implemRequests()) {
    auto sub =
        std::find_if(assign.subs().begin(), assign.subs().end(),
                     [&subname](std::shared_ptr<ImplemSubAssign> const& val) {
                       return subname->name() == val->request();
                     });
    assert(sub != assign.subs().end());
    key += computeAssignKey(*(*sub));

    key += ",";
  }
  key += "}";

  return key;
}

ImplemNodeBuilder::ImplemNodeBuilder(std::shared_ptr<LinkingContext> context) :
 m_context(context)
{}

ImplemNodeBuilder::~ImplemNodeBuilder()
{}

std::shared_ptr<skuldenka::core::runtime::ImplemNode>
ImplemNodeBuilder::requestNode(skuldenka::core::model::ImplemAssign const& assign)
{
  // if found in memory, return
  auto elem = m_implemNodes.find(ImplemNodeBuilder::computeAssignKey(assign));
  if (elem != m_implemNodes.end()) {
    return elem->second;
  }

  // list sub implems and request their nodes
  std::vector<std::shared_ptr<ImplemNode>> subs;
  for (auto const& sub : assign.subs()) {
    subs.push_back(this->requestNode(*sub));
  }

  Implem impl = m_context->implem(assign.implem().find());

  auto inode = ImplemNode::create(impl.m_vtables, impl.traitIdReduction, subs,
                                  impl.m_traitsReq);
  m_implemNodes.emplace(ImplemNodeBuilder::computeAssignKey(assign), inode);
  return inode;
}

} // namespace skuldenka::core::translation