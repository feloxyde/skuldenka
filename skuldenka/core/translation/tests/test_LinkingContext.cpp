/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "fixture_Model.hpp"
#include <array>
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/translation/LinkingContext.hpp>
#include <sstream>
#include <string>
#include <unordered_set>

using skuldenka::core::model::Variant;
using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::runtime;
using namespace skuldenka::core::translation;

SUITE(translation)
{
  TEST_FIXTURE(FixtureModel, listTraits)
  {
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Trait>> traits;
    auto program = model->find<skuldenka::core::model::Program>("run1simple");

    listTraits(program->implem(), traits);

    CHECK_EQUAL(4, traits.size());
    CHECK_EQUAL(1, traits.count(model->find<SubModule>("commons")->find<Trait>(
                       "drive")));
    CHECK_EQUAL(1, traits.count(model->find<SubModule>("commons")->find<Trait>(
                       "engine")));
    CHECK_EQUAL(1, traits.count(model->find<SubModule>("commons")->find<Trait>(
                       "vehicle")));
    CHECK_EQUAL(1, traits.count(model->find<Trait>("main")));
  }

  TEST_FIXTURE(FixtureModel, listImplems)
  {
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Implem>> implems;
    auto program = model->find<skuldenka::core::model::Program>("run1simple");

    listImplems(program->implem(), implems);

    CHECK_EQUAL(4, implems.size());
    CHECK_EQUAL(
        1, implems.count(model->find<SubModule>("drives")
                             ->find<skuldenka::core::model::Implem>("wheel")));
    CHECK_EQUAL(1, implems.count(model->find<SubModule>("engines")
                                     ->find<skuldenka::core::model::Implem>(
                                         "electricEngine")));
    CHECK_EQUAL(
        1, implems.count(model->find<SubModule>("vehicles")
                             ->find<skuldenka::core::model::Implem>("car")));
    CHECK_EQUAL(
        1, implems.count(model->find<skuldenka::core::model::Implem>("run1")));
  }
}

SUITE(Implem)
{
  TEST(Construction)
  {
    //#FIXME implement this
    CHECK(false);
  }
}

SUITE(LinkingContext)
{

  TEST_FIXTURE(FixtureModel, Construction)
  {
    LinkingContext lc(
        model, model->find<skuldenka::core::model::Program>("run1simple"));
    CHECK(lc.model() == model);
    //#FIXME is there something else to check ?
  }

  TEST_FIXTURE(FixtureModel, traitId)
  {
    // Do we have a way to check for trait ID?

    LinkingContext lc(
        model, model->find<skuldenka::core::model::Program>("run1simple"));

    assert(model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
        "drive"));
    traitId_t idDrive = lc.traitId(
        model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
            "drive"));

    assert(model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
        "engine"));
    traitId_t idEngine = lc.traitId(
        model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
            "engine"));

    assert(model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
        "vehicle"));
    traitId_t idVehicle = lc.traitId(
        model->find<SubModule>("commons")->find<skuldenka::core::model::Trait>(
            "vehicle"));

    assert(model->find<skuldenka::core::model::Trait>("main"));
    traitId_t idMain =
        lc.traitId(model->find<skuldenka::core::model::Trait>("main"));

    CHECK(idDrive != idEngine && idDrive != idVehicle && idDrive != idMain);
    CHECK(idEngine != idVehicle && idEngine != idMain);
    CHECK(idVehicle != idMain);

    CHECK(idDrive < 4);
    CHECK(idEngine < 4);
    CHECK(idVehicle < 4);
    CHECK(idMain < 4);

    CHECK(false); //#FIXME check for exception throw !
    CHECK(false);
  }

  TEST_FIXTURE(FixtureModel, implem)
  {
    // CHECK that each implem gets an Implem translation
    LinkingContext lc(
        model, model->find<skuldenka::core::model::Program>("run1simple"));

    assert(model->find<SubModule>("drives"));
    assert(model->find<SubModule>("drives")->find<skuldenka::core::model::Implem>(
        "wheel"));
    lc.implem(
        model->find<SubModule>("drives")->find<skuldenka::core::model::Implem>(
            "wheel"));

    lc.implem(
        model->find<SubModule>("engines")->find<skuldenka::core::model::Implem>(
            "electricEngine"));
    lc.implem(model->find<SubModule>("vehicles")
                  ->find<skuldenka::core::model::Implem>("car"));

    lc.implem(model->find<skuldenka::core::model::Implem>("run1"));

    CHECK(false); //#FIXME check for exception throw !
    CHECK(false); // check implem created are valid
    CHECK(false);
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}