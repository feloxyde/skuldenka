/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_TRANSLATION_TESTS_FIXTURE_MODEL_HPP
#define SKULDENKA_CORE_TRANSLATION_TESTS_FIXTURE_MODEL_HPP

#include <cassert>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "mock_BodyWrapper.hpp"

#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Destructor.hpp>
#include <skuldenka/core/model/ImplemAssign.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/model/typedefs.hpp>
#include <skuldenka/core/runtime/ImplemNode.hpp>
#include <skuldenka/core/runtime/Instance.hpp>

using namespace skuldenka::core::model;
using namespace skuldenka::core::runtime;
using skuldenka::core::model::Variant;

// this is a REAL fake (lol) model
// one program is about driving a car
// the other is about driving a truck
class FixtureModel
{
public:
  static std::vector<std::string> output;

public:
  FixtureModel() : model(std::make_shared<Model>())
  {
    assert(model);
    initCommons();
    initDrives();
    initEngines();
    initVehicles();
    initMains();
    initPrograms();
    skuldenka::core::model::ErrorLog errs = model->check();
    assert(!errs);

    output.clear();
  }

  ~FixtureModel()
  {}

  void initCommons();

  void initDrives();

  void initEngines();

  void initVehicles();

  void initMains();

  void initPrograms();

public:
  std::shared_ptr<Model> model;
};

std::vector<std::string> FixtureModel::output;

void FixtureModel::initCommons()
{

  // create namespace commons
  model->declareSubModule("commons");
  {
    auto commons = model->find<SubModule>("commons");

    commons->declareTrait("drive");
    {
      auto drive = commons->find<Trait>("drive");
      drive->sm("create").r({"drive"});
      drive->mm("go");
    }

    commons->declareTrait("engine");
    {
      auto engine = commons->find<Trait>("engine");
      engine->sm("create").r({"engine"});
      engine->mm("start");
    }

    commons->declareTrait("vehicle");
    {
      auto vehicle = commons->find<Trait>("vehicle");
      vehicle->sm("create").r({"vehicle"});
      vehicle->mm("advance");
    }
  }

  model->declareTrait("main");
  {
    auto main = model->find<Trait>("main");
    main->sm("main");
  }
}

Variant wheel_sm_create(std::shared_ptr<ImplemNode> node)
{
  //#FIXME shoud use node.instance() or node.alloc() instead ?
  FixtureModel::output.push_back("creating wheel");
  return Variant(Instance::create(node, 0));
}

Variant wheel_mm_go(std::shared_ptr<Instance> instance)
{
  FixtureModel::output.push_back("wheel");
  return Variant();
}

void FixtureModel::initDrives()
{
  // create namespace commons
  model->declareSubModule("drives");
  {
    auto drives = model->find<SubModule>("drives");

    drives->declareImplem("wheel", 0);
    {
      auto wheel = drives->find<skuldenka::core::model::Implem>("wheel");
      wheel->t(Identifier("drive"), std::list<Identifier>({"commons"}), true);
      wheel->requestTrait(Identifier("drive"),
                          std::list<Identifier>({"commons"}), true);
      skuldenka::core::model::Vtable& vwheel = wheel->defineVtable(
          "drive", std::list<Identifier>({"commons"}), true);
      vwheel
          .defineMethod("create",
                        std::make_shared<StaticBodyWrapper0a>(wheel_sm_create))
          .r({"::commons::drive"});

      vwheel.defineMethod("go",
                          std::make_shared<MemberBodyWrapper0a>(wheel_mm_go));
    }
  }
}

Variant electricEngine_sm_create(std::shared_ptr<ImplemNode> node)
{
  //#FIXME shoud use node.instance() or node.alloc() instead ?
  FixtureModel::output.push_back("creating electric engine");
  return Variant(Instance::create(node, 0));
}

Variant electricEngine_mm_start(std::shared_ptr<Instance> instance)
{
  FixtureModel::output.push_back("starting electric engine");
  return Variant();
}

void FixtureModel::initEngines()
{
  model->declareSubModule("engines");
  {
    auto engines = model->find<SubModule>("engines");

    engines->declareImplem("electricEngine", 0);
    {
      auto electric =
          engines->find<skuldenka::core::model::Implem>("electricEngine");
      electric->t("engine", std::list<Identifier>({"commons"}), true);
      electric->requestTrait("engine", std::list<Identifier>({"commons"}), true);
      skuldenka::core::model::Vtable& velectric = electric->defineVtable(
          "engine", std::list<Identifier>({"commons"}), true);
      velectric
          .defineMethod("create", std::make_shared<StaticBodyWrapper0a>(
                                      electricEngine_sm_create))
          .r({"::commons::engine"});

      velectric.defineMethod("start", std::make_shared<MemberBodyWrapper0a>(
                                          electricEngine_mm_start));
    }
  }
}

Variant car_sm_create(std::shared_ptr<ImplemNode> node)
{
  //#FIXME shoud use node.instance() or node.alloc() instead ?
  FixtureModel::output.push_back("creating car");
  Variant car = Instance::create(node, 5);
  assert(car.is<std::shared_ptr<Instance>>());
  // creating engine
  (*(car.as<std::shared_ptr<Instance>>()))[0] =
      (*(node->sub(0)))(node->traitId(1), 0);
  (*(car.as<std::shared_ptr<Instance>>()))[1] =
      (*(node->sub(1)))(node->traitId(2), 0);
  (*(car.as<std::shared_ptr<Instance>>()))[2] =
      (*(node->sub(1)))(node->traitId(2), 0);
  (*(car.as<std::shared_ptr<Instance>>()))[3] =
      (*(node->sub(1)))(node->traitId(2), 0);
  (*(car.as<std::shared_ptr<Instance>>()))[4] =
      (*(node->sub(1)))(node->traitId(2), 0);
  // starting engine
  (*(car.as<std::shared_ptr<Instance>>()))[0].as<std::shared_ptr<Instance>>()->
  operator()(node->traitId(1), 0);
  return car;
}

Variant car_mm_advance(std::shared_ptr<Instance> instance)
{
  FixtureModel::output.push_back("advancing with car :");
  // calling methods of each drive
  (*instance)[1].as<std::shared_ptr<Instance>>()->operator()(
      instance->node()->traitId(2), 0);
  (*instance)[2].as<std::shared_ptr<Instance>>()->operator()(
      instance->node()->traitId(2), 0);
  (*instance)[3].as<std::shared_ptr<Instance>>()->operator()(
      instance->node()->traitId(2), 0);
  (*instance)[4].as<std::shared_ptr<Instance>>()->operator()(
      instance->node()->traitId(2), 0);
  return Variant();
}

void FixtureModel::initVehicles()
{
  model->declareSubModule("vehicles");
  {
    auto vehicles = model->find<SubModule>("vehicles");

    vehicles->declareImplem("car", 5);
    {
      auto car = vehicles->find<skuldenka::core::model::Implem>("car");
      car->t("vehicle", std::list<Identifier>({"commons"}), true);
      car->requestTrait("vehicle", std::list<Identifier>({"commons"}), true);
      car->requestTrait("engine", std::list<Identifier>({"commons"}), true);
      car->requestTrait("drive", std::list<Identifier>({"commons"}), true);

      car->requestImplem("engine").t("engine", std::list<Identifier>({"commons"}),
                                     true); // implem0
      car->requestImplem("drive").t("drive", std::list<Identifier>({"commons"}),
                                    true); // implem1

      skuldenka::core::model::Vtable& vcar = car->defineVtable(
          "vehicle", std::list<Identifier>({"commons"}), true);

      vcar.defineMethod("create",
                        std::make_shared<StaticBodyWrapper0a>(car_sm_create))
          .r({"::commons::vehicle"});

      vcar.defineMethod("advance",
                        std::make_shared<MemberBodyWrapper0a>(car_mm_advance));
    }
  }
}

Variant run1_sm_main(std::shared_ptr<ImplemNode> node)
{
  //#FIXME shoud use node.instance() or node.alloc() instead ?
  FixtureModel::output.push_back("initializing main");
  Variant vehicle = node->sub(0)->operator()(node->traitId(1), 0);
  assert(vehicle.is<std::shared_ptr<Instance>>());
  // advance three times with vehicle
  for (size_t i = 0; i < 3; ++i) {
    vehicle.as<std::shared_ptr<Instance>>()->operator()(node->traitId(1), 0);
  }

  //#FIXME implement here
  return Variant();
}

void FixtureModel::initMains()
{
  model->declareImplem("run1", 1);
  {
    auto run1 = model->find<skuldenka::core::model::Implem>("run1");
    run1->t("main");
    run1->requestTrait("main", std::list<Identifier>({}), true);
    run1->requestTrait("vehicle", std::list<Identifier>({"commons"}), true);
    run1->requestImplem("vehicle").t(
        "vehicle", std::list<Identifier>({"commons"}), true); // implem1

    skuldenka::core::model::Vtable& vrun1 = run1->defineVtable("main");

    vrun1.defineMethod("main",
                       std::make_shared<StaticBodyWrapper0a>(run1_sm_main));
  }
}

void FixtureModel::initPrograms()
{
  model->declareProgram("run1simple", "run1");
  {
    auto run1simple = model->find<Program>("run1simple");
    auto& assign = run1simple->implem();
    auto& vehicle = assign.addSub("vehicle", "car", {"vehicles"}, true);
    vehicle.addSub("engine", "electricEngine", {"engines"}, true);
    vehicle.addSub("drive", "wheel", {"drives"}, true);
  }
}

#endif