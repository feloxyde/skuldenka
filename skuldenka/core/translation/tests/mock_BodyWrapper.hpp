/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CORE_TRANSLATION_TESTS_MOCK_BODYWRAPPER_HPP
#define SKULDENKA_CORE_TRANSLATION_TESTS_MOCK_BODYWRAPPER_HPP

#include <array>
#include <cassert>
#include <memory>
#include <string>
#include <vector>

#include <skuldenka/core/model/Component.hpp>
#include <skuldenka/core/model/Destructor.hpp>
#include <skuldenka/core/model/ImplemAssign.hpp>
#include <skuldenka/core/model/Method.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Type.hpp>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/model/typedefs.hpp>

using namespace skuldenka::core::model;

namespace skuldenka::core::runtime {
class ImplemNode;
class Instance;
} // namespace skuldenka::core::runtime

using skuldenka::core::runtime::ImplemNode;
using skuldenka::core::runtime::Instance;

template <typename ThisType>
class MockBodyWrapper0a : public Method<ThisType>::Body
{
public:
  MockBodyWrapper0a(Variant (*method)(std::shared_ptr<ThisType>)) :
   m_method(method){};

  virtual ~MockBodyWrapper0a()
  {}

  virtual std::shared_ptr<typename Method<ThisType>::Body> copy()
  {
    return std::make_shared<MockBodyWrapper0a<ThisType>>(m_method);
  }

  virtual Variant operator()(std::shared_ptr<ThisType> thisptr)
  {
    return m_method(thisptr);
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 1> const& args)
  {
    throw "called with wrong params";
  }

  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 2> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 3> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 4> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 5> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 6> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 7> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 8> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 9> const& args)
  {
    throw "called with wrong params";
  }
  virtual Variant operator()(std::shared_ptr<ThisType> thisptr,
                             std::array<Variant, 10> const& args)
  {
    throw "called with wrong params";
  }

private:
  Variant (*m_method)(std::shared_ptr<ThisType>);
};

typedef MockBodyWrapper0a<ImplemNode> StaticBodyWrapper0a;
typedef MockBodyWrapper0a<Instance> MemberBodyWrapper0a;

#endif