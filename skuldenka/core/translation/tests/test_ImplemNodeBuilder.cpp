/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include <UnitTest++/UnitTest++.h>

#include "fixture_Model.hpp"
#include <array>
#include <iostream>
#include <memory>
#include <skuldenka/core/model/Variant.hpp>
#include <skuldenka/core/translation/ImplemNodeBuilder.hpp>
#include <skuldenka/core/translation/LinkingContext.hpp>
#include <sstream>
#include <string>

using skuldenka::core::model::Variant;
using namespace skuldenka::core;
using namespace std;
using namespace skuldenka::core::runtime;
using namespace skuldenka::core::translation;
using namespace skuldenka::core::model;

SUITE(ImplemNodeBuilder)
{

  TEST_FIXTURE(FixtureModel, computeAssignKey)
  {
    auto const& assign1 =
        model->find<skuldenka::core::model::Program>("run1simple")->implem();
    auto const& assign2 =
        *(model->find<skuldenka::core::model::Program>("run1simple")
              ->implem()
              .subs()[0]);

    std::string key1 = ImplemNodeBuilder::computeAssignKey(assign1);
    std::string key2 = ImplemNodeBuilder::computeAssignKey(assign2);

    CHECK_EQUAL("::run1{::vehicles::car{::engines::electricEngine{},::drives::"
                "wheel{},},}",
                key1);

    CHECK_EQUAL("::vehicles::car{::engines::electricEngine{},::drives::"
                "wheel{},}",
                key2);

    CHECK(false); //#FIXME check that two implem assign with same structure have same key
  }

  TEST_FIXTURE(FixtureModel, construction)
  {
    auto lc = std::make_shared<LinkingContext>(
        model, model->find<skuldenka::core::model::Program>("run1simple"));
    ImplemNodeBuilder nodeBuilder(lc);
  }

  TEST_FIXTURE(FixtureModel, requestNodeVtable)
  {
    auto lc = std::make_shared<LinkingContext>(
        model, model->find<skuldenka::core::model::Program>("run1simple"));
    ImplemNodeBuilder nodeBuilder(lc);

    auto ind = nodeBuilder.requestNode(
        model->find<skuldenka::core::model::Program>("run1simple")->implem());

    // check that nodes created are right ones
    auto vehicle = ind->sub(0);
    auto drive = vehicle->sub(1);
    auto engine = vehicle->sub(0);

    Variant driveInstance = drive->operator()(vehicle->traitId(2), 0);
    Variant engineInstance = engine->operator()(vehicle->traitId(1), 0);

    CHECK(drive != engine);
    CHECK_EQUAL("creating wheel", FixtureModel::output[0]);
    CHECK_EQUAL("creating electric engine", FixtureModel::output[1]);

    CHECK(false);
  }

  TEST_FIXTURE(FixtureModel, requestNodeExec)
  {
    auto lc = std::make_shared<LinkingContext>(
        model, model->find<skuldenka::core::model::Program>("run1simple"));
    ImplemNodeBuilder nodeBuilder(lc);

    auto ind = nodeBuilder.requestNode(
        model->find<skuldenka::core::model::Program>("run1simple")->implem());

    // CHECK that the node is well formed
    ind->operator()(ind->traitId(0), 0);

    size_t line = 0;
    CHECK_EQUAL(23, FixtureModel::output.size());
    CHECK_EQUAL("initializing main", FixtureModel::output[line++]);
    CHECK_EQUAL("creating car", FixtureModel::output[line++]);
    CHECK_EQUAL("creating electric engine", FixtureModel::output[line++]);
    CHECK_EQUAL("creating wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("creating wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("creating wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("creating wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("starting electric engine", FixtureModel::output[line++]);
    CHECK_EQUAL("advancing with car :", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("advancing with car :", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("advancing with car :", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL("wheel", FixtureModel::output[line++]);
    CHECK_EQUAL(23, line);

    CHECK(false); //#FIXME CHECK that two different assigns with same structure return same node
  }
}

int main(int, const char*[])
{
  return UnitTest::RunAllTests();
}