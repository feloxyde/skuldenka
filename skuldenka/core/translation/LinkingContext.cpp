/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#include "LinkingContext.hpp"
#include <algorithm>
#include <iostream>
#include <limits>
#include <skuldenka/core/model/Implem.hpp>
#include <skuldenka/core/model/ImplemAssign.hpp>
#include <skuldenka/core/model/Program.hpp>
#include <skuldenka/core/model/Trait.hpp>
#include <skuldenka/core/model/Vtable.hpp>
#include <skuldenka/core/runtime/Vtable.hpp>

using namespace skuldenka::core;

namespace skuldenka::core::translation {

void listTraits(
    skuldenka::core::model::ImplemAssign const& assign,
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Trait>>& traits)
{
  // listing traits for the current Implem
  auto implem = assign.implem().find();
  assert(implem);

  auto subset = implem->unpack();
  traits.insert(subset.begin(), subset.end());

  // listing traits for subs
  for (auto const& sub : assign.subs()) {
    listTraits(*sub, traits);
  }
}

void listImplems(
    skuldenka::core::model::ImplemAssign const& assign,
    std::unordered_set<std::shared_ptr<skuldenka::core::model::Implem>>& implems)
{ // listing traits for the current Implem
  auto implem = assign.implem().find();
  assert(implem);

  implems.insert(implem);

  // listing traits for subs
  for (auto const& sub : assign.subs()) {
    listImplems(*sub, implems);
  }
}

Implem::Implem(std::shared_ptr<skuldenka::core::model::Implem> implem,
               LinkingContext const& context)
{
  assert(implem);
  // linking

  // listing vtables of the implem, at the right place of a vector
  runtime::traitId_t minId(std::numeric_limits<runtime::traitId_t>::max());
  runtime::traitId_t maxId(std::numeric_limits<runtime::traitId_t>::min());
  std::vector<runtime::Vtable> vtables(context.traitCount());
  for (auto const& vtable : implem->vtables()) {
    std::vector<std::shared_ptr<model::StaticMethod::Body>> staticMethods;
    std::vector<std::shared_ptr<model::MemberMethod::Body>> memberMethods;

    //#FIXME sort methods here ????
    for (auto sm : vtable->methods<model::StaticMethod>()) {
      assert(sm->body());
      staticMethods.push_back(sm->body());
    }

    for (auto mm : vtable->methods<model::MemberMethod>()) {
      assert(mm->body());
      memberMethods.push_back(mm->body());
    }

    runtime::traitId_t traitId = context.traitId(vtable->trait().find());
    if (minId > traitId) {
      minId = traitId;
    }

    if (maxId < traitId) {
      maxId = traitId;
    }

    vtables[traitId] = runtime::Vtable(staticMethods, memberMethods);
  }

  // then copy temp vector into new vector
  traitIdReduction = minId;
  m_vtables.resize(maxId + 1 - minId);
  for (size_t id = minId; id <= maxId; ++id) {
    m_vtables[id - traitIdReduction] = vtables[id];
  }

  //#FIXME build and retreive traits requests
  for (auto const& trait : implem->traitRequests()) {
    m_traitsReq.push_back(context.traitId(trait->trait().find()));
  }
}

Implem::Implem(Implem const& origin) :
 m_vtables(origin.m_vtables),
 m_traitsReq(origin.m_traitsReq),
 traitIdReduction(origin.traitIdReduction)
{}

Implem::~Implem()
{}

LinkingContext::LinkingContext(std::shared_ptr<model::Model> model,
                               std::shared_ptr<model::Program> program) :
 m_model(model)
{
  assert(m_model);
  assert(program);

  // assigning an runtime::id on traits
  // listing traits
  { // block of scope to release trait set as soon as possible
    std::unordered_set<std::shared_ptr<model::Trait>> traits;
    listTraits(program->implem(), traits);
    // assigning id completely arbitrarily
    runtime::traitId_t id = 0;
    for (auto& trait : traits) {
      m_traitIds.emplace(trait, id++);
    }
  }

  { // listing implems
    std::unordered_set<std::shared_ptr<model::Implem>> implems;
    listImplems(program->implem(), implems);
    for (auto& implem : implems) {
      m_implems.emplace(implem, Implem(implem, *this));
    }
  }
}

Implem& Implem::operator=(Implem const& other)
{
  this->m_vtables = other.m_vtables;
  this->m_traitsReq = other.m_traitsReq;
  this->traitIdReduction = other.traitIdReduction;
  return *this;
}

LinkingContext::~LinkingContext()
{}

runtime::traitId_t const&
LinkingContext::traitId(std::shared_ptr<model::Trait> trait) const
{
  //#FIXME catch exception here and throw a more explicit one
  return m_traitIds.at(trait);
}

Implem const& LinkingContext::implem(std::shared_ptr<model::Implem> implem) const
{
  //#FIXME catch exception here and throw a more explicit one
  return m_implems.at(implem);
}

size_t LinkingContext::traitCount() const
{
  return m_traitIds.size();
}

std::shared_ptr<skuldenka::core::model::Model> LinkingContext::model() const
{
  return m_model;
}

} // namespace skuldenka::core::translation