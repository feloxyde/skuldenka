/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CPPMEMBERMETHOD_HPP
#define SKULDENKA_CPPMEMBERMETHOD_HPP

#include "Method.hpp"
#include "Variant.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <iostream>
#include <memory>
#include <string>
//#FIXME add prototype usage and Trait adding

namespace skuldenka {

class Trait;

template <typename R, typename... paramsT>
class CppStaticMethod final : public StaticMethod
{
public:
  CppStaticMethod(Prototype<StaticMethod> const& proto, R (*method)(paramsT...));
  virtual ~CppStaticMethod();

  virtual Variant operator()(std::vector<Variant> const& args);

private:
  R (*m_method)(paramsT...);
};

template <typename R, typename... paramsT>
std::shared_ptr<CppStaticMethod<R, paramsT...>> wrapCppMemberMethod(
    Prototype<StaticMethod> const& proto, R (*method)(paramsT...));

template <typename R, typename... paramsT>
R staticMethodCaller(R (*method)(paramsT...), std::vector<Variant> const& args);

/*



 TEMPLATE IMPLEMENTATION



 */

template <typename R, typename... paramsT>
CppStaticMethod<R, paramsT...>::CppStaticMethod(Prototype<StaticMethod> const& proto,
                                                R (*method)(paramsT...)) :
 StaticMethod::StaticMethod(proto), m_method(method)
{
  //#FIXME here add checks on types vs proto ?
}

template <typename R, typename... paramsT>
CppStaticMethod<R, paramsT...>::~CppStaticMethod()
{}

template <typename R, typename... paramsT>
Variant CppStaticMethod<R, paramsT...>::operator()(std::vector<Variant> const& args)
{
  if constexpr (std::is_same<R, void>::value) {
    staticMethodCaller(m_method, args);
    return Variant();
  } else {
    return Variant(staticMethodCaller(m_method, args));
  }
}

/*
----
*/

template <typename R, typename... paramsT>
std::shared_ptr<CppStaticMethod<R, paramsT...>>
wrapCppStaticMethod(Prototype<StaticMethod> const& proto, R (*method)(paramsT...))
{
  return std::make_shared<CppStaticMethod<R, paramsT...>>(proto, method);
}

/*
---- Specializing for 0 to 10 params
*/

template <typename R, typename... paramsT>
R staticMethodCaller(R (*method)(paramsT...), std::vector<Variant> const& args)
{
  throw not_yet_implemented(
      "CppStaticMethod::staticMethodCaller, too many params !");
}

template <typename R>
R staticMethodCaller(R (*method)(), std::vector<Variant> const& args)
{
  return method();
}

template <typename R, typename T0>
R staticMethodCaller(R (*method)(T0), std::vector<Variant> const& args)
{
  return method(args[0].as<T0>());
}

template <typename R, typename T0, typename T1>
R staticMethodCaller(R (*method)(T0, T1), std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>());
}

template <typename R, typename T0, typename T1, typename T2>
R staticMethodCaller(R (*method)(T0, T1, T2), std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3>
R staticMethodCaller(R (*method)(T0, T1, T2, T3), std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3, typename T4>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4, T5),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4, T5, T6),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6, typename T7>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4, T5, T6, T7),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6, typename T7, typename T8>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4, T5, T6, T7, T8),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>(), args[8].as<T8>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3, typename T4,
          typename T5, typename T6, typename T7, typename T8, typename T9>
R staticMethodCaller(R (*method)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9),
                     std::vector<Variant> const& args)
{
  return method(args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>(), args[8].as<T8>(),
                args[9].as<T9>());
}

} // namespace skuldenka

#endif
