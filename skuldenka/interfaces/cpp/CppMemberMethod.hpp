/**
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

#ifndef SKULDENKA_CPPMEMBERMETHOD_HPP
#define SKULDENKA_CPPMEMBERMETHOD_HPP

#include "Method.hpp"
#include "Variant.hpp"
#include "exception.hpp"
#include "typedefs.hpp"
#include <memory>
//#FIXME add prototype usage and Trait adding

namespace skuldenka {

class Trait;

template <typename R, typename... paramsT>
class CppMemberMethod final : public MemberMethod
{
public:
  CppMemberMethod(Prototype<MemberMethod> const& proto,
                  R (*method)(std::shared_ptr<Instance>, paramsT...));
  virtual ~CppMemberMethod();

  virtual Variant operator()(std::shared_ptr<Instance> instance,
                             std::vector<Variant> const& args);

private:
  R (*m_method)(std::shared_ptr<Instance>, paramsT...);
};

template <typename R, typename... paramsT>
std::shared_ptr<CppMemberMethod<R, paramsT...>>
wrapCppMemberMethod(Prototype<MemberMethod> const& proto,
                    R (*method)(std::shared_ptr<Instance>, paramsT...));

template <typename R, typename... paramsT>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, paramsT...),
                     std::shared_ptr<Instance>, std::vector<Variant> const& args);

/*



 TEMPLATE IMPLEMENTATION



 */

template <typename R, typename... paramsT>
CppMemberMethod<R, paramsT...>::CppMemberMethod(
    Prototype<MemberMethod> const& proto,
    R (*method)(std::shared_ptr<Instance>, paramsT...)) :
 MemberMethod::MemberMethod(proto), m_method(method)
{}

template <typename R, typename... paramsT>
CppMemberMethod<R, paramsT...>::~CppMemberMethod()
{}

template <typename R, typename... paramsT>
Variant CppMemberMethod<R, paramsT...>::operator()(
    std::shared_ptr<Instance> instance, std::vector<Variant> const& args)
{
  if constexpr (std::is_same<R, void>::value) {
    memberMethodCaller(m_method, instance, args);
    return Variant();
  } else {
    return Variant(memberMethodCaller(m_method, instance, args));
  }
}

/*
----
*/

template <typename R, typename... paramsT>
std::shared_ptr<CppMemberMethod<R, paramsT...>>
wrapCppMemberMethod(Prototype<MemberMethod> const& proto,
                    R (*method)(std::shared_ptr<Instance>, paramsT...))
{
  return std::make_shared<CppMemberMethod<R, paramsT...>>(proto, method);
}

/*
---- Specializing for 0 to 10 params
*/

template <typename R, typename... paramsT>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, paramsT...),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  throw not_yet_implemented(
      "CppMemberMethod::memberMethodCaller, too many params !");
}

template <typename R>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance);
}

template <typename R, typename T0>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>());
}

template <typename R, typename T0, typename T1>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>());
}

template <typename R, typename T0, typename T1, typename T2>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3, typename T4>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4,
                                 T5),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4,
                                 T5, T6),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6, typename T7>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4,
                                 T5, T6, T7),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3,
          typename T4, typename T5, typename T6, typename T7, typename T8>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4,
                                 T5, T6, T7, T8),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>(), args[8].as<T8>());
}

template <typename R, typename T0, typename T1, typename T2, typename T3, typename T4,
          typename T5, typename T6, typename T7, typename T8, typename T9>
R memberMethodCaller(R (*method)(std::shared_ptr<Instance>, T0, T1, T2, T3, T4,
                                 T5, T6, T7, T8, T9),
                     std::shared_ptr<Instance> instance,
                     std::vector<Variant> const& args)
{
  return method(instance, args[0].as<T0>(), args[1].as<T1>(), args[2].as<T2>(),
                args[3].as<T3>(), args[4].as<T4>(), args[5].as<T5>(),
                args[6].as<T6>(), args[7].as<T7>(), args[8].as<T8>(),
                args[9].as<T9>());
}

} // namespace skuldenka

#endif
