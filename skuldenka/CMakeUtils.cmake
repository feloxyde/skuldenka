#[[
Copyright or © or Copr. Félix Bertoni (2020)

felix.bertoni987@gmail.com

This software is a computer program whose purpose is to provide an easy way 
to create softwares with a modulare design.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
]]


function(SETUP_COVERAGE_TARGET_FOR target_name dependency_list)

    SETUP_TARGET_FOR_COVERAGE_GCOVR_HTML(
        NAME "coverage_${target_name}"                 
        EXECUTABLE ctest -j ${N_PROC} -R "test_${target_name}*" || (exit 0) # Executable in PROJECT_BINARY_DIR
        DEPENDENCIES ${dependency_list}
    )

endfunction(SETUP_COVERAGE_TARGET_FOR)


## TEST_DIR will test everything in a dir

function(TEST_DIR target tests_dir )
#loading test file
    set(local_all_tests_target "testo_${target}")
    add_custom_target(${local_all_tests_target} COMMAND ctest --output-on-failure -j ${N_COUNT}  -R "test_${target}*")   
    if(ENABLE_COVERAGE)
        add_custom_target(${local_all_tests_target}_coverage_dep)  
    endif(ENABLE_COVERAGE)

    file (GLOB testfiles ${tests_dir}/test_*.cpp)
#parsing files and adding them with TEST_FILE
    foreach(testf ${testfiles})
        get_filename_component(tname ${testf} NAME_WE)
        STRING(REGEX REPLACE "^test_" "test_${target}_" full_tname ${tname})
        add_executable(${full_tname} ${testf})
        set(libs ${ARGN})
        target_link_libraries(${full_tname} ${libs} UnitTest++)
        add_dependencies(${local_all_tests_target} ${full_tname})
        if(ENABLE_COVERAGE)
            add_dependencies(${local_all_tests_target}_coverage_dep ${full_tname})
            add_dependencies(global_coverage_dep ${full_tname})
        endif(ENABLE_COVERAGE)
        add_test(NAME ${full_tname} COMMAND ${full_tname})
    endforeach(testf)

    if(ENABLE_COVERAGE)
        SETUP_COVERAGE_TARGET_FOR(${target} ${local_all_tests_target}_coverage_dep)
    endif(ENABLE_COVERAGE)

endfunction(TEST_DIR)


